# KTE/ISZZ@ - Informační technologie (2023/24)

> Not implemented yet
 
## Okruh hardwarová a softwarová konfigurace a správa výpočetní techniky
1. [Analogová a číslicová technika – analogové a číslicové veličiny, analogové a číslicové zobrazení signálu, analogové a číslicové obvody, výhody číslicového zpracování signálu ve srovnání se signálem analogovým, číselné soustavy.](./hw_1.md)
2. [Logické funkce – logický obvod, kombinační logický obvod, sekvenční logický obvod, logické funkce, Způsoby popisu logických funkcí (pravdivostní tabulka, Booleova algebra, mapy), realizace logických funkcí.](./hw_2.md)
3. [Kodéry a dekodéry – multiplexory a demultiplexory, klopné obvody, posuvné registry, čítače impulsů a děliče frekvence (rozdělení, použití, pravdivostní tabulka, časový diagram), paměti – statické, dynamické, programovatelné, paměťové systémy.](./hw_3.md)
4. [Bezpečnost práce – legislativní vymezení, bezpečnost práce z pohledu zaměstnance a zaměstnavatele, rizika práce, řešení bezpečnosti práce v podmínkách škol (učitel a žák, základní a střední škola), pravomoci vedoucího pracovníka, ředitele. Bezpečnost práce v laboratořích a odborných učebnách školy, bezpečnostní značení, řešení únikových východů.](./hw_4.md)
5. [Požární ochrana – legislativní vymezení, požární ochrana v podmínkách škol a v laboratořích s výpočetní technikou.](./hw_5.md)
6. [Ergonomie – vymezení pojmu, ergonomie práce a pracovního prostředí, aplikace ergonomických zásad ve školství, ergonomie počítačového pracoviště a zásady bezpečnosti práce na PC a pro práci na interaktivní tabuli.](./hw_6.md)
7. [Robotika pro vzdělávání – širší souvislosti robotiky z pohledu aspektů sociálních, etických a společenských, charakteristika vybavení pro realizaci výuky robotiky, robotické stavebnice a školská praxe.](./hw_7.md)
8. [Robotické stavebnice – specifika, pojetí a přehled robotických stavebnic, jednodeskové počítače (Arduino, Raspberry) a aplikace (Tinkercad).](./hw_8.md)
9. [Operační systémy dotykových zařízení (Android, iOS, Windows) – nastavení rozhraní dotykového tabletu, použití jednoduchých a vícedotekových gest, elektronické účty a účty k nákupu aplikací (Windows Store, Android Market a App Store).](./hw_9.md)
10. [Microsoft Office pro dotyková zařízení – kancelářský balíček iWork, aplikace k práci s multimédii – video (Movie Maker, Pinacle Studio, iMovie), aplikace k práci s multimédii – hudba (Garage Band), aplikace pro fitness a řízení životního stylu, multitasking na dotykovém tabletu.](./hw_10.md)
11. [Hardware pro platformu PC a jeho vývoj – generace počítačů, počátky vývoje platformy IBM PC, serverová rozšíření, disková pole RAID a jejich dělení, hardware – principy komunikace, mosty, IRQ, DMA, rozšiřující sběrnice.](./hw_11.md)
12. [Počítačové komponenty pro platformu PC – procesory a jejich charakteristika a dělení, paměti a jejich charakteristika a dělení, základní desky a jejich charakteristika a dělení, rozšiřující komponenty PC a jejich charakteristika a dělení, pevné disky, rozšiřující karty, napájecí zdroje, vstupní periferie a jejich charakteristika a dělení, výstupní periferie – charakteristika a dělení.](./hw_12.md)
13. [Systémový a aplikační software – architektura operačních systému, typy a rozdělení operačních systému, BIOS, základní rozdělení aplikačního software, charakteristika jednotlivých typů programů, typy licencí, platnost licencí, licenční model firmy Microsoft, EULA, licenční programy firmy Microsoft, licenční model Open Source.14. Provoz a konfigurace desktopového operačního systému – typy desktopových operačních systémů, konfigurace a správa desktopového operačního systému (správa uživatelů, zabezpečení, příkazová řádka, příkaz cmd, příkaz ping a jeho přepínače, příkaz ipcconfig, a jeho přepínače, příkaz netstat).](./hw_13.md)
14. [Provoz a konfigurace desktopového operačního systému – typy desktopových operačních systémů, konfigurace a správa desktopového operačního systému (správa uživatelů, zabezpečení, příkazová řádka, příkaz cmd, příkaz ping a jeho přepínače, příkaz ipcconfig, a jeho přepínače, příkaz netstat).](./hw_14.md)
15. [Virtualizační technologie – virtualizace a její realizace, dělení virtualizačních technologií, typy virtualizačních nástrojů, použití virtualizačního nástroje a základní principy tvorby virtuálních strojů.](./hw_15.md)
16. [Virtualizační technologie a jejich využití ve vzdělávání – model integrace virtualizace do vzdělávání, dimenze modelu využití virtualizačních technologií ve vzdělávání, typické aplikace virtualizačních technologií ve vzdělávání.](./hw_16.md)
17. [Virtualizační nástroj VMware Workstation – použití virtualizačního nástroje VMware Workstation, základní parametry a omezení virtuálních strojů, možnosti migrace a přenosu virtuálních strojů. ](./hw_17.md)

## Okruh počítačové sítě a webové technologie
1. [Architektura počítačových sítí – typy a topologie počítačových sítí, typy komunikačních médií (kabeláže), charakteristika sítových protokolů, protokol ISO OSI a jeho struktura, protokol TCP](./netw_1.md)/IP a jeho struktura.
2. [Bezdrátové přenosy dat – technologie bezdrátových přenosů, adresace v počítačových sítích, topologie sítí, hardware pro BPD, Access Point, technologie AP, konfigurace vysílajícího AP, konfigurace klientského AP.](./netw_2.md)
3. [Počítačové sítě a IP protokol – základní princip IP protokolu, IP diagram, ICMP protokol, ARP protokol, IP adresy, význam IP adres, třídy IP adres, speciální IP adresy, maska sítě, nečíslované sítě, adresní plán.](./netw_3.md)
4. [Aplikační vrstva počítačových sítí – klasifikace služeb aplikační vrstvy, typy serverů aplikační vrstvy a jejich hlavní vlastnosti, popis základních služeb MS Windows server, přehled dostupných služeb MS Windows, instalace a konfigurace MS Windows server.](./netw_4.md)
5. [Provoz a konfigurace síťového operačního systému – typy síťových operačních systémů, Active Directory a jeho správa (správa uživatelů, správa organizačních jednotek, správa DNS záznamů).](./netw_5.md)
6. [Služba WWW – základní technologie konstrukce www stránek, princip činnosti služby WWW, vysvětlení pojmů HTTP, DNS, URL.](./netw_6.md)
7. [HTML – základní pravidla, syntaxe, limity, CSS – základní pravidla, syntaxe, responsivní návrh, použití CSS preprocesorů (příklady).](./netw_7.md)
8. [Programování klientských skriptů www – Javascript a jeho princip činnosti, datové typy, základní programové konstrukce, příklady použití.](./netw_8.md)
9. [Technologie jQuery – použití knihovny, její účel a výhody, jednoduchý příklad zápisu selektoru s použitím vybrané funkce (hide, show, atp.).](./netw_9.md)
10. [Technologie AJAX – účel, použití, příklad s využitím knihovny jQuery. (Kubrický)](./netw_10.md)
11. [Programování serverových skriptů www a webový server – princip činnosti, příklady, PHP a princip jeho činnosti, datové typy, základní programové konstrukce, příklady použití.](./netw_11.md)
12. [OOP v PHP – definice třídy, objekty, základní pravidla návrhu.](./netw_12.md)
13. [Požadavky současného návrhu webových aplikací – mobile first, responsivita webových aplikací, konstrukce návrhu, frameworky např. Bootstrap: jeho účel a použití](./netw_13.md)
14. [SEO Optimalizace – techniky, účel, příklady. Validátory kódu – techniky, účel.15. Webové aplikace třetích stran – např. Facebook app, Google apps, Youtube atp. Způsoby získání aplikací a aplikační rozhraní.](./netw_14.md)
16. [Databázový systém MySQL (MariaDb) – obsluha databázové systému. Uživatelské role a oprávnění, klientské aplikace – příklady, použití (mysql.exe, Workbench, PHPMyAdmin, Adminer).](./netw_16.md)
17. [MySQL a jeho využití pro web – rozdělení datových typů atributů, tabulkové enginy, typy a rozdíly, definice omezení cizího klíče, zamykání tabulek a transakce – princip a účel použití. ](./netw_17.md)

## Okruh databázové systémy, elektronické vzdělávací prostředí a materiály 
1. [E-learning a jeho vývoj – programování učiva (lineární a větvené programy), rozvoj distančního vzdělávání v závislosti na rozvoji technologií (korespondenční, multimediální, hypermediální).](./rdbs_1.md)

2. [E-learning a jeho složky – vymezení pojmu e-learning, širší a užší pojetí e-learningu, a jejich odlišnosti, složky e-learningu, LMS systém a jeho charakteristika a funkce, elektronická studijní opora a její složky, fáze tvorby elektronické studijní opory, multimedialita a interaktivita v e-learningu.](./rdbs_2.md)

3. [E-learning a jeho účastnící – role vyučujícího (tutora) v e-learningu, vzdělávaný a možnosti jeho aktivizace, využití modernizačních prvků v e-learningu (m-learning, virtuální realita, simulace apod.), komunikační kanály v e-learningu, využití virtuální třídy, problematika elektronického testování.](./rdbs_3.md)

4. [Základní pojmy v oblasti teorie systémů – historie teorie systémů, předmět zkoumání teorie systémů, základní pojmy teorie systémů, určení účelu a vymezení systému na objektu, struktura a dekompozice systému, organizace a řízení systémů.](./rdbs_4.md)

5. [Systémy a jejich modely – vymezení pojmu model, dělení modelů, volba struktury modelu, volba parametrů modelu, možnosti grafického vyjádření modelů, systémy a jejich řízení.](./rdbs_5.md)

6. [Teorie informačních systémů – vymezení pojmu informační systém, úloha informačních systémů, prvky informačního systému, data, informace, informace a znalosti, znalosti a rozhodování.](./rdbs_6.md)

7. [Informatika ve vzdělávání – informační technologie, informační gramotnost, ICT a jejich role v informační společnosti.](./rdbs_7.md)

8. [Grafické programy a multimédia – komprimace, malware, skenery a digitální fotoaparáty, OCR, digitální fotografie.](./rdbs_8.md)

9. [Multimédia – multimediální výukové programy, výukové programy vhodné pro vzdělávací oblast Informační a komunikační technologie, záznam a úprava zvuku, mixování zvuku.](./rdbs_9.md)

10. [Typy multimediálních prezentací a zásady jejich tvorby – princip zpracování klipů na digitální videostřižně (střih, titulky, dabing, hudba na pozadí apod.), volba různých stupňů komprese výsledného videa v závislosti na požadované kvalitě záznamu a kapacitě výstupního média.](./rdbs_10.md)

11. [Statistické zpracování dat – popisná a induktivní statistika, typy proměnných, základní charakteristiky měření, hypotézy, nejznámější statistické testy pro nominální, ordinální a metrická data.](./rdbs_11.md)

12. [Metody statistického zpracování dat – korelace, analýza rozptylu, vícerozměrné statistické metody, sémantický diferenciál, q-metodologie, dotazník, didaktický test.13. Základní pojmy oblasti Databázových systémů – vysvětlení a vztah pojmů Databáze a Databázový systém, relační model databáze – základní struktura a objekty.](./rdbs_12.md)

13. [Využití programu AutoCAD pro tvorbu 2D výkresové dokumentace – práce se souřadným systémem (příkaz USS a jeho parametry), základní příkazy a postupy pro vytváření 2D rovinných obrazců a jejich parametry (úsečka, kružnice, spline, křivka](./rdbs_13.md)

14. [Rozšiřující pojmy oblasti Databázových systémů – pojmy relace, atribut, entita, doména, klíč, normalizační pravidla - definice, vysvětlení typů integrity databáze.](./rdbs_14.md)

15. [Jazyk SQL – základní pravidla a syntaxe jazyka SQL – rozdělení SQL příkazů do skupin, základní součásti dotazu SELECT (FROM, WHERE, ORDER BY, LIMIT), agregační funkce a příklady jejich použití, metoda čárkového spojení – příklady zápisu.](./rdbs_15.md)

16. [Použití jazyka SQL – dotazy spojení JOIN – typy, rozdíly, syntaxe zápisu, příkazy GROUP BY a HAVING – účel použití.](./rdbs_16.md)

17. [Pokročilé použití jazyka SQL – definice a účel použití pohledů, triggerů a uložených proder, uložené procedury: rozdělení, rozdíly mezi procedurou a funkcí, možnosti a nadstavby PL/SQL.](./rdbs_17.md)

## Okruh algoritmizace a tvorba softwarových aplikací a počítačové grafiky 
1. [Základy algoritmizace (v jazyce Python) – struktura jazyka, vývojové prostředí, projekt a jeho součásti, knihovny (moduly).](./algrrr_1.md)

2. [Abstraktní datové typy (ADT, seznam, fronta, zásobník) a práce s nimi. , Řízení chodu programu (rozhodovací struktury) a cykly v Pythonu, jejich syntaxe a použití.](./algrrr_2.md)

3. [Funkce (v jazyce Python) – jejich tvorba a volání, parametry funkcí. Lokální a globální proměnné. Možnosti grafického uživatelského rozhraní (GUI) v Pythonu (tkinter...). Práce se soubory, CSV koncept.](./algrrr_3.md)

4. [Vývoj programovacích jazyků pro výuku programování na ZŠ – kategorizace, popis možností vybraného jazyka z pohledu řízení chodu programu (větvení, cykly, bloky/funkce).](./algrrr_4.md)

5. [Programování robota – přehled možností všeobecně, podrobnější popis ve zvoleném prostředí.](./algrrr_5.md)

6. [Unplugged programování a algoritmizace – vývojové diagramy, pseudokód, přirozený jazyk, vizuální programování, různé typy vizuálních programovacích jazyků a jejich popis.](./algrrr_6.md)

7. [Strukturované programování a objektově orientované programování – a typy programovacích jazyků (kompilované, interpretované programovací jazyky) a jejich popis.](./algrrr_7.md)

8. [Kódovací prostředí Scratch– možnosti a způsoby využití ve výuce zaměřené na základy algoritmizace, programování v softwaru Scratch.](./algrrr_8.md)

9. [Vývojové prostředí a programovací software pro robotiku – Scratch, Wiring, Python.](./algrrr_9.md)

10. [Charakteristika Bitbeam– spojení s robotickými stavebnicemi LEGO.](./algrrr_10.md)

11. [Grafické programy CAD – jejich technologie a možnosti využití - etapy výroby a jim odpovídající systémy počítačové podpory, možnosti technologie CAD, dělení programů CAD s ohledem na možnosti využití, popis možností programů využívajících 2D, 2.5D a 3D technologii, rastrová a vektorová grafika.](./algrrr_11.md)

12. [Grafický systém AutoCAD a popis jeho činnosti – obecný popis programu a možnosti jeho využití, práce s hladinami, možnosti ovládání programu AutoCAD, popis pracovního panelu programu, příkazový řádek a jeho součásti, způsoby zobrazování v systému AutoCAD.](./algrrr_12.md)

13. [Využití programu AutoCAD pro tvorbu 2D výkresové dokumentace – práce se souřadným systémem (příkaz USS a jeho parametry), základní příkazy a postupy pro vytváření 2D rovinných obrazců a jejich parametry (úsečka, kružnice, spline, křivka,obdélník, oblouk), tvorba šrafování a práce s kótami (tvorba a úprava kót, zapisování tolerancí).](./algrrr_13.md)

14. [Využití programu AutoCAD pro tvorbu 3D výkresové dokumentace – práce se souřadným systémem v prostoru, základní příkazy a postupy pro vytváření 3D prostorových ploch a těles a jejich parametry (koule, válec, hranol, plocha, anuloid, jehlan), booleovské operace pro úpravu prostorových ploch a těles (sjednocení, rozdíl, průnik).](./algrrr_14.md)

15. [Tvorba 3D grafických modelů s využitím programu AutoCAD – metody a postupy pro generování 3D prostorových ploch a těles s využitím 2D rovinných obrazců (rotování, vysunování), možnosti tvorby řezů a průřezů prostorových těles, export pro potřeby 3D tisku.](./algrrr_15.md)

16. [Grafická komunikace – druhy a typy grafické komunikace, pojem vizualizace dat a infografika, základní druhy a prvky infografiky, použití jednotlivých typů a metod vizualizace dat.](./algrrr_16.md)

17. [Metody a formy počítačové grafiky – vývojová stádia počítačové grafiky, základní principy zobrazování grafické informace, kompresní metody rastrových formátů grafiky, základní rastrové a vektorové formáty, dostupné programy pro tvorbu a editaci rastrové a vektorové grafiky.](./algrrr_17.md)

#!/bin/zsh

# output file
OUTF=$(basename $1).html

# print styles
echo "<style>
    body{
        font-family: Arial, sans-serif;
        font-size: 14px;
        line-height: 1.5em;
        margin: 0;
        padding: 0;
    }
    article{
		page-break-after: always;
        padding: 5px;
        margin: 10px;
        margin-bottom: 12em;
    }
    article:last-of-type{
        margin-bottom: 0;
    }
    article h1 {
        font-size: 1.5em;
        margin: 0;
        padding: 0;
    }
    article h2 {
        font-size: 1.2em;
        margin: 0;
        padding: 0;
    }
    article ul {
        border: 1px solid #EEE;
        padding: 10px 2em 1em 2em;
        margin: 0 auto 10px auto;
    }
    article ul ul {
        border: none;
    }
</style>" > ${OUTF};

# format MD files to HTML and concatenate the results into output.html
for f in $(ls $1*.md); do
	echo "Processing" $f;
	echo '<article id="'$f'">' | tee -a ${OUTF};
	pandoc $f -t html -o /dev/stdout | tee -a ${OUTF};
	echo "</article>\n\n" | tee -a ${OUTF};
done
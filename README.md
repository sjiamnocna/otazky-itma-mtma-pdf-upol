# Otázky ke SZZ na PdF UP Olomouc
- Pedagogika (*KPG/SZPR@*)
    - [Pedagogika](./Pedagogika)
- Informační tecgbiologie (*KTE/ISZZ@*)
- Matematika se zaměřením na vzdělávání (*KMT/SZB@*)

# Skript k tisku otázek z formátu MD
- Funguje ve WSL nebo v Linux CLI.
- `./printscript.sh ./Pedagogika/psychologie_`
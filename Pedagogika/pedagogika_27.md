# 27. Popište systém vzdělávání v ČR. Uveďte specifika předškolního, základního, středního, vyššího odborného, uměleckého, jazykového a zájmového (příp. jiného) zdělávání. Uveďte dílčí podrobnosti organizace školního roku.

## Systém vzdělávání v ČR

Český vzdělávací systém je komplexní struktura, která zahrnuje různé stupně a formy vzdělávání od předškolního po vysokoškolské vzdělávání a celoživotní učení. Fungování tohoto systému je řízeno zákony a vyhláškami vydanými Ministerstvem školství, mládeže a tělovýchovy (MŠMT).

## Předškolní vzdělávání
- **Instituce**: Mateřské školy
- **Věk dětí**: 3–6 let, případně do 7 let (odklad povinné školní docházky)
- **Cíle**: Doplnění rodinné výchovy, uspokojování potřeb a individuální rozvoj všech stránek osobnosti dítěte
- **Třídy**: Homogenní (děti stejného věku) a heterogenní (děti různého věku)
- **Dokument**: Rámcový vzdělávací program pro předškolní vzdělávání (RVP PV).

## Základní vzdělávání
- **Instituce**: Základní školy
- **Délka studia**: 9 let (1. stupeň: 1.–5. ročník, 2. stupeň: 6.–9. ročník)
- **Cíle**: Poskytování základního všeobecného vzdělání, příprava na praktický život
- **Dokument**: Rámcový vzdělávací program pro základní vzdělávání (RVP ZV).

## Střední vzdělávání
- **Instituce**: Gymnázia, střední odborné školy (SOŠ), střední odborná učiliště (SOU), konzervatoře
- **Délka studia**:
  - Gymnázia: 4 roky (ukončeno maturitní zkouškou)
  - SOŠ: 4 roky (ukončeno maturitní zkouškou)
  - SOU: 2–3 roky (ukončeno výučním listem), 4 roky (ukončeno maturitní zkouškou)
  - Konzervatoře: 6–8 let
- **Cíle**: Příprava pro výkon kvalifikovaných povolání a další studium
- **Dokument**: Rámcové vzdělávací programy pro střední vzdělávání (RVP SOV).

## Vyšší odborné vzdělávání
- **Instituce**: Vyšší odborné školy (VOŠ)
- **Délka studia**: 2–3 roky (ukončeno absolutoriem, titul DiS)
- **Cíle**: Příprava pro výkon náročných odborných činností
- **Dokument**: Rámcový vzdělávací program pro vyšší odborné vzdělávání (RVP VOŠ).

## Umělecké vzdělávání
- **Instituce**: Konzervatoře, základní umělecké školy
- **Délka studia**: Konzervatoře: 6–8 let; Základní umělecké školy: dle potřeby
- **Cíle**: Příprava na umělecké povolání, rozvoj talentu v oblasti hudby, tance, výtvarného umění
- **Dokument**: Rámcový vzdělávací program pro základní umělecké vzdělávání (RVP ZUV).

## Jazykové vzdělávání
- **Instituce**: Jazykové školy s právem státní jazykové zkoušky
- **Cíle**: Získání jazykových kompetencí, příprava na státní jazykové zkoušky
- **Dokument**: Specifické vzdělávací programy pro jazykové školy.

## Zájmové vzdělávání
- **Instituce**: Domy dětí a mládeže, školní kluby, školní družiny
- **Cíle**: Rozvoj zájmů a dovedností mimo formální vzdělávání
- **Aktivity**: Kroužky, tábory, volnočasové aktivity.

## Organizace školního roku
- **Začátek školního roku**: Obvykle první pracovní den v září
- **Konec školního roku**: 30. června
- **Prázdniny**:
  - Podzimní prázdniny: konec října
  - Vánoční prázdniny: konec prosince až začátek ledna
  - Pololetní prázdniny: konec ledna
  - Jarní prázdniny: únor až březen (různé termíny dle okresů)
  - Velikonoční prázdniny: březen/duben
  - Letní prázdniny: červenec a srpen
- **Vysvědčení**: Pololetní vysvědčení (konec ledna), závěrečné vysvědčení (konec června).
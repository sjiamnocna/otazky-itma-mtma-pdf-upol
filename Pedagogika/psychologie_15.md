# 15. Poruchy chování, klasifikace problémového chování, příčiny vzniku, agresivita a agrese, neagresivní poruchy chování, problematika syndromu ADHD, důsledky ADHD v dospělosti.

## Klasifikace problémového chování
> Problémové chování lze klasifikovat do několika kategorií, které zahrnují různé formy narušení společenských norem a pravidel.

1. **Agresivní poruchy chování**:
   - **Fyzická agrese**: Zahrnuje fyzické útoky, bití, ničení majetku.
   - **Verbální agrese**: Nadávky, urážky, vyhrožování.
   - **Nepřímá agrese**: Pomlouvání, šíření fám, sociální izolace ostatních.

2. **Neagresivní poruchy chování**:
   - **Útěky z domova**: Opakované opouštění domova bez souhlasu rodičů.
   - **Lhaní**: Opakované a bezdůvodné lhaní.
   - **Krádeže**: Páchání drobných krádeží.
   - **Vandalismus**: Ničení majetku, graffiti.

3. **Antisociální chování**:
   - Jedná se o chování, které je v rozporu se společenskými normami a pravidly a často zahrnuje porušování zákona.

4. **Impulzivní poruchy**:
   - Charakterizovány rychlými a nepředvídatelnými činy bez ohledu na jejich důsledky (např. kleptománie, pyrománie).

## Příčiny vzniku
Příčiny problémového chování jsou multifaktoriální a zahrnují kombinaci biologických, psychologických a sociálních faktorů.

1. **Biologické faktory**:
   - Genetické predispozice k impulsivitě a agresi.
   - Neurobiologické abnormality, včetně dysfunkcí v mozkových oblastech spojených s regulací emocí a chování.

2. **Psychologické faktory**:
   - Nedostatečné zvládání emocí a nízká frustrační tolerance.
   - Nedostatek empatie a morálního rozvoje.
   - Osobní traumata a stresové situace.

3. **Sociální faktory**:
   - Negativní rodinné prostředí, včetně násilí, zneužívání, nedostatečné výchovy a dohledu.
   - Vliv vrstevníků a sociální prostředí, které podporuje antisociální chování.
   - Sociálně-ekonomické problémy, jako je chudoba a nezaměstnanost.

## Agresivita a agrese
- **Agresivita**: Vnitřní tendence k agresivnímu chování, která se může nebo nemusí projevit navenek.
- **Agrese**: Konkrétní akt chování zaměřený na ublížení nebo poškození jiného člověka nebo majetku.

### Typy agrese
1. **Hostilní agrese**: Primárním cílem je způsobit bolest nebo újmu (fyzickou nebo emocionální).
2. **Instrumentální agrese**: Používá se jako prostředek k dosažení cíle (např. krádež s použitím násilí).
3. **Reaktivní agrese**: Impulsivní reakce na vnímanou provokaci nebo hrozbu.
4. **Proaktivní agrese**: Záměrné a plánované chování s cílem dosáhnout určitého výsledku.

## Neagresivní poruchy chování
- Zahrnují chování, které není přímo agresivní, ale stále narušuje společenské normy, jako je lhaní, útěky, vandalismus a krádeže.

## Problematika syndromu ADHD (Porucha pozornosti s hyperaktivitou)
- **ADHD** je neurovývojová porucha charakterizovaná přetrvávající nepozorností, hyperaktivitou a impulzivitou.
- **Příznaky**:
  - Nepozornost: Problémy s udržením pozornosti, snadná rozptýlitelnost, zapomínání úkolů.
  - Hyperaktivita: Nadměrná aktivita, neklid, problémy se sezením na místě.
  - Impulzivita: Problémy s ovládáním impulsů, přerušování ostatních, impulzivní rozhodování.

### Důsledky ADHD v dospělosti
- **Akademické a pracovní problémy**: Nízká úroveň dosaženého vzdělání, problémy s udržením zaměstnání, časté změny práce.
- **Sociální vztahy**: Problémy s udržováním dlouhodobých vztahů, sociální izolace, konflikty s ostatními.
- **Psychické zdraví**: Zvýšené riziko úzkosti, deprese a nízkého sebevědomí.
- **Rizikové chování**: Vyšší pravděpodobnost zapojení do rizikového chování, jako je zneužívání návykových látek nebo nehody.
- **Legální problémy**: Vyšší pravděpodobnost zapojení do trestné činnosti a problémů se zákonem.

> ADHD je chronické onemocnění, které vyžaduje dlouhodobý přístup k řízení symptomů, často zahrnující kombinaci medikamentózní léčby, behaviorální terapie a podpory ze strany rodiny a školního prostředí.
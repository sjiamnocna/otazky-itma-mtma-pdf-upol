# 23. Popište systém kurikulárních dokumentů aktuálně platných pro školy regionálního kolství. U dokumentů na státní úrovni vymezte, čím a jak podmiňují podobu urikulárních dokumentů na nižší úrovni.

## Systém kurikulárních dokumentů

Kurikulární dokumenty tvoří soubor oficiálních materiálů, které určují obsah a formu vzdělávání ve školách. Tyto dokumenty jsou vytvářeny na několika úrovních a každý stupeň ovlivňuje následující úroveň. V České republice je systém kurikulárních dokumentů rozdělen do tří hlavních úrovní:

1. **Státní úroveň**
   - **Národní program vzdělávání (NPV)**
      - Definuje dlouhodobé cíle vzdělávací politiky České republiky.
      - Určuje základní hodnoty a principy vzdělávání.
   - **Rámcové vzdělávací programy (RVP)**
      - Vycházejí z NPV a konkretizují vzdělávací cíle pro jednotlivé stupně škol (např. RVP pro základní vzdělávání, RVP pro gymnázia)
         - Gympl, kuchař/číšník, strojař, zdravotníci, zahradník, výtvarka...
         - RVP ZŠS - žáci se středně těžkou a těžkou mentální retardací
      - Obory jsou označeny kódem oboru ze soustavy oborů podle stupně vzdělání, výstupních dokumentů
         - `L` střední nebo střední odborné (bez maturity i výučního listu)
         - ...
         - `K` úplně střední s maturitou (gymnázia).
      - Obsahují očekávané výstupy a klíčové kompetence, které by žáci měli dosáhnout.
      - RVP jsou závazné pro všechny školy a jsou východiskem pro tvorbu školních vzdělávacích programů (ŠVP).

2. **Školní úroveň**
   - **Školní vzdělávací programy (ŠVP)**
     - Jsou vytvářeny jednotlivými školami na základě RVP.
     - Přizpůsobují obsah a metody výuky specifickým potřebám a podmínkám školy.
     - Definují konkrétní učební plány, časové rozvržení a metody hodnocení.

3. **Třídní úroveň**
   - **Plány výuky a tematické plány**
     - Detailně rozpracovávají jednotlivé učební předměty na úrovni třídy.
     - Obsahují konkrétní učební cíle, postupy a prostředky, které budou využity v průběhu školního roku.

## Dokumenty na státní úrovni a jejich vliv na nižší úrovně

1. **Národní program vzdělávání (NPV)**
   - **Funkce**: Určuje celkový rámec a směr vzdělávací politiky v České republice.
   - **Vliv**: NPV ovlivňuje tvorbu RVP tím, že stanovuje dlouhodobé cíle, hodnoty a principy, na kterých má být vzdělávání postaveno. NPV zajišťuje, že vzdělávací politika je konzistentní a reflektuje potřeby společnosti.

2. **Rámcové vzdělávací programy (RVP)**
   - **Funkce**: Konkrétněji definují vzdělávací cíle a obsah pro jednotlivé stupně škol.
   - **Vliv**: RVP jsou závazné pro tvorbu ŠVP. Každá škola musí vytvořit svůj ŠVP v souladu s RVP, což zajišťuje jednotnou úroveň vzdělání napříč školami. RVP určují, jaké klíčové kompetence a očekávané výstupy by měli žáci dosáhnout na konci určitého stupně vzdělání.

3. **Školní vzdělávací programy (ŠVP)**
   - **Funkce**: Přizpůsobují vzdělávací obsah a metody specifickým podmínkám a potřebám jednotlivých škol.
   - **Vliv**: ŠVP umožňují školám flexibilitu při realizaci vzdělávacího procesu. Školy mohou upravit a doplnit učební plány tak, aby lépe vyhovovaly jejich žákům a místním podmínkám, přičemž stále musí dodržovat rámec stanovený RVP.

4. **Plány výuky a tematické plány**
   - **Funkce**: Podrobně rozpracovávají výuku jednotlivých předmětů na úrovni třídy.
   - **Vliv**: Tyto plány jsou vytvořeny na základě ŠVP a zajišťují, že výuka je systematická a cílí na dosažení specifických vzdělávacích cílů stanovených v ŠVP. Učitelé mohou upravovat plány výuky podle aktuálních potřeb třídy a jednotlivých žáků, přičemž se řídí zásadami a cíli stanovenými ve vyšších kurikulárních dokumentech.

Tento systém kurikulárních dokumentů zajišťuje konzistenci a kvalitu vzdělávání na všech úrovních, přičemž umožňuje školám a učitelům přizpůsobit výuku specifickým podmínkám a potřebám jejich žáků.
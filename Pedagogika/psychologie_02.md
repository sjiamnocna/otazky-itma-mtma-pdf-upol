# 2. Přehled základních psychologických směrů – psychoanalýza, behaviorismus, humanistická psychologie, kognitivní psychologie, gestalt psychologie, transpersonální psychologie a představitelé jednotlivých směrů.

## 1. Psychoanalýza
- **Zakladatel:** Sigmund Freud
- **Hlavní myšlenky:** 
  - Vnitřní dispozice individua, primárnost pudových zdrojů.
  - Nevědomí má základní roli v psychice člověka.
  - Strukturální teorie psychiky: Id (Ono), Ego (Já), Superego (Nadjá).
  - Boj mezi různými aspekty osobnosti (pudy, rozum, vědomí).
  - Význam nevědomých sil a jejich vliv na chování.
- **Představitelé:** Alfred Adler, C. G. Jung

## 2. Behaviorismus
- **Zakladatel:** John B. Watson
- **Hlavní myšlenky:**
  - Zaměření na pozorovatelné chování, odmítnutí introspekce.
  - Chování jako systém naučených reakcí na podněty.
  - Experiment jako hlavní výzkumná metoda.
- **Představitelé:** B. F. Skinner, Ivan Pavlov, Edward Thorndike

## 3. Humanistická psychologie
- **Zakladatelé:** Abraham Maslow, Carl Rogers
- **Hlavní myšlenky:**
  - Důraz na jedinečnost člověka, jeho svobodu a potenciál pro seberealizaci.
  - Hierarchie potřeb (Maslowova pyramida), seberealizace na vrcholu.
  - Klíčová role subjektivní zkušenosti a vnitřního světa.
- **Představitelé:** Viktor Frankl, Rollo May

## 4. Kognitivní psychologie
- **Hlavní myšlenky:**
  - Studium poznávacích procesů, jako jsou vnímání, paměť, myšlení a řeč.
  - Lidská mysl jako informační procesor, analogie s počítačem.
  - Kognitivní teorie se zaměřují na zpracování informací a jejich vliv na chování.
- **Představitelé:** Jean Piaget, Albert Bandura, Aaron Beck

## 5. Gestalt psychologie
- **Zakladatelé:** Max Wertheimer, Wolfgang Köhler, Kurt Koffka
- **Hlavní myšlenky:**
  - Celostní přístup ke studiu psychických jevů, celek je více než součet jeho částí.
  - Vnímání a myšlení jako strukturované celky, zákony organizace vjemů (principy pregnance).
  - Důraz na vnímání vzorců a konfigurací.

## 6. Transpersonální psychologie
- **Zakladatel:** Stanislav Grof
- **Hlavní myšlenky:**
  - Zkoumání spirituálních zážitků a stavů vědomí, které přesahují běžnou zkušenost.
  - Kategorií zážitků zahrnují časově a prostorově rozšířené vědomí, mystické zážitky, setkání s archetypálními obrazy.
  - Využití nekonvenčních metod, jako jsou holotropní dýchání nebo psychedelické látky.
- **Představitelé:** Ken Wilber, Michael Murphy
# 4. Poznávací procesy a jejich charakteristika - čití a vnímání, představy a fantazie, paměť.

## Čití a vnímání
### Čití
- **Definice**: Proces přijímání informací prostřednictvím smyslových orgánů (analýzátorů).
- **Složení analyzátoru**: 
  - **Receptor**: Nervové buňky reagující na podněty.
  - **Nervy**: Předávají informace do mozkových center.
  - **Mozkové centrum**: Zpracovává informace a vytváří počitky.
- **Počitky**: Základní jednotky čití (zrakové, sluchové, čichové, chuťové, hmatové).

### Vnímání
- **Definice**: Proces zpracování a interpretace počitků, vedoucí k vytvoření vjemů.
- **Vjem**: Ucelený obraz předmětu nebo jevu, který vzniká na základě smyslových informací.
- **Charakteristiky vnímání**:
  - **Výběrovost**: Schopnost vybírat důležité informace.
  - **Apercepce**: Závislost vnímání na minulých zkušenostech.
  - **Konstantnost**: Vnímání předmětů jako stálých, i když se mění jejich vzhled.
  - **Transpozice**: Schopnost vnímat stejný předmět v různých podmínkách.

## Představy a fantazie
### Představy
- **Definice**: Reprodukce obrazů z paměti, které nejsou aktuálně vnímané.
- **Klasifikace podle vzniku**:
  - **Paměťové představy**: Vyvolané na základě předchozí zkušenosti.
  - **Fantazijní představy**: Nové kombinace a transformace paměťových představ.

### Fantazie
- **Definice**: Proces vytváření nových představ, často s prvky nereálnosti.
- **Způsoby tvorby fantazijních představ**:
  - **Aglutinace**: Spojování různých částí do nového celku.
  - **Hyperbolizace**: Zvětšení nebo zmenšení určitých prvků.
  - **Schematizace**: Zjednodušení složitých předmětů.
  - **Typizace**: Zvýraznění typických vlastností.

## Paměť
### Procesy paměti
- **Zapamatování**: Proces uložení informací do paměti.
  - **Úmyslné**: Cílené zapamatování.
  - **Neúmyslné**: Automatické zapamatování bez cíleného úsilí.
- **Uchování**: Udržení informací v paměti po určitou dobu.
- **Vybavování**: Proces znovuvyvolání uložených informací.
  - **Reprodukce**: Aktivní vybavení informací bez jejich přítomnosti.
  - **Znovupoznání**: Uvědomění si informací při jejich opětovném vnímání.

### Druhy paměti
- **Krátkodobá paměť**: Uchovává informace na krátkou dobu, omezený rozsah.
- **Dlouhodobá paměť**: Ukládá informace na delší dobu, neomezený rozsah.
- **Senzorická paměť**: Udržuje informace z jednotlivých smyslů po velmi krátkou dobu.

### Vlastnosti paměti
- **Vštípivost**: Schopnost zapamatovat si informace.
- **Rozsah paměti**: Množství informací, které lze uchovat.
- **Přesnost paměti**: Míra správnosti vybavených informací.
# 28. Definujte pojem školská legislativa. Demonstrujte provázanost školských právních předpisů v rámci logicky uzavřeného celku s právními předpisy jednotlivých odvětví práva soukromého a veřejného s následnou aplikací do praktické roviny.

## Školská legislativa
- **Definice**: Školská legislativa se označuje soubor právních norem a předpisů s přímou nebo nepřímou vazbou na oblast školství. Zahrnuje předpisy práva veřejného i soukromého, včetně předpisů s mezinárodním prvkem, zejména z hlediska legislativy EU.
- **Úrovně legislativy**:
  - Ústavní úroveň
  - Zákony
  - Vyhlášky
  - Nařízení vlády
  - Ministerská nařízení
  - Dokumenty podzákonného a samosprávného charakteru (např. v působnosti zřizovatele)
  - Metodiky, výkladová pravidla, dokumenty z dohledové činnosti (např. školní inspekce).

## Provázanost školských právních předpisů s právními předpisy jednotlivých odvětví práva soukromého a veřejného
- **Základní právní předpisy**:
  - **Listina základních práv a svobod (LZPS)**: V čl. 33 je zakotveno právo na vzdělání, které zahrnuje:
    - Právo na vzdělání pro každého
    - Povinnou školní docházku
    - Právo na bezplatné vzdělání v základních a středních školách podle schopností občana a možností společnosti, včetně vysokých škol
    - Regulaci možnosti zřizovat jiné školy než státní a vyučovat na nich
  - **Zákon č. 561/2004 Sb., školský zákon**: Základní právní předpis upravující předškolní, základní, střední, vyšší odborné a jiné vzdělávání
  - **Zákon č. 563/2004 Sb., o pedagogických pracovnících**: Definuje předpoklady pro výkon pedagogické činnosti, odbornou kvalifikaci, další vzdělávání pedagogů a kariérní růst.

## Aplikace do praktické roviny
- **Systém vzdělávání**: Školská legislativa určuje strukturu a organizaci školského systému, včetně zřizování a provozu škol, stanovení vzdělávacích programů a hodnocení výsledků vzdělávání.
- **Sociální aspekty**: Legislativní normy zajišťují práva a povinnosti všech účastníků vzdělávacího procesu, včetně žáků, rodičů a pedagogů, a to v souladu s principy rovnosti a nediskriminace.
- **Bezpečnost a ochrana zdraví**: Právní předpisy v oblasti školství zahrnují také normy týkající se bezpečnosti a ochrany zdraví ve školním prostředí, což je klíčové pro vytvoření bezpečného a zdravého prostředí pro vzdělávání.
- **Financování**: Školský zákon a související předpisy regulují financování školství, včetně státního rozpočtu, dotací a veřejných zakázek.
- **Mezinárodní aspekty**: Legislativní normy v oblasti školství mají často mezinárodní přesahy, např. v oblasti ochrany osobních údajů (GDPR) a uznávání zahraničního vzdělání.

## Provázanost s právem soukromým a veřejným
- **Soukromé právo**: Školská legislativa se prolíná s občanským zákoníkem (zákon č. 89/2012 Sb.), který definuje právní subjektivitu osob, odpovědnost, náhradu škody a smluvní vztahy. Zákoník práce a související předpisy upravují vztahy škol jako zaměstnavatelů k pedagogům a dalším zaměstnancům.
- **Veřejné právo**: Právní předpisy v oblasti veřejného práva zahrnují správní řád, trestní zákon a trestní řád, přestupkový zákon, zákon o obcích, stavební normy a předpisy na ochranu osobních údajů.

Tento komplexní systém zajišťuje, že školství v ČR funguje na základě jasně definovaných pravidel, která jsou v souladu s širším právním rámcem a mezinárodními standardy.

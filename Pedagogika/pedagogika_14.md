# 14. Charakterizujte pedagogickou diagnostiku a argumentujte její význam v současné kole. Popište základní metody pedagogické diagnostiky. Charakterizujte způsoby  význam hodnocení ve školní praxi.

## Pedagogická diagnostika
Pedagogická diagnostika (PD) je vědecká disciplína, která se zabývá hodnocením úrovně výchovně-vzdělávacího procesu, jeho podmínkami a výsledky. Formuluje teorii pedagogického diagnostikování a jeho metody a hledá možné způsoby vysvětlení získaných zjištění. Ačkoliv využívá poznatků z psychologie, její úkoly a cíle jsou odlišné od diagnostiky psychologické. PD se zajímá o sociální, kulturní a hospodářskou situaci rodiny žáka, výchovné působení mimoškolních institucí a kvalitu neformálních společenských interakcí. Zaměřuje se na obsahovou složku (zjišťování dosažené úrovně vědomostí, dovedností a návyků) a procesuální složku (způsob, jakým proces výchovy a vzdělávání probíhá a jak ovlivňuje žáka).

### Význam pedagogické diagnostiky v současné škole
1. **Poznání žáka**: Učitel potřebuje dítě poznat ve všech směrech, aby výsledky jejich společné práce byly co nejlepší.
2. **Individualizace výuky**: Akceptace jedinečnosti každého žáka a porozumění jeho osobnostním zvláštnostem a rodinným souvislostem.
3. **Zlepšení výukových postupů**: Na základě diagnostických zjištění hledání efektivnějších cest a úprava výukových postupů.
4. **Podpora učitele**: Umožňuje učiteli reagovat na individuální potřeby žáků a přizpůsobit výuku aktuálním podmínkám třídy.

## Základní metody pedagogické diagnostiky
1. **Pozorování**
   - **Popis**: Učitel pozoruje chování a reakce žáků během výuky a dalších školních aktivit.
   - **Typy**: Krátkodobé a dlouhodobé, náhodné a systematické, introspekce (sebepozorování) a extrospekce (pozorování druhých).
   - **Použití**: Identifikace problémů, zjišťování efektivity výukových metod.
2. **Rozhovor**
   - **Popis**: Přímá komunikace mezi učitelem a žákem (nebo rodiči).
   - **Typy**: Strukturovaný (předem stanovené otázky) a nestrukturovaný (volný).
   - **Použití**: Získání hlubšího porozumění problémům žáka, zjišťování osobních a rodinných informací.
3. **Testy**
   - **Popis**: Standardizované zkoušky zaměřené na zjištění úrovně vědomostí a dovedností.
   - **Typy**: Testy schopností, psychických funkcí, školní zralosti, didaktické testy.
   - **Použití**: Měření pokroku, diagnostika vzdělávacích potřeb.
4. **Analýza výsledků činnosti**
   - **Popis**: Rozbor hotových prací a úkolů žáků.
   - **Použití**: Hodnocení kvality a pokroku ve výuce, identifikace silných a slabých stránek žáků.

## Způsoby a význam hodnocení ve školní praxi
1. **Způsoby hodnocení**
   - **Hodnocení známkou (klasifikace)**: Tradiční metoda využívající číselné nebo písmenné hodnocení.
   - **Slovní hodnocení**: Kvalitativní posouzení výkonu žáka a jeho postojů k učení.
   - **Sebehodnocení žáka**: Autonomní hodnocení vlastních výkonů a pokroku žákem.
   - **Další formy**: Bodové či procentové hodnocení, odměny, tresty, pochvaly, zpětná vazba.

2. **Význam hodnocení**
   - **Zpětná vazba pro učitele**: Informace o efektivitě výuky a potřebách úprav.
   - **Zpětná vazba pro žáky**: Informace o pokroku, motivace k dalšímu učení.
   - **Motivace žáků**: Povzbuzování k lepší organizaci práce a větší úsilí.
   - **Dokumentace pro rozhodování**: Záznamy o prospěchu žáků slouží k rozhodování o vzdělávacích potřebách a jednání s rodiči.
   - **Posouzení připravenosti**: Hodnocení připravenosti žáků na další učení a případnou diferenciaci výuky.
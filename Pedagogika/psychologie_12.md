# 12. Vymezení normality a abnormality, problematika stresu, obranné mechanismy, frustrace, konflikt, stres, vnímání stresu; dimenze stresu: emocionální, intelektuální, sociální, enviromentální, spirituální, fyzická.

## Normalita
- **Normalita** je stav nebo chování, které odpovídá běžným, průměrným nebo standardním vzorcům společnosti. 
- **Kriteria normality**:
  - **Statistické hledisko**: Chování, které se vyskytuje u většiny populace.
  - **Normativní hledisko**: Chování, které je považováno za normální na základě sociálních nebo kulturních norem.
  - **Funkční hledisko**: Schopnost jedince fungovat v každodenním životě a plnit své role.

## Abnormalita
- **Abnormalita** je stav nebo chování, které se odchyluje od běžných, průměrných nebo standardních vzorců, často s negativními důsledky pro jedince nebo společnost.
- **Kriteria abnormality**:
  - **Statistická vzácnost**: Chování nebo myšlení, které je u populace velmi vzácné.
  - **Porušení norem**: Chování, které porušuje kulturní nebo sociální normy.
  - **Osobní strádání**: Jedinec zažívá stres, úzkost nebo jiné negativní emoce.
  - **Zhoršené fungování**: Schopnost jedince fungovat v každodenním životě je narušena.

# Problematika stresu

## Stres
- **Stres** je psychický a fyziologický stav vyvolaný vnějšími nebo vnitřními podněty (stresory), které jsou vnímány jako ohrožující nebo nadměrně zatěžující.
- **Stresory**: Události nebo podmínky, které vyvolávají stresovou reakci (např. ztráta zaměstnání, osobní konflikty, finanční problémy).

## Vnímání stresu
- **Vnímání stresu** je subjektivní a závisí na osobních vlastnostech, zkušenostech, zvládacích schopnostech a aktuálním stavu jedince.
- **Primární hodnocení**: Posouzení, zda je situace ohrožující.
- **Sekundární hodnocení**: Posouzení dostupných zdrojů a možností zvládání stresu.

# Obranné mechanismy
- **Obranné mechanismy** jsou nevědomé psychologické strategie, které jedinec používá k ochraně před úzkostí a nepříjemnými emocemi.
- **Příklady**:
  - **Popření**: Odmítání přijmout realitu nebo fakta.
  - **Projekce**: Připisování vlastních nežádoucích myšlenek nebo pocitů jiným lidem.
  - **Racionalizace**: Vytváření logických vysvětlení pro iracionální chování.
  - **Sublimace**: Přesměrování nepřijatelných impulzů do společensky přijatelných činností.

# Frustrace
- **Frustrace** je emoční stav vznikající, když jedinec nemůže dosáhnout svých cílů nebo uspokojit své potřeby.
- **Příčiny**: Vnější překážky (např. sociální bariéry) nebo vnitřní faktory (např. nedostatek dovedností).

# Konflikt
- **Konflikt** je situace, kdy jedinec čelí dvěma nebo více protichůdným požadavkům, cílům nebo potřebám.
- **Typy konfliktů**:
  - **Konflikt přitažlivost-přitažlivost**: Volba mezi dvěma pozitivními možnostmi.
  - **Konflikt averze-averze**: Volba mezi dvěma negativními možnostmi.
  - **Konflikt přitažlivost-averze**: Situace obsahuje jak přitažlivé, tak odpudivé aspekty.

# Dimenze stresu

## 1. Emocionální dimenze
- **Definice**: Zahrnuje emoční reakce na stres, jako jsou úzkost, strach, vztek, smutek nebo deprese.
- **Příklady**: Ztráta blízké osoby může vyvolat hluboký smutek a pocit beznaděje.

## 2. Intelektuální dimenze
- **Definice**: Vliv stresu na kognitivní procesy, jako je pozornost, paměť, rozhodování a schopnost řešit problémy.
- **Příklady**: Vysoký stres může zhoršit schopnost soustředit se a zpracovávat informace.

## 3. Sociální dimenze
- **Definice**: Vliv stresu na mezilidské vztahy a sociální interakce.
- **Příklady**: Konflikty v práci nebo v rodině mohou zvýšit úroveň stresu a vést k izolaci nebo nedorozuměním.

## 4. Environmentální dimenze
- **Definice**: Vliv fyzického prostředí na stres, včetně hluku, znečištění, klimatu nebo životních podmínek.
- **Příklady**: Bydlení v hlučné oblasti může zvýšit úroveň stresu a ovlivnit kvalitu spánku.

## 5. Spirituální dimenze
- **Definice**: Zahrnuje otázky smyslu života, hodnot a víry, které mohou ovlivnit, jak jedinec zvládá stres.
- **Příklady**: Víra v určitý smysl života nebo vyšší moc může poskytnout útěchu a podporu v těžkých časech.

## 6. Fyzická dimenze
- **Definice**: Fyzické reakce těla na stres, včetně hormonálních změn, imunitní reakce a psychosomatických symptomů.
- **Příklady**: Chronický stres může vést k fyzickým problémům, jako jsou bolesti hlavy, vysoký krevní tlak nebo žaludeční vředy.
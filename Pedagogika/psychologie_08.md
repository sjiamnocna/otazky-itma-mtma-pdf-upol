# 8. Vývojová psychologie a její předmět, determinace psychického vývoje člověka, pojem vývojové změny, klasifikace vývojových změn. Zrání a učení.

## Předmět vývojové psychologie
- **Vývojová psychologie** je obor psychologie, který studuje změny a kontinuitu v chování, myšlení, emocích a sociálních vztazích během života člověka.
- Zaměřuje se na vývoj od početí až po smrt, zahrnující různé oblasti jako kognitivní, motorický, emoční a sociální vývoj.

## Determinace psychického vývoje člověka
1. **Biologické faktory**:
   - Genetická výbava (dědičnost), vrozené predispozice, neurologický vývoj.
   - Endokrinní systém, růst a zrání těla, vrozené reflexy.

2. **Environmentální faktory**:
   - Sociální a kulturní vlivy, výchova, vzdělání, socioekonomické prostředí.
   - Vliv vrstevníků, rodiny, školy, médií a širší společnosti.

3. **Interakce genetických a environmentálních faktorů**:
   - Psychický vývoj je výsledkem složité interakce mezi genetickými predispozicemi a vlivy prostředí.
   - Různé teorie kladou důraz na různé aspekty této interakce, například ekologický přístup zdůrazňuje význam kontextu, ve kterém se dítě vyvíjí.

## Pojem vývojové změny
- **Vývojové změny**: Procesy, které vedou k změnám v chování, schopnostech a prožívání v průběhu času.
- **Druhy vývojových změn**:
  - **Kvantitativní změny**: Změny v množství nebo velikosti (např. růst výšky, rozšíření slovní zásoby).
  - **Kvalitativní změny**: Změny ve struktuře nebo povaze chování (např. změna v typu myšlení).

## Klasifikace vývojových změn
1. **Kognitivní vývoj**: Změny ve způsobu myšlení, řešení problémů a zpracovávání informací.
   - **Příklady**: Vývoj od konkrétního k abstraktnímu myšlení, zlepšení paměťových schopností.

2. **Motorický vývoj**: Zlepšení motorických dovedností a koordinace.
   - **Příklady**: Vývoj jemné a hrubé motoriky, učení se chůzi, psaní.

3. **Emoční vývoj**: Změny v emoční regulaci, prožívání a vyjadřování emocí.
   - **Příklady**: Vývoj empatie, zvládání stresu a emocí.

4. **Sociální vývoj**: Změny v mezilidských vztazích a sociálních dovednostech.
   - **Příklady**: Vývoj sociálních rolí, vztahy s vrstevníky, schopnost spolupráce.

## Zrání a učení
- **Zrání**:
  - Proces, který je do značné míry určen genetickými faktory a přirozeným vývojem organismu.
  - Zahrnuje biologický růst a vývoj nervového systému, který umožňuje určité schopnosti a dovednosti (např. schopnost chůze, řeči).

- **Učení**:
  - Proces získávání nových znalostí, dovedností, hodnot a návyků prostřednictvím zkušeností, praxe a vzdělání.
  - Učení je silně ovlivněno prostředím a může modifikovat nebo posílit vlivy zrání.

- **Vztah mezi zráním a učením**:
  - Zrání poskytuje biologické základy, na kterých může učení stavět (např. dítě musí dosáhnout určité úrovně motorického zrání, aby se mohlo naučit jezdit na kole).
  - Učení může urychlit nebo usměrnit proces zrání (např. stimulující prostředí může podpořit kognitivní vývoj).


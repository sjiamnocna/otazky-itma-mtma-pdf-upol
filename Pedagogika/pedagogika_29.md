# 29. Vyjmenujte práva a povinnosti účastníků vzdělávacího procesu v návaznosti na vnitřní ředpisy škol a školských zařízení. Zdůvodněte význam dodržování předpisů z oblasti ezpečnosti a ochrany zdraví při vzdělávání a s ním přímo souvisejících činnostech. opište možnosti využití interdisciplinární spolupráce při řešení výchovných problémů a školách a možnosti využití výchovných opatření ze strany škol a školských ařízení.

## Práva a povinnosti účastníků vzdělávacího procesu

### Práva žáků
- **Právo na vzdělání**: Vzdělávací proces je systematická činnost zahrnující interakci pedagoga a žáka.
- **Právo na přístup k informacím**: Podporujícím rozvoj žáka a o průběhu vzdělávání.
- **Právo na vyjádření názoru**: V záležitostech týkajících se vzdělávání.
- **Právo na účast na akcích školy**.
- **Právo na poradenskou pomoc školy**.

### Povinnosti žáků
- **Povinnost řádně docházet do školy** a účastnit se akcí pořádaných školou v rámci povinného vyučování.
- **Povinnost omlouvat nepřítomnost** podle pravidel.
- **Povinnost šetrně zacházet s vybavením školy** a udržovat pořádek.
- **Povinnost chovat se slušně** a podle společenských pravidel.
- **Povinnost ohlásit změnu zdravotního stavu**.
- **Povinnost neopouštět budovu školy bez svolení pedagoga**.

### Práva učitelů
- **Právo na rovné zacházení** a odměňování podle zákoníku práce.
- **Právo na další vzdělávání**.
- **Právo na účast na chodu školy**.
- **Právo na slušné a důstojné jednání** ze strany žáků, rodičů a školy.

### Povinnosti učitelů
- **Povinnost vykonávat výchovně vzdělávací činnost** s přihlédnutím k ochraně žáků.
- **Povinnost dodržovat učební plány, osnovy a předpisy**.
- **Povinnost vykonávat dozor**.
- **Povinnost zachovávat mlčenlivost o osobních údajích**.
- **Povinnost spolupracovat s výchovným poradcem, metodikem prevence a zákonnými zástupci**.
- **Povinnost vyvarovat se nežádoucího chování** (např. sexuální povahy).

## Význam dodržování předpisů z oblasti bezpečnosti a ochrany zdraví při vzdělávání
- **Zajištění bezpečnosti**: Zachování zdraví a života všech účastníků vzdělávacího procesu.
- **Preventivní opatření**: Prevence před mimořádnými událostmi, účinná reakce na mimořádné události a následná evaluace a opatření.
- **Školní krizové plány**: Každá škola má vypracovaný krizový plán a provádí pravidelné prověrky BOZ a BOZP.

## Interdisciplinární spolupráce při řešení výchovných problémů
- **Mezioborová spolupráce**: Zapojení odborníků z různých oborů (psychologové, sociální pracovníci, speciální pedagogové).
- **Intenzivní spolupráce s rodinou**: Nezbytná pro odhalení příčin výchovných problémů.
- **Řešení výchovných problémů**: Od drobných disociálních projevů (zlozvyky, neposlušnost) po závažné asociální chování (záškoláctví, závislosti, kriminalita).

## Možnosti využití výchovných opatření ze strany škol a školských zařízení
- **Běžné pedagogické prostředky**: Pro řešení lehčích výchovných problémů (zlozvyky, úzkost).
- **Speciální výchovná opatření**: Pro závažnější problémy (agrese, šikana), včetně zapojení dalších institucí a odborníků.
- **Strukturované prostředí**: Vytváření podpůrného a strukturovaného prostředí ve třídě.
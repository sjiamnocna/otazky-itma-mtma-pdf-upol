# 21. Prezentujte ucelený systém didaktických zásad. Vysvětlete postup učitelovy práce s chybou ve vyučování. Prezentujte chybu jako pozitivní jev pro učení žáka a navrhněte konkrétní doporučení pro učitele při práci s chybou ve výuce vašeho probačního předmětu.

## Didaktické zásady

Didaktické zásady jsou základní pravidla vedoucí k efektivní výuce. Jsou to vědecky zdůvodněné požadavky a pravidla, které se vztahují na všechny činitele výuky, tedy na žáky, učitele a učivo. Zásady se vyvíjejí v závislosti na společensko-historickém a vědecko-technickém rozvoji a jejich formulace vychází ze tří základních zdrojů:
1. Poznatky pedagogického a psychologického výzkumu.
2. Příklady dobré praxe.
3. Historické zkušenosti  .

## Základní didaktické zásady podle Vališové

1. **Zásada vědeckosti**
   - Obsah učiva by se měl zakládat na podložených faktech a nejnovějších věděckých poznatcích.
   - Učitel by měl sledovat aktuální vývoj svého oboru a aktualizovat své pedagogické vzdělání.

2. **Zásada aktivity**
   - Žáci musejí mít kladný vztah k učení, aby byli aktivní. 
   - Předpokládá volbu vhodných motivačních prostředků a soustředění pozornosti na význam aplikace didaktických zásad.

3. **Zásada cílevědomosti**
   - Adekvátní práce učitele s edukačními cíli je klíčová.
   - Důležitá je podpora vnitřní motivace žáka.

4. **Zásada soustavnosti**
   - Nové poznatky se musí opírat o předchozí a tvořit základnu pro následující.
   - Osvojování vědomostí a dovedností musí probíhat v ucelené soustavě a logickém systému  .

## Postup učitelovy práce s chybou ve vyučování

1. **Vnímání chyby jako pozitivního jevu**
   - Chyba je odchylka od normy či očekávání, ale prakticky je informací, která vyzývá ke změně stavu a ke korekci.
   - Učitel by měl chápat chybu jako příležitost ke zdokonalení, a přispívat tak k vytvoření správného postoje žáků k chybám.

2. **Strategie práce s chybou**
   - Netrestat chyby, ale využívat je jako motivační východisko pro poznávací proces.
   - Povzbuzovat žáky, aby chyby vnímali jako přirozenou součást učení a dokázali jich využít.
   - Kdykoliv je učitel upozorněn na chybu, měl by poděkovat za opravu a vyslovit uznání.

## Chyba jako pozitivní jev pro učení žáka
- Chyba je diagnostika toho, že se žák o něco pokoušel, byť neúspěšně, což je nezbytné pro učení.
- Učení typu „pokus-omyl“ je jeden ze způsobů, jak se něco dozvídáme. Neúspěch je součást vzdělání a podporuje hledačství žáků.

## Návrh práce s chybou pro učitele informatiky

### 1. Vnímání chyby jako pozitivního jevu
- **Vysvětlení významu chyby**: Na začátku kurzu učitel informatiky vysvětlí studentům, že chyby jsou přirozenou součástí učení programování a práce s technologií. Zároveň zdůrazní, že chyby jsou cenné pro pochopení toho, jak věci fungují.
- **Pozitivní přístup k chybám**: Učitel povzbuzuje studenty, aby se nebáli dělat chyby a aby je vnímali jako příležitost k učení a růstu.

### 2. Strategie práce s chybou
- **Diagnostika chyby**
  - **Identifikace chyby**: Při řešení programovacích úloh učitel povzbuzuje studenty, aby identifikovali chyby ve svém kódu. To zahrnuje jak syntaktické chyby (chybný zápis kódu), tak logické chyby (chybný algoritmus).
  - **Sebereflexe**: Studenti by měli být vedeni k tomu, aby se zamysleli nad tím, proč k chybě došlo, a co by mohli udělat jinak, aby chybu opravili a zdokonalovali se v psaní kódu bez chyb.

- **Zpětná vazba a oprava chyby**
  - **Peer review**: Učitel může organizovat sezení, kde studenti vzájemně kontrolují svůj kód a poskytují si zpětnou vazbu. To podporuje spolupráci a učí studenty, jak konstruktivně kritizovat a přijímat kritiku.
  - **Iterativní proces**: Učitel podporuje studenty, aby opravovali chyby krok za krokem a testovali změny průběžně. To pomáhá studentům lépe porozumět, jak jednotlivé části kódu ovlivňují celý program.

### 3. Konkrétní doporučení pro učitele informatiky
- **Práce s chybami v kódu**
  - **Syntaktické chyby**: Při objevování syntaktických chyb učitel ukazuje, jak používat ladicí nástroje (debuggery) a integrované vývojové prostředí (IDE), které často poskytují užitečné informace o tom, kde se chyba vyskytla a jak ji opravit.
  - **Logické chyby**: Učitel podporuje studenty v tom, aby psali testovací případy a systematicky kontrolovali svůj kód. To zahrnuje kontrolu vstupních a výstupních hodnot a ověřování správnosti algoritmu.

- **Analýza chyb**
  - **Post-mortem analýza**: Po dokončení projektu nebo úkolu učitel organizuje diskusi, kde studenti společně analyzují chyby, které během práce udělali. Diskuse zahrnuje, co se z chyb naučili a jak by mohli předejít podobným chybám v budoucnu.
  - **Dokumentace chyb**: Učitel povzbuzuje studenty, aby si vedli záznamy o chybách, na které narazili, a o jejich řešeních. To může sloužit jako užitečný zdroj pro budoucí reference a opakování.

### 4. Podpora chybování jako součást učení
- **Povzbuzování k experimentování**: Učitel podporuje studenty, aby experimentovali s různými přístupy a algoritmy, i když to může vést k chybám. Cílem je rozvíjet kreativní myšlení a odvahu zkoušet nové věci.
- **Kultura otevřenosti**: Učitel vytváří třídu, kde je bezpečné dělat chyby a mluvit o nich. To zahrnuje respektování všech nápadů a návrhů studentů a podporu otevřené komunikace.

### Příklad konkrétního výukového cíle
- **Výukový cíl**: Žák bude schopen identifikovat a opravit syntaktické a logické chyby v jednoduchém programu napsaném v jazyce Python.
- **Postup práce**: Učitel nejprve vysvětlí základní typy chyb a ukáže, jak je hledat pomocí ladicích nástrojů. Následně studenti dostanou úkol s chybami, které mají najít a opravit. Po dokončení úkolu proběhne diskuse, kde studenti prezentují své řešení a sdílejí své zkušenosti s ostatními.

Tímto způsobem může učitel informatiky přispět k vytvoření pozitivního a podpůrného prostředí, kde jsou chyby vnímány jako přirozená a cenná součást procesu učení.
# 22. Ozřejměte, jaké jsou zásady práce učitele s materiálními didaktickými prostředky. osuďte, jaké učební pomůcky lze ve vašem aprobačním předmětu používat při ealizaci zásady názornosti, a uveďte je. Objasněte, jaká je jejich role při výuce i jaký e poměr učitelova slova a prezentace pomůcky.

## Zásady práce učitele s materiálními didaktickými prostředky

1. **Výběr vhodných prostředků**
   - **Relevantnost**: Materiální didaktické prostředky musí být vybrány tak, aby odpovídaly cílům výuky a byly přiměřené věku a schopnostem žáků.
   - **Aktualizace**: Učitel by měl používat moderní a aktuální prostředky, které reflektují současné poznatky a technologie.

2. **Integrace do výuky**
   - **Systematické zařazení**: Materiální prostředky by měly být integrovány do výuky systematicky, aby podpořily dosažení výukových cílů.
   - **Komplementarita**: Prostředky by měly doplňovat a obohacovat verbální výklad učitele, nikoliv ho nahrazovat.

3. **Názornost a srozumitelnost**
   - **Jasnost**: Prezentované pomůcky musí být jasné a snadno pochopitelné pro všechny žáky.
   - **Názornost**: Prostředky by měly být vizuálně atraktivní a názorné, aby usnadnily pochopení složitých konceptů.

4. **Aktivní zapojení žáků**
   - **Interaktivita**: Materiální prostředky by měly podporovat aktivní účast žáků a zapojení do výukového procesu.
   - **Samostatná práce**: Učitel by měl podporovat žáky v samostatném používání učebních pomůcek.

## Učební pomůcky při realizaci zásady názornosti v předmětu informatika

1. **Počítače a software**
   - **Role**: Základní nástroj pro praktické cvičení programování, práci s databázemi a grafickými programy.
   - **Poměr učitelova slova a prezentace**: Učitel vysvětluje teorii a postupy, žáci následně aplikují získané poznatky prakticky na počítači.

2. **Interaktivní tabule**
   - **Role**: Umožňuje dynamickou a interaktivní prezentaci učiva, která může zahrnovat kreslení schémat, spouštění programů a ukazování simulací.
   - **Poměr učitelova slova a prezentace**: Kombinace verbálního výkladu a vizuální demonstrace přímo na tabuli.

3. **Didaktické hry a simulace**
   - **Role**: Pomáhají žákům pochopit složité koncepty programování a algoritmizace hravou formou.
   - **Poměr učitelova slova a prezentace**: Učitel vysvětluje pravidla a principy, žáci interaktivně hrají a experimentují.

4. **Modely a schémata**
   - **Role**: Fyzické modely nebo schémata sítí, hardwarových komponent nebo datových struktur pomáhají vizualizovat abstraktní pojmy.
   - **Poměr učitelova slova a prezentace**: Učitel poskytuje teoretický základ, následně ukazuje a vysvětluje modely.

## Role učebních pomůcek při výuce

- **Podpora porozumění**: Učební pomůcky zvyšují názornost výuky a usnadňují žákům pochopení složitých a abstraktních pojmů.
- **Zvýšení motivace**: Interaktivní a vizuálně atraktivní pomůcky mohou zvýšit zájem a motivaci žáků k učení.
- **Zlepšení paměti**: Názorné pomůcky napomáhají lepšímu zapamatování a dlouhodobé retenci informací.
- **Rozvoj dovedností**: Praktické pomůcky podporují rozvoj konkrétních dovedností, jako je programování, řešení problémů nebo práce s technologiemi.

## Poměr učitelova slova a prezentace pomůcky

- **Vyváženost**: Učitel by měl najít rovnováhu mezi verbálním vysvětlováním a použitím názorných pomůcek. Slova by měla připravit kontext a vysvětlení, zatímco pomůcky by měly doplnit a konkretizovat informace.
- **Doplňkovost**: Učitelova slova a pomůcky by měly být navzájem doplňující se složky výuky. Verbální výklad poskytuje strukturu a logiku, zatímco názorné pomůcky usnadňují vizualizaci a praktické porozumění.

### Doporučení pro učitele informatiky

1. **Připravit materiální pomůcky předem**: Ujistit se, že všechny potřebné pomůcky jsou připraveny a funkční před začátkem hodiny.
2. **Střídání metod**: Kombinovat různé učební pomůcky (počítače, tabule, modely) s verbálním výkladem pro udržení zájmu a pozornosti žáků.
3. **Aktivní zapojení žáků**: Povzbuzovat žáky k aktivnímu používání učebních pomůcek a interakci s nimi během výuky.
4. **Reflexe a zpětná vazba**: Pravidelně získávat zpětnou vazbu od žáků na efektivitu použitých pomůcek a upravovat výuku podle jejich potřeb a reakcí.

Tímto způsobem může učitel informatiky efektivně využívat materiální didaktické prostředky k podpoře názornosti a interaktivity ve výuce, což vede k lepšímu porozumění a zapojení žáků.
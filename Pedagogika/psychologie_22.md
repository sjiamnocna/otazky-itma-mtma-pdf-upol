# 22. Jedinec v sociální skupině. Znaky a klasifikace sociálních skupin. Struktura a dynamika malé sociální skupiny.

> Jedinec je nedílnou součástí různých sociálních skupin, které ovlivňují jeho chování, názory a hodnoty. Sociální skupiny poskytují jedincům identitu, podporu a normy chování. Skupiny mohou být definovány různými charakteristikami a mohou mít různou strukturu a dynamiku.

## Znaky sociálních skupin

1. **Interakce**:
   - Členové skupiny vzájemně komunikují a interagují.
   - Interakce mohou být přímé (osobní setkání) nebo nepřímé (prostřednictvím médií).

2. **Společné cíle a zájmy**:
   - Skupina má společné cíle, zájmy nebo hodnoty, které spojují její členy.
   - Tyto cíle mohou být explicitní (formální) nebo implicitní (neformální).

3. **Normy a pravidla**:
   - Skupiny vytvářejí normy a pravidla, které regulují chování členů a udržují sociální řád.
   - Tyto normy mohou být formální (právní předpisy, pravidla) nebo neformální (nepsaná pravidla chování).

4. **Sociální role**:
   - Každý člen skupiny má určitou roli, která určuje jeho pozici a očekávání chování uvnitř skupiny.
   - Roly mohou být formální (vedoucí, člen týmu) nebo neformální (rádce, zábavný člen).

5. **Pocit sounáležitosti**:
   - Členové skupiny mají pocit příslušnosti a identifikace s danou skupinou.
   - Tento pocit posiluje soudržnost a loajalitu vůči skupině.

## Klasifikace sociálních skupin

1. **Primární skupiny**:
   - Skupiny s blízkými, osobními a dlouhodobými vztahy mezi členy.
   - Příklady: rodina, blízcí přátelé.

2. **Sekundární skupiny**:
   - Skupiny s méně intimními a často formálními vztahy, zaměřené na dosažení specifických cílů.
   - Příklady: pracovní týmy, sportovní kluby, třídy ve škole.

3. **Formální skupiny**:
   - Skupiny s definovanou strukturou, pravidly a hierarchií, často vytvořené za účelem dosažení konkrétních cílů.
   - Příklady: korporace, armáda, politické strany.

4. **Neformální skupiny**:
   - Skupiny s neformálními a spontánními vztahy, bez pevné struktury.
   - Příklady: přátelské skupiny, zájmové kroužky.

5. **Referenční skupiny**:
   - Skupiny, ke kterým se jedinci vztahují a s nimiž se identifikují, i když nejsou jejich členy.
   - Příklady: celebrity, společenské třídy, profesionální skupiny.

## Struktura a dynamika malé sociální skupiny

### Struktura malé sociální skupiny
- **Hierarchie**:
  - Malé skupiny často mají hierarchickou strukturu, kde některé osoby zaujímají vedoucí role.
  - Hierarchie může být formální (vedoucí, zástupci) nebo neformální (přirození vůdci).

- **Role**:
  - Každý člen má specifickou roli, která určuje jeho funkci a odpovědnost ve skupině.
  - Role mohou být přiřazeny formálně (organizátorem skupiny) nebo vznikají přirozeně (na základě osobnostních vlastností).

- **Komunikace**:
  - Efektivní komunikace je klíčová pro fungování skupiny. Může zahrnovat jak verbální, tak neverbální prostředky.
  - Komunikace může být otevřená a participativní nebo omezená a řízená.

### Dynamika malé sociální skupiny
- **Soudržnost (kohéze)**:
  - Míra, do jaké jsou členové skupiny motivováni zůstat v ní a pracovat na dosažení společných cílů.
  - Vysoká soudržnost vede k lepší spolupráci a vyšší spokojenosti členů.

- **Konflikt**:
  - Nevyhnutelná součást dynamiky skupiny, může být konstruktivní nebo destruktivní.
  - Konstruktivní konflikty mohou vést k růstu a inovacím, zatímco destruktivní konflikty mohou oslabit skupinu.

- **Normy**:
  - Pravidla a očekávání chování, které jsou v rámci skupiny sdíleny.
  - Normy regulují chování členů a udržují sociální řád ve skupině.

- **Fáze vývoje skupiny** (Tuckmanův model):
  - **Forming (formování)**: Členové se seznamují, utváří se základní pravidla a struktura.
  - **Storming (bouření)**: Vznikají konflikty a třenice, když se členové přizpůsobují různým názorům a stylům práce.
  - **Norming (normalizace)**: Vzniká dohoda na pravidlech a normách, zvyšuje se spolupráce.
  - **Performing (výkon)**: Skupina efektivně pracuje na dosažení svých cílů.
  - **Adjourning (rozpouštění)**: Skupina se rozpadá po dosažení cíle nebo při ukončení projektu.

> Malé sociální skupiny jsou klíčové pro rozvoj sociálních dovedností, identifikaci s hodnotami a normami, a poskytují jedinci podporu a identitu. Struktura a dynamika těchto skupin hrají zásadní roli v jejich efektivitě a celkovém fungování.
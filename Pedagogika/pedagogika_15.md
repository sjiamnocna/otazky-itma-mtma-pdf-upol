# 15. Charakterizujte a zdůvodněte základní trendy proměn výchovy v historickém kontextu obových paradigmat (pojetí člověka a výchovy, cíle výchovy). Vysvětlete, jak se obové koncepty výchovy odrážejí v současných koncepcích výchovy a vzdělávání.

## Historický kontext a proměny výchovy

1. **Řecko-antické období (starověká paideia)**
   - **Pojetí člověka**: Člověk jako „animal rationale“ (bytost rozumová).
   - **Cíle výchovy**: Kulturní vzdělávání a rozvoj rozumu.
   - **Důraz**: Rozvoj rozumových schopností a ctností.

2. **Židovsko-křesťanské období (středověké educatio)**
   - **Pojetí člověka**: Člověk jako „imago dei“ (obraz Boží).
   - **Cíle výchovy**: Kultivace duše a příprava na život věčný, důraz na morální a náboženské hodnoty.
   - **Důraz**: Kultivace morálních a duchovních hodnot.

3. **Novověké období (moderní doba)**
   - **Pojetí člověka**: Člověk jako „ego cogito“ (myslící bytost).
   - **Cíle výchovy**: Rozvoj svobodné a autentické osobnosti, schopnost kritického myšlení.
   - **Důraz**: Osobní svoboda, rozvoj individuality a racionální myšlení.

## Pojetí výchovy a cíle v historickém kontextu

1. **Optimistické pojetí**
   - **Představitelé**: Platon, Komenský, Kant, Pestalozzi.
   - **Charakteristika**: Výchova a prostředí mají klíčový vliv na vývoj jedince.

2. **Pesimistické pojetí**
   - **Představitelé**: J. J. Rousseau, nacistická výchova.
   - **Charakteristika**: Důraz na dědičnost, prostředí a výchova mají omezený vliv.

3. **Utopické pojetí**
   - **Představitelé**: John Locke.
   - **Charakteristika**: Výchova je všemocná, jedinec se formuje pouze prostřednictvím výchovy („tabula rasa“).

4. **Realistické pojetí**
   - **Představitelé**: Současní pedagogové.
   - **Charakteristika**: Vývoj jedince je ovlivněn dědičností, prostředím a výchovou současně&#8203;:citation[oaicite:4]{index=4}&#8203;&#8203;:citation[oaicite:3]{index=3}&#8203;&#8203;:citation[oaicite:2]{index=2}&#8203;.

## Současné koncepce výchovy a vzdělávání

1. **Pragmatismus**
   - **Charakteristika**: Důraz na aktivní zapojení dítěte, získávání zkušeností a prožitků.
   - **Využití**: Projektová výuka, hands-on aktivity.

2. **Dynamická pedagogika**
   - **Charakteristika**: Bere v úvahu proměnlivost společenského prostředí, klade důraz na perspektivu.
   - **Využití**: Přizpůsobení výuky aktuálním společenským potřebám.

3. **Experimentální pedagogika**
   - **Charakteristika**: Klade důraz na experiment v oblasti prostředků výchovy, cíl není prioritou.
   - **Využití**: Laboratorní práce, experimenty ve výuce.

4. **Existenciální pedagogika**
   - **Charakteristika**: Důraz na výchovu jedince jako svobodné a autentické osobnosti.
   - **Využití**: Podpora individuálních projektů, sebepoznání a seberealizace.

5. **Pedagogika kultury**
   - **Charakteristika**: Účast na kulturních hodnotách a příprava na činnou účast v tvůrčím životě.
   - **Využití**: Kulturní projekty, umělecká výchova.

6. **Náboženská pedagogika**
   - **Charakteristika**: Rozvoj jednotlivce a jeho duše, vnitřní překonání člověka.
   - **Využití**: Výuka náboženství, etické a morální vzdělávání.

7. **Alternativní směry 20. století**
   - **Charakteristika**: Důraz na rozvoj tvořivosti, aktivnosti a solidarity.
   - **Využití**: Montessori, Waldorfské školy, Daltonský plán.

8. **Antipedagogické směry**
   - **Charakteristika**: Popírají jakýkoliv cíl výchovy, považují ho za manipulaci.
   - **Využití**: Volná výchova, samoučení&#8203;:citation[oaicite:1]{index=1}&#8203;&#8203;:citation[oaicite:0]{index=0}&#8203;.

## Závěr
Dobové koncepty výchovy se v současných koncepcích výchovy a vzdělávání odrážejí v různých pedagogických přístupech, které kladou důraz na individualizaci výuky, aktivní zapojení žáků a přizpůsobení výchovy měnícím se společenským podmínkám. Historické paradigmaty formovaly základní principy a cíle, které jsou stále relevantní a inspirující pro moderní vzdělávací praxi.
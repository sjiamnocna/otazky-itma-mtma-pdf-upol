# 1. Charakterizujte pedagogiku jako vědní obor. Popište a interpretujte historický vývoj pedagogiky jako vědy. Vysvětlete vztah pedagogiky k ostatním vědám. Charakterizujte předmět, znaky a funkce pedagogiky. Vztah pedagogické teorie a pedagogické praxe. Uveďte systém pedagogických disciplín. Popište a zhodnoťte aktuální problémy a otázky pedagogiky.

- Pojem **pedagogika** pochází z antického Řecka, kde *paidagogos* (obvykle *vzdělaný otrok*) pečoval v bohatých rodinách o děti (pais=dítě, agóge=vedení).
- **Pedagogika** je věda o výchově, která zkoumá výchovný proces jako jeden z nejvýznamnějších společenských jevů.
- Podle **Skalky (1977)** - pedagogika je věda o výchově, která zkoumá výchovu jako záměrnou formativní činnost, její vývoj, zákonitosti, podstatu, strukturu, funkci, mechanismy v historických podmínkách společenského vývoje a rozpracovává teorii a metodiku výchovně-vzdělávacího procesu, jeho obsah, principy, organizační formy, metody a postupy
- Podle **Skalkové (2004)** - vědní obor, který zahrnuje teorii i výzkum v oblasti výchovy a vzdělávání
- Podle **Švarcové (2005)** - pedagogika je společenskovědní disciplína, která zkoumá významnou součást skutečnosti: záměrné ovlivňování vývoje osobnosti člověka
- Obsah výuky vzniká z praktických potřeb, vyplývajících ze společenského vývoje.
    - V počátcích lidské společnosti byl člověk svázaný s přírodou a jejími zákonitostmi. Příprava mladých lidí k životu se realizovala v jednoduchém společném pracovním procesu, kde se stýkali děti i dospělí, a děti napodobují počínání dospělých. Velký význam však měl osobní příklad dospělých a osvojování řemeslných dovedností. Toto zasvěcování = iniciace probíhalo již u nejstarších známých kultur, později se vzdělávání a jeho různé formy staly výsadou majetných skupin občanů.
- pedagogika zkoumá cíle, úkoly, metody a prostředky výchovy a vzdělávání
- V posledních 2 stoletích se výchova demokratizovala a stala se postupně přístupnou všem vrstvám obyvatelstva. S přílivem informací postupně narůstá složitost prostředí, ve kterém se musel jedinec orientovat a adaptovat. Předávání poznatků a zkušeností se tím stalo složitější, proto byla potřeba zorganizovat cílevědomou výchovnou činnost.
- Přestože výchova zde existuje nějakým způsobem od počátku lidstva, samotná věda o výchově vznikla až v novověku. Do té doby se pedagogické myšlení rozvíjelo v rámci filozofických a teologických koncepcí.
- Již ve **starověkém Řecku** byly formulovány zásady výchovy a byla zde snaha vytvořit její ideál. K těmto filozofům, kteří se o to pokoušeli patřili např. **Sokrates**, **Platon**, **Aristoteles** a **Démokritos**. Velký rozmach podnítila filozofie v období humanismu a renesance, která ve své pedagogické rovině odráží výchovně-vzdělávací problémy své doby a často svými názory na výchovu předbíhá dobu.
- **John Locke**
    - vnímá výchovu jako zásadní pro rozvoj člověka
    - „výchova je všemocná“ a člověk je utvářen především působením výchovy
- **F. Bacon**
    - reflektuje potřebu rostoucího obsahu vzdělání (potřebu zařadit např. technické znalosti pro obchodníky a řemeslníky apod.)
    - Dílo: O důstojnosti a pokroku věd (1623)
- **J. A. Komenský**
    - Ve **Velké didaktice** nastiňuje požadavky na obsah vzdělávání
    - Vliv osvícenství – víra v sílu lidského rozumu vede k encyklopedickému pojetí světa
    - Kritika vlivu náboženství a církve, důraz je kladen na přirozenost člověka.
    - Vytvořil první ucelenější systém poznatků o výchově a vzdělávání, proto bývá považován za zakladatele vědy o výchově.
- **J. J. Rosseau**
    - Podceňoval výchovu, kladl důraz na „přirozený a svobodný“ vývoj člověka
    - Rozhodující faktor je podle něj to, co je vrozené a pedagogika do procesu rozvoje vnitřních sil v souladu s přírodou nemá zasahovat, pouze jej pozorovat.
- Až v 19. století se společně s dynamickým rozvojem přírodních a společenských věd začíná pedagogika konstituovat jako samostatný vědní obor, ve kterém jsou řešeny základní obecné, teoretické a metodologické problémy výchovy.
    - V této době dochází hlavně ve střední Evropě ke vzniku specializovaných institucí, které mají za úkol rozvíjet teorii výchovy. Důvodem byly také potřeby rozvíjející se buržoazní společnosti v oblasti průmyslu, obchodu a administrativy.
- Od reforem **Marie Terezie** (osvícenství) u nás vzniká a vyvíjí se veřejné školství 
    - Výchova a vzdělání začínají být systematické, institucionalizované, postupem času se systém škol dále mění
- Od přelomu 18. a 19. století se objevují dvě převládající koncepce:
    - **Teorie vzdělávacího realismu** – hájila zařazování přírodních věd do obsahu vzdělávání,
    - **Teorie novohumanistického vzdělání** – tendence určovat obsah vzdělání podle subjektu, důraz kladen na formální vzdělání, na řeckou kulturu a studium klasických obsahů
    - **J. F. Herbart** (**herbartismus**) prosazuje důslednou autoritu pedagoga, klade důraz na klasická díla a znalost řečtiny pro rozvoj dovedností a mravní rozvoj, ap.
- Za zakladatele **moderní pedagogické teorie**, která se opírala o psychologii a etiku, je považován **J.F. Herbart**, který na základě děl jeho předchůdců ve svých pracích naznačil předmět a strukturu samostatné vědecké disciplíny.
- Proces sebeuvědomování a konstituování pedagogiky jako samostatné vědní disciplíny můžeme chápat jako **druhou etapu vývoje pedagogického myšlení**, která trvá **od 19. století až po dnešní dobu**. Z hlediska rozvoje didaktického myšlení je důležité, že se na přelomu 18. a 19. století začal rozpracovávat pojem „všeobecné vzdělání“ ve smyslu kultivace člověka
- Od počátku 20. století se objevuje a rozvíjí tzv. **pedagogický reformismus** (pojem zastřešující různé reformní návrhy a koncepce v pedagogice typické kritikou tradiční školy, pedocentrismem, respektem k dítěti a jeho zvláštnostem, globalizací obsahů ve vyučování, činnostním učením, kooperací, spoluprací školy a rodiny apod.)
    - Mezi nejznámější reformní hnutí patří např. školy *Montessori*, *Waldorfské školy*, *jenská škola*, *daltonský plán* aj.
- do Československa se první reformní myšlenky dostávají ve 20. a 30. letech 20. století, objevují se snahy reformovat školství, které je pouze s drobnými změny (prostřednictvím tzv. malého školského zákona) přejímáno z doby před rokem 1918, o reformní podoby školství se u nás snaží např. Václav Příhoda („Příhodova školská reforma“).
- 	Po roce 1989 u nás vznikají různé soukromé (např. církevní aj.) a reformní školy (*Montessori*, *Waldorf*), v jejichž práci se obměňuje tradiční přístup k objektu a subjektu výchovy.
- Ve vnímání subjektu a objektu výchovy si můžeme všímat přístupu pojmenovatelného jako reciprocita (vzájemné ovlivňování, spolupráce, kdy je aktivní objekt i subjekt), reciprocita by měla postupně přejít v sebevýchovu (mělo by tím dojít ke splynutí subjektu a objektu výchovy)
## Vztah pedagogiky k ostatním vědám
- Vztah pedagogiky k ostatním vědám se projevuje především v oblasti komplexního přístupu k výzkumu výchovných jevů, kde dochází k setkávání odborníků z mnoha vědních disciplín, což umožňuje zkoumat pedagogické jevy v širším kontextu.
- Pedagogika se svým charakterem nemůže izolovat od ostatních věd, naopak má hluboký vztah, zejména ke vědám, které jí poskytují materiál pro analýzu výchovných jevů
- Pedagogika musí využívat výsledků a teoretických pouček těchto věd a tvořivě je začleňovat do svého systému

- **O. Baláž** vymezuje 4 základní okruhy spojení pedagogiky s ostatními vědami:
    - **Vědy z oblasti filozofie**
        - Umožňují pedagogice vytyčit cíle, obsah vzdělání a výchovy, analyzovat prostředí, ve kterém se výchovný děj odehrává
        - vědy **psychologicko-antropologické** umožňují aplikovat a korigovat výchovné působení z hlediska vnitřních podmínek jednotlivců a skupin.
        Pedagogika a psychologie
        - pedagogika se dlouhou dobu rozvíjela v rámci filozofie, filozofické směry také jasně ovlivnily vývoj pedagogiky
        - Mezi významné filozofy, kteří se věnovali výchově patří: **Platon**, **Aristoteles**, **Démokritos**, **E. Rotterdamský**, **F. Rabelais**, **J.Locke**,
        **R. Descartes**, **J.J.Rousseau**, **C.A. Helvétius**, **D. Diderot**, **J.A. Condorcet**, **I. Kant**, **J. F. Herbart**, **K.D. Ušinskij**, **L.N. Tolstoj**, **H. Spencer** a ve 20.století **T.G. Masaryk**
        - výchova chce v praxi řešit problémy, o kterých filozofie uvažuje pouze abstraktně
        - filozofie je významná i pro pedagogickou praxi, pomáhá např. výchovným pracovníkům lépe se orientovat v nejrůznějších otázkách společenského života a lépe aplikovat výchovné cíle a zásady v každodenní praxi
        - hraniční disciplínou mezi pedagogikou a filozofií je **filozofie výchovy** - ta se zabývá objasňováním problémů spojených s edukačními procesy ve společnosti v historickém kontextu
        - **Pedagogika a filozofie** mají společný objekt zkoumání - poznávání a formování osobnosti člověka
    - **Psychologie**
        - odhaluje pedagogické zákonitosti psychických procesů a vlastností, které je nutné respektovat při výchově
        - hraniční disciplínou je **pedagogická psychologie**, která zkoumá psychické zákonitosti v konkrétních výchovně vzdělávacích situacích a podmínkách, které odpovídají daným cílům výchovy a zároveň přihlíží k věkovým a individuálním zvláštnostem žáků
        - Největší význam mají **obecná, vývojová, sociální a klinická psychologie, psychologie osobnosti a psychologická diagnostika**
    - **Sciologie**
        - Studuje vliv společenského prostředí na člověka a společenské vztahy mezi lidmi a tím umožňuje hlouběji poznávat prostředí, ve kterém se odehrává výchovný proces.
        - Sleduje širší problematiku vztahu společnosti a výchovně-vzdělávací soustavy.
        - Díky sociologickému zkoumání vlivů společenského prostředí na člověka je možné přesněji vymezit možnosti a hranice výchovného působení.
        - Hraniční disciplínou je **sociální pedagogika**, která zkoumá např. problémy rodinné výchovy, studuje procesy vytváření kolektivů dětí a mládeže, zájmových útvarů i sociální procesy, které v nich probíhají.
        - podle *Baláže* je soc. pedagogika integrující disciplínou, která nejen spojuje pedagogiku a sociologii, ale sjednocuje do uceleného systému teorii mimoškolní výchovy a pedagogiku volného času a další podobné pedagogické disciplíny
    - **Ekonomie**
        - Hraniční disciplínou je **ekonomie vzdělávání**
            - Zajišťuje vědecké plánování vzdělávání a zkoumá účelnost využití prostředků určených na výchovu a vzdělávání, zintenzivnění uplatňování výsledků vědeckovýzkumné práce a jejich praktické aplikace
            - pedagogická ekonomie zaměřuje výzkum i na posuzování a vyhodnocování základních pedagogických dokumentů, metodických příruček a učebnic
        - další hraniční disciplína je **školský management**
            - zkoumá proces plánování, organizování, operativního řízení, kontroly a motivace výchovy a vzdělávání ve všech úrovních.
- Tyto 4 vědy patří k základním vědám o které se pedagogika opírá, ale obecně se dá říct, že pedagogika spolupracuje se všemi přírodními, společenskými a technickými vědami, i když s některými jen okrajově, např:
    - **Etika** - věda o mravnosti a mravních hodnotách, teorii morálky, vymezuje a zdůvodňuje způsoby mravního chování lidí.
    - **Přírodní vědy** - poskytují pedagogice poznatky o celkovém tělesném stavu a proměnlivosti jednotlivých tělesných znaků vzhledem k prostředí u populací v určitých časových obdobích, o analýze stavby lidského těla a o životních funkcích organismu.
    - **Lékařské vědy** - teorie tělesných a mentálních defektů, nemocí a jejich příčin, vliv prostředí na zdraví člověka, duševní i fyzické hygieně práce.
    - **Logika** - poskytuje pomoc při řešení metodologických problémů, při vymezování obsahu vzdělávání, stanovení didaktických principů a zásad.
    - **Kybernetika** - poskytuje předpoklady pro teorii programovaného učení, pro koncepci vyučovacích strojů i pro řízení pedagogického procesu.
    - **Matematika** - spolu s logikou a kybernetikou, která pomocí kvantitativní analýzy umožňuje kvantifikaci společenských jevů.
    - **Etnografie** - lidové útvary vzdělanosti a výchovy, prostředky, jimiž se šíří lidová kultura a tím i výchova širokých vrstev nejen v procesu práce, ale i v lidové tvorbě.
    - **Demografie** - populační vědy založená na statistice podává přehled o rozdělení školsky získané vzdělanosti různých stupňů a rozvoji vzdělanostních trendů u různých věkových skupin, žen či mužů.

## Předmět, znaky a funkce pedagogiky
- Předmětem pedagogiky je výchova, výchova dětí a mládeže a organizované formy i druhy výchovy dospělých.
- Zkoumá zákonitosti vývoje člověka ve všech obdobích jeho života.
- Vychází z výchovy a vzdělávání jsou z celoživotních procesů - musí se zkoumat nepřetržitě v průběhu celého života.
- Zabývá se nejen výchovou v budoucnosti, ale také v minulosti.
- Předmětem pedagogiky je také žák jako objekt i subjekt výchovy a učitel jako řídící činitel výchovně-vzdělávacího procesu
    - na výchovu mají vliv i mimoškolní činitelé - předmětem pedagogiky je také rodina, mimoškolní výchova, organizace dětí a mládeže a všechny masové komunikační prostředky výchovy
- je nutné věnovat pozornost výchově jako celku i jejím dílčím formám (velké skupiny lidí vs malé skupiny).
- Předmětem jsou také jedinci, kteří jsou nějakým způsobem zdravotně znevýhodněni.
- Pedagogika jako vědní obor musí nejdříve pedagogické jevy popsat a klasifikovat podle různých kritérií, poté jevy musí srovnávat, vyhodnocovat a zobecňovat čímž se dostává k obecným pedagogickým kategoriím. Tím je možné pochopit velké množství pedagogických jevů a současně odhalit dílčí a obecné pedagogické zákonitosti a je schopna vytvářet pedagogický systém.
- **Základní znaky pedagogiky**
    1. Přesně vymezený předmět výzkumu
    2. Tvoří určitý systém
    3. Má své metody odpovídající povaze předmětu a logickým zákonitostem
- **Funkce pedagogiky**
    - **Poznávací**	- praktická (utvářející)
    - **Prognostická** - vztah pedagogické teorie a pedagogické praxe
        - Pedagogická teorie je vždy abstraktního rázu, přesahuje svou obecností konkrétní situace, je zobecněním společného v jednotlivých situacích. Snaží se proniknout k tomu, co je ve výchovném procesu zásladní a neměnné. Je velmi málo konkrétní, nemůže vystihnout každou situaci.
        - Pedagogická praxe se snaží vyhovět konkrétním podmínkám a potřebám, je proměnlivá, závislá na době a místě.
        - Teorie vždy vychází z praktické společenské potřeby a opět se do ní finálně vrací
        - Teorie a praxe jsou navzájem propojené a je proto důležitá znalost obojího Systém pedagogických disciplín
        - **Pedagogická teorie s praxí se navzájem prolínají**, teorie zdokonaluje praxi, ukazuje jí nové efektivnější postupy a dokonalejší  prostředky činnosti. Praxe ověřuje a opravuje teorii, je kritériem pravdivosti výsledků lidského poznání a poskytuje teorii důkazy ke zobecňování
- ve starší literatuře se člení na 7 disciplín
    1. **Obecná pedagogika** - obecné základy pedagogiky, podstata společenské fce. výchovy, otázky a cíle výchovy, objasňuje základní pedagogické pojmy, stanovení biologických a psychologických podmínek výchovy, vymezením složek výchovy, školskými soustavami, výchovnými a vzdělávacími institucemi, metodologickými problémy pedagogiky atd., snaží se o systematizaci a interpretaci základních ped. jevů a zákonitostí
    2. **Didaktika** - teorie vzdělávání a vyučování) - objasňuje podstatu výchovně-vzdělávací činnosti, zabývá se otázkami obsahu, metod, forem, didaktických prostředků, učebních pomůcek…, zkoumá organizaci vyučovacího procesu
    3. **Teorie výchovy** - zkoumá a objasňuje výchovné jevy a děje v užším slova smyslu, zaměřuje se hlavně na cíle, úkoly, obsah, metody, formy a prostředky výchovy, otázka organizace dětského kolektivu
    4. **Metodiky** (speciální didaktiky=teorie vyučování) - teorie vzdělávání a výchovy v užších specifických úsecích
    5. **Nauka o školských normách a předpisech** - srovnávací a popisná nauka o normách a předpisech kterými se řídí školství
    6. **Dějiny pedagogiky** - vývoj pedagogických idejí a myšlení, patří sem i analýza díla a života pedagogických myslitelů
    7. **Speciální pedagogiky** - teorie formování jedince se specifickými zaměřeními
        - a. **vývojová pedagogika** (pedagogika kojence, puberty…).
        - b. **speciální pedagogiky** (výchova a vzdělávání osob se speciálními vzdělávacími potřebami, vyplývající z různých druhů postižení).
            - **psychopedie** - mentálně postižené osoby.
            - **tyflopedie** - zrakově postižení.
            - **etopedie** - osoby s poruchami chování	logopedie - osoby s narušenou komunikační schopností.
            - **surdopedie** - sluchově postižení	somatopedie - osoby s postižením hybnosti.
            - speciální pedagogika osob s vícenásobným postižením.
            - speciální pedagogika osob s parciálními nedostatky - specifické poruchy učení, chování a pozornosti.
- Někteří autoři člení pedagogiku **vertikálně** a **horizontálně**
    - **Horizontální členění** přihlíží se k obsahu disciplíny
    - **Vertikální členění** - vychází se z věkového období objektu výchovy (dítě, žák, student, dospělý člověk)
- Výhodnější je členit pedagogiku po spojení více kritérií (**integrující způsob členění**) základní
    - srovnávací pedagogika
    - metodologie
    - pedagogická diagnostika hraniční
    - pedagogická psychologie
    - sociální pedagogika
    - ekonomie výchovy aplikované
    - předškolní, školní, středoškolská, vojenská ap.

## Aktuální problémy a otázky pedagogiky
### Terminologie pedagogiky
- nepřesná a nejednotná, jinak chápána veřejností, oborníky a ve světě
- veřejnost chápe pedagogiku jako soubor návodů a postupů používaných učitelem při vyučování
- odborníci ji dělí na část teoretickou a praktickou, věda a výzkum zabývající se vzděláváním a výchovou
- problém i v pojetí pedagogiky jako pojmu v různých zemích
- v zahraničí
    - Německo (autonomní monodisciplinární vědní obor orientovaný k filozofii)
    - Francie (sociální věda a multidisciplinární obor)
    - Anglie, USA (ve smyslu multidisciplinárních oborů pokrývajících edukaci)
- **Autorita** je moc uplatňovaná v souladu s hodnotami ovládaných za využití přijatelné a schválené formy, respekt pozitivně či negativně ovlivňující výchovu.
    - Pokud pedagogům schází, vede k rozkladu skupiny a neúčinnosti výchovy
- **Emoce** - psychický stav pramenící ze subjektivního prožívání vztahu k něčemu a někomu, citová vazba mezi učitelem a žáky.
    - Je důležité nepřeceňovat, ani nepodceňovat roli emocí ve výchovně-vzdělávacím procesu
- **Pozornost** - psychický proces, soustředěnost duševní činnosti člověka po určitou dobu na určitý objekt, jev nebo činnost. Problém je udržet si pozornost žáků. Pozitivně tuto schopnost ovlivňuje autorita, motivace a zajímavost výkladu
- **Funkční gramotnost** - vybavenost člověka pro realizaci různých aktivit potřebných pro život v současnosti.
    - Lidé, kteří neumějí číst a psát jsou označováni za negramotné.
    - Problémem zejména rozvojových zemí, u nás negramotnost 1–3 %.Důležité zkoumat příčiny vedoucí k funkční negramotnosti – dva faktory
        - Pedagogické faktory – obsah školního vzdělávání není zaměřen jen na osvojování činností uplatnitelných v praktic. životě
        - Socioekonomické a sociokulturní faktory – u lidí z určitých sociálních vrstev významně vyšší výskyt funkční negramotnosti
- **Multikulturní výchova** - transdisciplinární oblast spojující přístupy pedagogiky, kulturní antropologie, interkulturní psychologie, sociologie. Proces jehož prostřednictvím si jednotlivci mají utvářet dispozice k pozitivnímu vnímání a hodnocení kulturních systémů odlišných od jejich vlastní kultury. Konkrétní vzdělávací program zabezpečující žákům z etnických, rasových, náboženských minorit učební prostředí a vzdělávací obsahy přizpůsobené specifickým jazykovým, kulturním potřebám žáků.
    - Problémem je, zda u takovýchto skupin uplatňovat programy stejné jako pro žáky české nebo zda vytvářet pro tyto žáky programy speciální a specifické. Problémem také samotné uplatňování edukačních konstruktů multikulturní výchovy v edukační realitě - výzkumně prozatím nepostiženo.
    - **Výzkumné problémy této oblasti**:
        - **Spolupráce rodiny a školy**
            - rodina a škola musí spolu komunikovat a kooperovat, oblast teorie partnerství rodiny a školy
            - rodina i škola vzdělávají a vychovávají – základní výchovně-vzdělávací instituce
            - realita je však často odlišná
            - škola často pojímána jako nutnost a rodiče se školou odmítají komunikovat - negativní pro dítě
            - opakem angažovanost rodičů pro školu - pozitivní vliv na dítě
            - vztahy mezi školami a rodiči spíše stagnují, než by byly zintenzivňovány
        - ***Alternativní a inovativní školy***
            - nutnost teoreticky oddělit pojmy alternativní a inovativní škola
            - rozhodujícím kritériem pro posouzení o jakou školu se jedná je odlišnost od standardu převládajícího ve vzdělávacím systému.
            - **alternativní škola** – vzdělávací instituce vyznačující se nějakou pedagogickou specifičností, na rozdíl od standardní, běžné školy- alternativní přístupy pochází většinou ze zahraničí a jejich využití je velmi přínosné ve výchovně-vzdělávacím procesu.
                - pedagogická teorie studuje alternativní školství z hlediska historie:
                - alternativních škol a přístupů (navazuje zejména na reformní školství a nejznámější školy jsou waldorfská, montessoriovská, freinetovská, jenská, daltonská a winnetská)
                - moderních typů alternativních škol (otevřená škola, zdravá škola, magnetová škola, škola bez ročníků, angažované, autentické, prožitkové, aktivní učení)
                - rozdíly mezi státními a soukromými školami (většina alternativních škol jsou školy soukromé)
                - problém – jak efektivně financovat soukromý sektor v oblasti vzdělávání a výchovy
            - **inovativní škola** – škola, která přešla od spontánních dílčích inovací k podstatným systémovým změnám v cílech, metodách a organizaci vyučování.
                - Inovativních škol v ČR velmi málo, jsou prakticky součástí alternativního školství
        - **Evropská dimenze ve vzdělávání**
            - princip prostupující vzdělávacími systémem, podporující chápání širších evropských souvislostí a perspektiv vzdělávání
            - úkol zavádět do školním kurikulu témata umožňující žákům a studentům osvojovat si vědomosti o jiných evropských národech a etnicích
            - programy týkající se vzájemné výměny studentů (TEMPUS, SOCRATES)
            - často však výchova „k evropanství“ bývá na základních školách plně opomenuta
            - zařazovat do souvislosti s „výchovou k lásce k vlasti“
            - **pedagogický výzkum evropské dimenze odhaluje problémy a nové úkoly**:
                - změna myšlení mladých lidí ve směru k evropanství
                - osvojení si pozitivních a tolerantních postojů k příslušníkům jiných národů a etnik
                - projevení pozitivního vlivu tohoto typu vzdělávání na eliminaci předsudků mezi příslušníky jednotlivých národů a etnik
                - oblast není dostatečně zpracována
                - problém - předsudky při přijímání studentů z jiných zemí (rasismus, xenofobie, nižší možnost našich studentů na přijetí na VŠ)
        - **Kázeňské problémy na školách** (zejména šikana)
            - poměrně velký problém se kterým se setkávají učitelé na většině ZŠ
            - existuje několik typologizací kázeňských problémů (**CANGELOSI, J**., Strategie řízení třídy):
                - násilné jednání vůči žákům (šikana)	• násilné chování vůči učitelům.
                - jednání a chování žáka pod vlivem alkoholu	• jednání a chování žáka pod vlivem drog.
                - kouření, pozdní příchody a chození za školu	• podvádění při testech, vandalismus.
            - jednotlivé problémy ještě děleny do dvou oblastí – lehčího a těžšího charakteru.
            - na školách převažují problémy lehčího charakteru (vyrušování, absence domácích úkolů, pozdní příchody do školy).
            - z problémů těžšího charakteru se na školách nejčastěji vyskytuje násilné jednání vůči žákům – fyzické nebo psychické týrání žáků.
            - důležité odhalení projevů a řešení.
            - nutno dodržovat základní pravidla přístupu, odhalování a řešení kázeňských problémů.
            - problematikou šikany se u nás zabývá Bendl, Kolář, Říčan ad.
        - **Environmentální výchova**
            - budování pozitivního vztahu k přírodě, který je základem šetrného a předvídavého chování a jednání
            - děti musí být vedeni k co nejšetrnějšímu chování k přírodě a jednání ve smyslu nutnosti nepoškozovat přírodu
            - výchova a vzdělávání jak ve škole, tak v pomáhajících institucích (např. Lipka), ale klíčové postupně budovat ekologicky šetrné myšlení a jednání u rodičů dětí
            - problém – nevhodná medializace ekologické a environmentální problematiky, převažují názory značně diskutabilní
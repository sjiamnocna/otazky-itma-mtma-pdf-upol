# 6. Temperament, přehled základních temperamentových typologií a možnosti jejich praktického využití (např. Kretschmer, Spranger, Eysenck, Pavlov, Jung). Emotivita a temperament.

> **Temperament**: Vrozená složka osobnosti, která ovlivňuje emocionální reaktivitu a chování. Projevuje se v intenzitě, rychlosti a trvání emočních reakcí.

## Základní temperamentové typologie

### 1. Typologie Hippokratés-Galenos
- **Historický základ**: Založena na tělesných tekutinách (humorech) ovlivňujících psychiku.
- **Čtyři typy**:
  - **Sangvinik**: Optimistický, společenský, aktivní (převaha krve).
  - **Cholerik**: Vznětlivý, energický, rozhodný (převaha žluči).
  - **Melancholik**: Citlivý, introspektivní, pesimistický (převaha černé žluči).
  - **Flegmatik**: Klidný, spolehlivý, pasivní (převaha hlenu).

### 2. Kretschmerova konstituční typologie
- **Typologie podle tělesné stavby**:
  - **Pyknický typ**: Krátký a robustní, sklony k maniodepresivní psychóze.
  - **Atletický typ**: Svalnatý a silný, sklony k epilepsii.
  - **Leptosomní typ**: Štíhlý a vysoký, sklony k schizofrenii.
  - **Dysplastický typ**: Neurčitý typ, směs znaků různých typů.

### 3. Typologie Sprangera
- **Zaměření na hodnoty a životní cíle**:
  - **Teoretický typ**: Zájem o poznání a vědu.
  - **Ekonomický typ**: Zaměřený na užitek a materiální hodnoty.
  - **Estetický typ**: Zaměřený na umění a krásu.
  - **Sociální typ**: Orientovaný na pomoc druhým.
  - **Politický typ**: Usilující o moc a vliv.
  - **Náboženský typ**: Zaměřený na duchovní hodnoty.

### 4. Eysenckova typologie
- **Dvourozměrný model**: Extravert-introvert a stabilita-labilita.
  - **Extraverti**: Společenští, aktivní, optimističtí.
  - **Introverti**: Rezervovaní, tišší, reflektivní.
  - **Stabilní**: Emocionálně vyrovnaní, klidní.
  - **Labilní**: Emocionálně nestabilní, úzkostní.

### 5. Pavlovova typologie
- **Fyziologické základy temperamentu**: Vyváženost a síla nervových procesů (vzrušení a útlum).
  - **Silný, vyvážený, pohyblivý typ**: Rychlé a silné reakce (sangvinik).
  - **Silný, vyvážený, inertní typ**: Pomalé reakce, stálost (flegmatik).
  - **Silný, nevyvážený typ**: Vysoká vzrušivost, neklid (cholerik).
  - **Slabý typ**: Nízká odolnost, citlivost (melancholik).

### 6. Typologie Carla Gustava Junga
- **Základní dimenze**: Introverze a extraverze.
  - **Introvert**: Zaměřený na vnitřní svět myšlenek a pocitů.
  - **Extravert**: Zaměřený na vnější svět a sociální interakce.

## Emotivita a temperament
- **Emotivita**: Schopnost a tendence k prožívání emocí, může být ovlivněna temperamentem.
- **Vliv temperamentu na emotivitu**: Určuje, jak intenzivně a rychle jedinec reaguje na emocionální podněty.

## Praktické využití temperamentových typologií
- **Sebepoznání**: Pomáhá jedincům lépe pochopit své chování a reakce.
- **Personální psychologie**: Využití při výběru zaměstnanců, vedení týmů, poradenství.
- **Vzdělávání**: Pomáhá učitelům pochopit individuální rozdíly mezi studenty a přizpůsobit výuku.
- **Psychoterapie**: Umožňuje terapeutům lépe porozumět klientům a jejich potřebám.
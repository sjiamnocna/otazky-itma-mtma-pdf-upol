# 20. Objasněte principy, na kterých je postavena taxonomie cílů psychomotorických, ěkterou z taxonomií představte. Na konkrétním příkladu prezentujte jednotlivé rovně této taxonomie. V souladu s vaší oborovou zaměřeností formulujte libovolný ýukový cíl, v němž převládá psychomotorická dimenze a kterého lze dosáhnout  rámci výuky ve škole.

#### Principy taxonomie psychomotorických cílů
Taxonomie psychomotorických cílů se zaměřuje na rozvoj pohybových dovedností a manuální zručnosti. Je založena na postupu od vědomé imitace pohybů až po jejich plnou automatizaci. Principy této taxonomie se zaměřují na sekvenční nácvik a zlepšování motorických dovedností prostřednictvím praktických činností a opakování.

#### Daveova taxonomie psychomotorických cílů
Taxonomie výcvikových (psychomotorických) cílů dle R. H. Daveyho zahrnuje následující úrovně:
1. **Imitace (Nápodoba)**
   - Žák po impulsu pozoruje příslušnou činnost a vědomě ji začíná napodobovat na základě vnějších podnětů a pozorování.
   - **Příklad**: Žák napodobí pohyby učitele při základních tanečních krocích.

2. **Manipulace (Praktická cvičení)**
   - Žák je schopen vykonat určitou pohybovou činnost podle slovního návodu, začíná rozlišovat mezi různými činnostmi a zvolí vhodnou nebo požadovanou činnost.
   - **Příklad**: Žák vykoná správnou techniku držení tužky podle pokynů učitele.

3. **Zpřesňování**
   - Žák dokáže vykonávat pohybový úkol s mnohem větší přesností a účinností.
   - **Příklad**: Žák zvládne přesně nakreslit jednoduchý obrázek podle předlohy.

4. **Koordinace**
   - Koordinace několika různých činností řazených za sebou v požadovaném sledu; pohybové výkony jsou vnitřně konzistentní.
   - **Příklad**: Žák provede sérii gymnastických cviků v přesném pořadí a rytmu.

5. **Automatizace**
   - Automatizace pohybů, které vedou k maximální účinnosti při minimálním vynaložení energie.
   - **Příklad**: Žák automaticky píše jednoduchý text s plynulým a správným držením tužky.

#### Výukový cíl s psychomotorickou dimenzí
##### Tematický celek: Tělesná výchova
- **Výukový cíl**: Žák provede základní gymnastické cviky (kotoul, přemet, stoj na rukou) s plynulým přechodem mezi jednotlivými cviky.
- **Klíčová kompetence**: Motorické dovednosti a tělesná zdatnost.

Tento cíl zahrnuje několik úrovní Daveovy taxonomie, počínaje imitací (žák napodobuje cviky), přes manipulaci (žák vykonává cviky podle pokynů), zpřesňování (žák provádí cviky přesněji), koordinaci (plynulý přechod mezi cviky) až po automatizaci (automatické provádění cviků). Rozvíjí se zde především motorické dovednosti žáků, což je klíčová kompetence v tělesné výchově.


# 13. Prezentujte výukové metody a jejich klasifikaci (dle vybraného autora). Prezentujte rganizační formy výuky a jejich klasifikaci. Demonstrujte možnosti využití vhodných etod a forem výuky ve výchovně vzdělávací činnosti.

## Výukové metody a jejich klasifikace
Výukovou metodu můžeme definovat jako specifickou činnost učitele, která rozvíjí vzdělanost žáků a vede je k dosahování stanovených výchovně-vzdělávacích cílů. Podle J. Maňáka (2001) lze výukové metody klasifikovat do následujících skupin:

1. **Metody z hlediska pramene poznání a typu poznatků (aspekt didaktický)**
   - **Metody slovní**
     - Monologické metody (vysvětlování, výklad, přednáška)
     - Dialogické metody (rozhovor, dialog, diskuse)
     - Metody písemných prací (písemná cvičení, kompozice)
     - Metody práce s učebnicí, knihou, textovým materiálem
   - **Metody názorně demonstrační**
     - Pozorování předmětů a jevů
     - Předvádění, demonstrace statických obrazů
     - Projekce statická a dynamická
   - **Metody praktické**
     - Nácvik pracovních a pohybových dovedností
     - Laboratorní činnost žáků
     - Pracovní činnost
     - Grafické a výtvarné činnosti

2. **Metody z hlediska aktivity a samostatnosti žáků (aspekt psychologický)**
   - Metody sdělovací
   - Metody samostatné práce žáků
   - Metody badatelské, výzkumné, problémové

3. **Metody z hlediska myšlenkových operací (aspekt logický)**
   - Postup srovnávací
   - Postup induktivní
   - Postup deduktivní
   - Postup analyticko-syntetický

4. **Metody z hlediska fází výchovně-vzdělávacího procesu (aspekt procesuální)**
   - Metody motivační
   - Metody expoziční
   - Metody fixační
   - Metody diagnostické
   - Metody aplikační.

## Organizační formy výuky a jejich klasifikace
Organizační forma výuky je vnějším organizačním rámcem vyučovacího procesu. Podle Kalhouse a Obsta (2002) jsou organizační formy výuky rozděleny do následujících kategorií:

1. **Individuální výuka**
   - Vyučování jednotlivců nebo malého počtu žáků
   - Jeden učitel řídí činnost jednoho žáka
   - Žáci různého věku a různých dovedností

2. **Hromadná a frontální výuka**
   - Jeden učitel vyučuje souběžně větší počet žáků stejného věku
   - Třídy v časových jednotkách, žáci plní stejné úkoly ve stejnou dobu

3. **Individualizovaná výuka**
   - Přizpůsobení práce každému žákovi na základě jeho možností
   - Individualizace výuky, přizpůsobení tempa učení, obsahu a organizace učební činnosti

4. **Projektová výuka**
   - Žáci za pomoci učitele řeší úkol komplexního charakteru – projekt.

## Možnosti využití vhodných metod a forem výuky ve výchovně vzdělávací činnosti
1. **Pozorování a experimenty v přírodovědě**
   - Metody názorně demonstrační a praktické mohou být využity k pozorování přírodních jevů a experimentování, což umožňuje žákům lépe pochopit učivo skrze přímou zkušenost.

2. **Projektová výuka v humanitních předmětech**
   - Projektová výuka může být využita k rozvoji kritického myšlení a spolupráce mezi žáky. Například projekt na téma historie může zahrnovat výzkum, prezentaci a diskusi, což podporuje hlubší porozumění a zapojení žáků.

3. **Individuální a individualizovaná výuka v matematice**
   - Přizpůsobení tempa učení a obsahu výuky každému žákovi může zvýšit jejich úspěšnost a sebedůvěru. Například používání speciálních učebních materiálů a individuální konzultace s učitelem.

4. **Diskusní metody ve společenských vědách**
   - Diskuse a dialogické metody podporují aktivní zapojení žáků, rozvoj argumentačních schopností a kritického myšlení. Témata jako etika a občanská výchova mohou být diskutována v rámci hodin, čímž se podporuje jejich porozumění a aplikace v reálném životě.
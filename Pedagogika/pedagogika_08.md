# 8. Charakterizujte prostředí školy. Uveďte možné inspirace alternativními pedagogickými systémy pro zefektivnění výchovného procesu.
- Škola je instituce ve veřejném nebo soukromém vlastnictví, která zajišťuje řízenou a systematickou edukaci.
- Je v ní poskytováno systematické vyučování vědomostí a dovedností, orientuje se na příslušné formy vzdělávání a výchovy a usiluje o uskutečňování vymezených cílů vzdělávání a výchovy.
- Škola přispívá k celkovému rozvoji jedince, působí jako ochranné zařízení vedoucí mládež k hodnotám a funguje jako formovatel lidí.

**Prostředí školy se dělí na**:
1. **Vnitřní vlivy** - Působí uvnitř školy a mohou být ve velké míře ovlivněny vedením školy.
    - **Mikroprostředí** zahrnuje žáky, partnery školy, sponzory, rodiče atd. Významným faktorem vnějšího mikroprostředí je veřejnost.
2. **Vnější vlivy** - Dělí se na mikroprostředí a makroprostředí okolí školy.
    - **Makroprostředí** odráží trendy vývoje celé společnosti, na které mají vliv jevy ekonomické, demografické, politické a kulturní.

**Školní prostředí představuje objektivní realitu, která se odráží v subjektivním vnímání, prožívání a hodnocení lidí, kteří jsou jeho součástí. Ke školnímu prostředí patří následující dimenze:**

- **Ekologická dimenze**: materiální a estetické aspekty školy (architektura školy, vybavení a uspořádání včetně přilehlých prostor - hřiště, zahrada atd.)  .
- **Společenská dimenze**: je dána lidmi ve škole (žáci, učitelé, rodiče, vedení školy a provozní personál).
- **Sociální dimenze**: způsob komunikace a kooperace mezi jednotlivci i skupinami.
- **Kulturní dimenze**: výsledky historické činnosti člověka - civilizace, náboženství, filosofie, zvyky a obyčeje.

#### Možné inspirace alternativními pedagogickými systémy
Pro zefektivnění výchovného procesu mohou být inspirací následující alternativní pedagogické systémy:
1. **Pedocentrismus** - Orientace na dítě, respektování jeho individuality a potřeb.
2. **Samostatná práce dítěte** - Podpora autonomie a odpovědnosti žáků za vlastní učení.
3. **Učitel jako průvodce** - Učitel je spíše facilitátorem než autoritativním zdrojem znalostí.
4. **Didaktické pomůcky** - Využití různých pomůcek a materiálů pro podporu výuky.
5. **Témata, která žáky aktuálně zajímají** - Zařazování aktuálních a relevantních témat do výuky.
6. **Sebehodnocení** - Podpora reflexe a sebehodnocení žáků, což posiluje jejich kritické myšlení a sebeuvědomění   .

Tyto přístupy mohou přispět k vytvoření dynamického a motivujícího prostředí, které podporuje aktivní zapojení žáků do výuky a jejich osobnostní růst.
# 5. Objasněte kategorii osobnost učitele. Uveďte požadavky kladené na osobnost učitele. Uveďte aktuální problémy pedagogické profese. Prezentujte možné typologie učitele a vývoj profesní dráhy učitelů. Charakterizujte kázeň a autoritu učitele ve výchově. Popište možnosti reflexe a sebereflexe v kontextu problematiky syndromu vyhoření u pedagogických pracovníků.

## Kategorie osobnost učitele
- **Osobnost učitele** je komplexní a zahrnuje široké spektrum vlastností, schopností a dovedností.
- Zahrnuje intelektuální, emocionální, sociální a etické aspekty.
- Klíčové komponenty:
  - **Kognitivní kompetence**: znalosti a intelektuální schopnosti.
  - **Emoční stabilita**: zvládání stresu a emocí.
  - **Sociální kompetence**: schopnost komunikace a spolupráce.
  - **Morální a etické hodnoty**: integrita a etické jednání.

## Požadavky kladené na osobnost učitele
- **Profesní kompetence**: odborné znalosti předmětu, pedagogické a didaktické dovednosti.
- **Psychologické kompetence**: empatie, trpělivost, schopnost motivovat a podporovat studenty.
- **Komunikační dovednosti**: efektivní verbální i neverbální komunikace, aktivní naslouchání.
- **Organizační schopnosti**: plánování, řízení času, schopnost adaptovat se na změny.
- **Etické a morální vlastnosti**: spravedlnost, zodpovědnost, důvěryhodnost.

## Aktuální problémy pedagogické profese
- **Stres a pracovní přetížení**: vysoké nároky na učitele, nedostatek času na přípravu.
- **Nízké finanční ohodnocení**: často neadekvátní odměna vzhledem k náročnosti povolání.
- **Nedostatek podpory**: málo zdrojů a podpory od vedení školy a společnosti.
- **Změny ve školských systémech**: časté reformy a změny kurikula.

## Možné typologie učitele
- **Podle stylu výuky**:
  - **Autoritativní učitel**: přísný, vyžaduje disciplínu.
  - **Demokratický učitel**: podporuje spolupráci a diskusi.
  - **Laissez-faire učitel**: nechává studentům volnost.
- **Podle přístupu k žákům**:
  - **Empatický učitel**: chápe a podporuje žáky.
  - **Analytický učitel**: zaměřuje se na logiku a analýzu.
  - **Kreativní učitel**: podporuje tvořivost a inovaci.

## Vývoj profesní dráhy učitelů
- **Začínající učitel**: adaptace na školní prostředí, získávání základních zkušeností.
- **Zkušený učitel**: rozvoj pedagogických dovedností, stabilizace profesních kompetencí.
- **Mentor a školitel**: sdílení zkušeností s kolegy, vedení začínajících učitelů.
- **Odborník a lídr**: vedení školních projektů, inovace a rozvoj školních programů.

## Kázeň a autorita učitele ve výchově
- **Kázeň**:
  - Důležité pro udržení pořádku a efektivní výuky.
  - Založena na jasných pravidlech a konzistentním přístupu.
- **Autorita**:
  - Respekt a důvěra studentů.
  - Získává se odborností, spravedlností a příkladem.

## Reflexe a sebereflexe v kontextu problematiky syndromu vyhoření u pedagogických pracovníků
- **Reflexe**:
  - Pravidelné hodnocení vlastní práce a jejích výsledků.
  - Zpětná vazba od kolegů a studentů.
- **Sebereflexe**:
  - Osobní úvahy nad vlastními pedagogickými postupy a jejich efektivitou.
  - Hledání způsobů, jak zlepšit vlastní praxi.
- **Syndrom vyhoření**:
  - Příznaky: vyčerpání, cynismus, pokles profesní výkonnosti.
  - Prevence: dostatek odpočinku, podpora, profesní rozvoj.
  - Možnosti řešení: profesní poradenství, supervize, změna pracovního prostředí.
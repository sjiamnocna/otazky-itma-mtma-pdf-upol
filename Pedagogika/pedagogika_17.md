# 17. Definujte pojem „výukový cíl“. Vyjmenujte požadavky na formulaci výukových cílů, by jich mohlo být ve výuce co nejefektivněji dosaženo. Vysvětlete pojem aktivní (aktivizační) slovesa a v kontextu vaší oborové specializace uveďte čtyři konkrétní říklady jejich použití při formulaci výukového cíle. S využitím zvolené taxonomie yhodnoťte náročnost těchto cílů.

## Výukový cíl
Výukový cíl chápeme jako představu o kvalitativních i kvantitativních změnách u jednotlivých žáků v oblasti kognitivní, afektivní a psychomotorické, kterých má být dosaženo ve stanoveném čase v procesu výuky. Je to představa o stavu, kterého má být dosaženo v určitém čase, nějaký výstup, výsledek, k němuž směřuje učitel během vyučovacího procesu. Jejich stanovení vede k efektivnějšímu řízení výuky a lepším výkonům a výsledkům žáků. Není na závadu, když se spolutvůrci výukových cílů stanou sami žáci.

## Požadavky na formulaci výukových cílů
- **Komplexní**
    - Musejí být vyjádřeny všechny tři roviny
        - kognitivní
        - psychomotorická
        - afektivní
- **Konzistentní**
    - Cíle musí být soudržné - provázané
    - Jdeme od nejjednodušších cílů ke složitějším
- **Kontrolovatelné**
    - Cíl má vlastnost umožňující kontrolu jeho splnění
- **Jednoznačné**
    - Cíl musí být stanoven tak, aby nebylo možné jiné interpretace.
    - Má jediný a přesný význam, který neumožňuje jiný výklad nebo řešení
- **Přiměřené**
    - Cíle mají být náročné, ale současně i splnitelné pro většinu žáků
    - Při sestavování cílů nesmí učitel zapomenout na rozdílné mentální, afektivní a psychomotorické úrovně jednotlivých žáků
    
- Požadovaný výkon - Aktivní sloveso
- Podmínky výkonu - Metody, podmínky, pomůcky
- Norma výkonu - Počet příkladů, rozsah

## Aktivní (aktivizační) slovesa
Aktivní slovesa jsou slova z Bloomovy taxonomie, která se používají k vymezení výukových cílů a zdůrazňují aktivní výkon žáka. Specifikují přímo činnost realizace cíle a ideálně volíme dokonavé tvary, které vyjadřují ukončenost děje z pohledu do budoucna, například: napíše, objasní, demonstruje, analyzuje.

## Příklady použití aktivních sloves v kontextu oborové specializace
1. **Žák se správnou výslovností vyjmenuje základní barvy** (Zapamatování - znalost)
   - Vyjadřuje schopnost zapamatovat si a správně reprodukovat informace.
   - **Náročnost**: Nízká, protože se jedná o zapamatování a reprodukci základních informací.
2. **Žák vlastními slovy vysvětlí význam určených anglických výrazů** (Porozumění)
   - Vyjadřuje schopnost porozumět a vysvětlit obsah vlastním způsobem.
   - **Náročnost**: Střední, protože zahrnuje pochopení a interpretaci informací.
3. **Žák aplikuje pravidla při tvoření gerundiového tvaru sloves** (Aplikace)
   - Vyjadřuje schopnost použít získané znalosti v konkrétních situacích.
   - **Náročnost**: Vyšší, protože zahrnuje aplikaci pravidel na nové příklady.
4. **Žák v textu vyhledá příklady nepravidelných sloves** (Analýza)
   - Vyjadřuje schopnost rozpoznat a identifikovat nepravidelná slovesa v kontextu.
   - **Náročnost**: Vysoká, protože vyžaduje analýzu a rozlišení konkrétních informací v textu.

## Taxonomie výukových cílů (Bloomova taxonomie)
1. **Zapamatování**: Definovat, popsat, vyjmenovat.
2. **Porozumění**: Vysvětlit, interpretovat, klasifikovat.
3. **Aplikace**: Použít, demonstrovat, implementovat.
4. **Analýza**: Analyzovat, rozlišit, kategorizovat.
5. **Hodnocení**: Posoudit, kritizovat, obhájit.
6. **Syntéza**: Tvořit, navrhnout, sestavit.
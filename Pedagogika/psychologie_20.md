# 20. Socializace, formy socializace a její činitelé. Sociální učení. Sociální postoje. Struktura, zjišťování, utváření a možnosti změny. Stereotypy a předsudky.

> Socializace je proces, kterým jedinec získává a internalizuje hodnoty, normy, dovednosti a znalosti potřebné pro efektivní fungování v dané společnosti. Tento proces probíhá po celý život a ovlivňuje jak individuální vývoj, tak společenské chování.

## Formy socializace

1. **Primární socializace**:
   - Probíhá v raném dětství, hlavně v rodinném prostředí.
   - Základní dovednosti, hodnoty a normy jsou předávány prostřednictvím rodičů nebo pečovatelů.
   - Zahrnuje základní sociální dovednosti, jako je jazyk, sociální interakce a základní hodnoty.

2. **Sekundární socializace**:
   - Probíhá později v životě, hlavně ve školním prostředí, mezi vrstevníky a v dalších sociálních skupinách.
   - Rozšíření a prohloubení hodnot a norem, které byly získány během primární socializace.
   - Zahrnuje učení se specifickým sociálním rolím, jako je role studenta, zaměstnance nebo občana.

3. **Terciární socializace**:
   - Probíhá během dospělosti a zahrnuje adaptaci na nové role a situace, jako je zaměstnání, manželství nebo rodičovství.
   - Zahrnuje učení nových dovedností a adaptaci na změny v životním stylu nebo prostředí.

## Činitelé socializace

1. **Rodina**:
   - Hlavní činitel primární socializace, kde jsou jedinci poprvé konfrontováni s hodnotami, normami a rolemi.
   - Ovlivňuje základní vzorce chování, jazyk, kulturní hodnoty a emocionální vývoj.

2. **Škola**:
   - Klíčový činitel sekundární socializace, poskytuje formální vzdělání a přípravu na profesionální život.
   - Učí specifické znalosti a dovednosti, podporuje sociální interakce a rozvíjí smysl pro disciplínu a odpovědnost.

3. **Vrstevníci**:
   - Významný činitel socializace, zvláště v období adolescence.
   - Ovlivňují módní trendy, chování, hodnoty a postoje.

4. **Média**:
   - Mají silný vliv na formování názorů, hodnot a představ o světě.
   - Můžou posilovat nebo oslabovat společenské normy, stereotypy a předsudky.

5. **Náboženství a kultura**:
   - Poskytují hodnotový rámec, normy a rituály, které ovlivňují chování a myšlení jedince.

## Sociální učení

- **Definice**: Proces, kterým se jedinci učí sociální chování pozorováním a napodobováním ostatních, stejně jako prostřednictvím přímého učení.
- **Mechanismy**:
  - **Imitace**: Napodobování chování modelů, jako jsou rodiče, učitelé nebo média.
  - **Posilování**: Proces, kdy jsou určitá chování posilována (odměňována) nebo tlumena (trestána).
  - **Observační učení (Bandura)**: Jedinci se učí novým chováním pozorováním modelů a jejich následků.

## Sociální postoje

### Struktura
- **Komponenty postojů**:
  1. **Kognitivní složka**: Přesvědčení a názory, které jedinec má o daném objektu nebo situaci.
  2. **Emoční složka**: Pocity a emoce, které jedinec k objektu nebo situaci cítí.
  3. **Behaviorální složka**: Tendence jednat určitým způsobem vzhledem k objektu nebo situaci.

### Zjišťování a utváření
- **Metody zjišťování**: Dotazníky, rozhovory, pozorování chování, experimenty.
- **Utváření postojů**: Postoje se mohou utvářet na základě osobních zkušeností, socializace, kulturního vlivu nebo informačního zpracování.

### Možnosti změny
- **Persuaze**: Úsilí ovlivnit postoje prostřednictvím komunikace a argumentace.
- **Kognitivní disonance**: Nepříjemný stav způsobený konfliktem mezi dvěma protichůdnými postoji nebo mezi postojem a chováním, který může vést ke změně postojů nebo chování.

## Stereotypy a předsudky

### Stereotypy
- **Definice**: Zjednodušené a pevné představy o skupině lidí, které jsou často nepřesné a negativní.
- **Funkce**: Stereotypy mohou sloužit k rychlému zpracování informací o ostatních, ale mohou také vést k zkreslenému vnímání a diskriminaci.

### Předsudky
- **Definice**: Negativní postoje nebo pocity vůči určité skupině lidí, založené na stereotypech.
- **Důsledky**: Předsudky mohou vést k diskriminaci, marginalizaci a sociální nespravedlnosti.

### Boj proti stereotypům a předsudkům
- **Vzdělávání a osvěta**: Zvýšení povědomí o negativních důsledcích stereotypů a předsudků.
- **Interkulturní dialog**: Podpora otevřeného dialogu mezi různými skupinami a kulturami.
- **Legislativní opatření**: Zákony proti diskriminaci a rasismu.

> Socializace a související procesy, jako jsou sociální učení, postoje, stereotypy a předsudky, hrají klíčovou roli v utváření sociální identity a interakcí. Porozumění těmto konceptům je nezbytné pro podporu sociálního začlenění a rovnosti.
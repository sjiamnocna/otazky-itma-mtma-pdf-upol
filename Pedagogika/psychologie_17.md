# 17. Psychologická charakteristika období dospívání – adolescence (identita, kognitivní, emocionální a sociální vývoj).

> **Adolescence** je období, které následuje po pubertě a trvá **přibližně od 12–13 do 18–20 let**. Je to fáze, kdy jedinec přechází od dětství k dospělosti, a je charakterizována intenzivním fyzickým, kognitivním, emocionálním a sociálním vývojem. Klíčovým úkolem tohoto období je nalezení a formování identity.

## Identita
- **Formování identity**: Hlavním úkolem adolescence je dosažení stabilní a soudržné identity. Dospívající zkoumají různé aspekty svého já, včetně hodnot, přesvědčení, sexuální orientace, kariérních cílů a sociálních rolí.
- **Eriksonova teorie identity vs. zmatení rolí**: Erik Erikson popsal tuto fázi jako konflikt mezi hledáním identity a zmatením ohledně rolí a budoucnosti. Úspěšné zvládnutí tohoto konfliktu vede k vytvoření jasné identity, zatímco neúspěch může vést k nejistotě a roztříštěné identitě.
- **Sebereflexe a introspekce**: Dospívající věnují více času přemýšlení o sobě, svých pocitech a názorech. Tato introspekce je klíčová pro rozvoj osobní identity.

## Kognitivní vývoj
- **Rozvoj formálních operací (Piaget)**: Dospívající získávají schopnost abstraktního myšlení, což zahrnuje schopnost uvažovat o hypotetických situacích, plánovat do budoucna a zvažovat různé možnosti a jejich důsledky.
- **Metakognice**: Zlepšuje se schopnost přemýšlet o vlastním myšlení, strategizovat a plánovat. Dospívající se stávají schopnými analyzovat své vlastní myšlenkové procesy a zlepšovat se v učení a rozhodování.
- **Kritické myšlení**: Zvyšuje se schopnost analyzovat informace, kriticky hodnotit argumenty a zpochybňovat autority a zavedené normy.
- **Rozvoj etických a morálních hodnot**: Dospívající začínají zkoumat a utvářet své vlastní morální a etické hodnoty, což často zahrnuje zpochybňování hodnot rodiny nebo společnosti.

## Emoční vývoj
- **Emoční intenzita a regulace**: Adolescence je charakterizována silnými a často proměnlivými emocemi. Dospívající se učí regulovat své emoce a vyrovnávat se s negativními pocity, jako je úzkost nebo deprese.
- **Hledání autonomie**: Dospívající se snaží osamostatnit a získat nezávislost od rodičů, což může vést k konfliktům, ale je nezbytné pro dosažení dospělé identity.
- **Vývoj empatie a emoční inteligence**: Dospívající zlepšují svou schopnost rozumět emocím druhých a reagovat na ně, což je klíčové pro budování a udržování vztahů.

## Sociální vývoj
- **Vrstevnické vztahy**: Vrstevníci hrají klíčovou roli v životě dospívajících. Skupiny vrstevníků poskytují podporu, možnost sdílení zážitků a prostředí pro experimentování s různými rolemi. Mohou také sloužit jako zdroj tlaku a sociálních norem.
- **Romantické vztahy**: Dospívající začínají zkoumat romantické a sexuální vztahy, které jsou důležité pro rozvoj intimity a sexuální identity.
- **Sociální srovnávání**: Dospívající se často srovnávají s ostatními, což může ovlivnit jejich sebeúctu a sebehodnocení.
- **Rodičovské vztahy**: I když dospívající hledají nezávislost, vztah s rodiči zůstává důležitým zdrojem podpory a ovlivňuje jejich sociální a emoční vývoj. Dospívající často vyjednávají nové formy vztahu s rodiči, které zahrnují větší autonomii.

> Adolescence je klíčovým obdobím, které připravuje jedince na dospělost. Je to čas intenzivního osobního růstu, během kterého si dospívající vytvářejí vlastní identitu, hodnoty a cíle, zatímco se učí zvládat nové sociální a emocionální výzvy.
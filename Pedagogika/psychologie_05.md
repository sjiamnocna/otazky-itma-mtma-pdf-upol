# 5. Poznávací procesy a jejich charakteristika – pozornost a vědomí, myšlení a inteligence.

## Pozornost
- Schopnost zaměřit se na určité podněty nebo činnosti, zatímco ostatní jsou ignorovány.

### Druhy pozornosti
- **Záměrná (aktivní)**: Vědomé zaměření pozornosti na určité objekty nebo úkoly.
- **Bezděčná (pasivní)**: Automatické zaměření pozornosti na nové nebo silné podněty.
- **Rozdělená**: Schopnost věnovat pozornost více než jednomu podnětu současně.
- **Selektivní**: Zaměření se na jeden podnět mezi mnoha jinými.

### Charakteristiky pozornosti
- **Trvání**: Délka času, po který je možné udržet pozornost.
- **Intenzita**: Míra koncentrace na daný podnět.
- **Rozsah**: Počet podnětů, na které je možné se soustředit najednou.
- **Flexibilita**: Schopnost přenášet pozornost mezi různými podněty.

### Poruchy pozornosti
- **Hyperprosexie**: Nadměrné zaměření pozornosti na určitou oblast.
- **Hypoprosexie**: Snížená schopnost udržet pozornost.
- **Aprosexie**: Úplná ztráta schopnosti zaměřit pozornost.
- **Paraprosexie**: Nesprávné zaměření pozornosti.

## Vědomí
- Stav aktivace mozku, zahrnující uvědomění si sebe a okolního světa.

### Funkce vědomí
- **Prožívání**: Uvědomování si psychických procesů, emocí a okolí.
- **Uvědomování si sebe sama**: Schopnost reflektovat vlastní myšlenky a pocity.

### Úrovně vědomí
- **Předvědomí**: Informace a vzpomínky, které nejsou aktuálně v centru pozornosti, ale mohou být snadno vyvolány.
- **Nevědomí**: Procesy a obsahy mimo vědomé uvědomění, které ovlivňují chování a prožívání.

## Myšlení
- **Myšlení** je ognitivní proces zahrnující zpracování informací, řešení problémů a tvorbu nových nápadů.

### Druhy myšlení
- **Konkrétní**: Zaměřené na specifické objekty a události.
- **Abstraktní**: Práce s obecnými pojmy a vztahy.
- **Deduktivní**: Postup od obecných principů k specifickým případům.
- **Induktivní**: Zobecňování na základě jednotlivých případů.

### Myšlenkové operace
- **Analýza**: Rozklad celku na části.
- **Syntéza**: Spojení jednotlivých částí do celku.
- **Abstrakce**: Vyčlenění podstatných vlastností.
- **Generalizace**: Zobecnění na základě jednotlivých případů.

## Inteligence
### Definice
- **Inteligence**: Schopnost učení se, řešení problémů a adaptace na nové situace.

### Složky inteligence
- **Analytická inteligence**: Schopnost kritického myšlení a řešení problémů.
- **Kreativní inteligence**: Schopnost inovativního myšlení a tvorby nových řešení.
- **Praktická inteligence**: Schopnost adaptace na okolní podmínky a využívání zkušeností v praxi.

### Měření inteligence
- **IQ testy**: Testy zaměřené na měření různých kognitivních schopností.
- **Emoční inteligence**: Schopnost rozpoznávat, rozumět a řídit vlastní emoce i emoce ostatních.

### Dědičnost a vliv prostředí
- **Dědičnost**: Inteligence je částečně dědičná, odhadovaný podíl dědičnosti je kolem 75%.
- **Prostředí**: Kromě genetických faktorů inteligenci ovlivňují také výchova, vzdělání a zkušenosti.
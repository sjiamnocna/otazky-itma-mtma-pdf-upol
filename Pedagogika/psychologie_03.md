# 3. Psychologická charakteristika osobnosti (univerzální znaky), determinace lidské psychiky (biologická, sociálně-kulturní a duchovní-Frankl, psychologický determinismus, nativistické a environmentální teorie). Přístupy ke studiu osobnosti (biologický, experimentální, psychometrický, sociální).

## Univerzální znaky osobnosti
1. **Temperament**: Biologicky podmíněná složka, vrozená a neměnná, ovlivňuje reaktivitu a emoční odpovědi.
2. **Rysy**: Trvalé vlastnosti, které ovlivňují chování jedince, jako je extroverze nebo introverze.
3. **Motivy**: Vnitřní hnací síly, které směřují chování k dosažení určitých cílů.
4. **Kognice**: Schopnosti spojené s myšlením, učením a pamětí.
5. **Inteligence**: Schopnost řešit problémy, přizpůsobovat se a učit se z nových situací.

## Determinace lidské psychiky
### Biologická determinace
- **Genetické faktory**: Dědičnost, genotyp, fenotyp, nervová soustava, neurotransmitery.
- **CNS a endokrinní systém**: Vlivy na psychické a fyziologické procesy, hormony, stresové reakce.

### Sociálně-kulturní determinace
- **Socializace**: Proces začlenění jedince do společnosti, osvojování norem a rolí.
- **Enkulturace**: Adaptace a přijetí kulturních hodnot, zvyků a norem.
- **Akulturace a asimilace**: Vlivy majoritní a minoritní kultury na jedince.

### Duchovní determinace
- **Viktor E. Frankl - Logoterapie**: Hledání smyslu života, duchovní dimenze jako zdroj motivace a smyslu.
- **Psychologický determinismus**: Vliv vrozených mechanismů a prostředí na chování a prožívání.

### Nativistické teorie
- **Vrozené predispozice**: Předurčené vlastnosti a mechanismy, jako jsou pudy a instinkty.

### Environmentální teorie
- **Vliv prostředí**: Vnější faktory a sociální vlivy formující psychiku jedince, zkušenosti a výchova.

## Přístupy ke studiu osobnosti
### Biologický přístup
- **Neurofyziologické procesy**: Vlivy mozku a nervové soustavy na chování a prožívání.

### Experimentální přístup
- **Vědecký výzkum**: Používání experimentů k pochopení psychických procesů a jejich vlivů.

### Psychometrický přístup
- **Měření psychických jevů**: Použití psychologických testů k hodnocení osobnostních rysů, inteligence a dalších schopností.

### Sociální přístup
- **Vliv společnosti**: Zkoumání vlivu sociálních interakcí a kontextu na chování a psychiku jedince.
- **Sociometrie**: Měření vztahů a atmosféry ve skupinách, analýza sociálních struktur.
# 19. Objasněte principy, na kterých jsou postaveny taxonomie cílů afektivních, některou  taxonomií představte. Ke konkrétnímu tematickému celku/výukové jednotce aformulujte libovolný výukový cíl, v němž převládá postojová dimenze, a uveďte, terá klíčová kompetence je tímto především rozvíjena.

## Principy taxonomie cílů afektivních
Taxonomie cílů afektivních se zaměřuje na hodnoty, postoje a emocionální aspekty učení. Tyto taxonomie jsou založeny na postupném zvnitřňování hodnot vychovávaných subjektů, což znamená, že jedinec postupně přijímá, reaguje, oceňuje a integruje určité hodnoty do svého systému. Afektivní cíle tedy reflektují změny v emocionálním a hodnotovém vývoji žáka.

## Kratwohlova taxonomie cílů v afektivní oblasti
Kratwohl a jeho kolegové představili taxonomii afektivních cílů, která pracuje s pěti základními úrovněmi:
- taxonomie afektivních (postojových) cílů je budována na postupném zvnitřňování hodnot vychovávaných subjektů
- **Vnímání**
    - Žák je připraven přijímat určitou hodnotu
- **Reagování**
    - Žák je schopen na hodnotu reagovat•
- **Hodnocení**
    - Žák začíná být o hodnotě vnitřně přesvědčen
- **Integrování**
    - Hodnota se zařazuje do žebříčku hodnot žáka
- **Interiorizace** (**zvnitřnění**)
    - Hodnota se stala součástí filozofie žáka

## Formulace výukového cíle s postojovou dimenzí
### Tematický celek: Environmentální výchova
- **Výukový cíl**: Žák projevuje aktivní zájem o ochranu životního prostředí a účastní se školních projektů zaměřených na recyklaci.
- **Klíčová kompetence**: Environmentální gramotnost a občanská kompetence.

Tento cíl se zaměřuje na oceňování a reagování, kde žák nejenže rozumí důležitosti ochrany životního prostředí, ale také aktivně přispívá k environmentálním iniciativám. Rozvíjí se zde především environmentální gramotnost a občanská kompetence, což vede k posílení odpovědného a aktivního přístupu k životnímu prostředí a komunitě.
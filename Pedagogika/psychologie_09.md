# 9. Metody vývojové psychologie – výzkumné přístupy (průřezový a longitudinální přístup), metody vývojové psychologické diagnostiky.

## Výzkumné přístupy

### 1. Průřezový přístup (Cross-sectional Approach)
- **Popis**: Tento přístup zkoumá různé věkové skupiny v jednom časovém okamžiku. Každá skupina reprezentuje různé fáze vývoje.
- **Výhody**:
  - Rychlost a nižší náklady, protože data jsou shromážděna najednou.
  - Umožňuje porovnat různé věkové skupiny ve stejném čase.
- **Nevýhody**:
  - Riziko zkreslení vlivem kohortových efektů (rozdíly mezi skupinami mohou být způsobeny různými historickými a kulturními zkušenostmi, nikoli věkem).
  - Nelze sledovat individuální vývoj a změny u jedinců.

### 2. Longitudinální přístup (Longitudinal Approach)
- **Popis**: Tento přístup sleduje stejné jedince nebo skupiny po delší časové období, aby se zjistily změny a vývoj v průběhu času.
- **Výhody**:
  - Umožňuje sledovat individuální vývoj a změny v průběhu času.
  - Poskytuje hlubší pochopení dlouhodobých efektů a trajektorií vývoje.
- **Nevýhody**:
  - Vysoké náklady a časová náročnost.
  - Možnost ztráty účastníků (tzv. attrition) během studie, což může ovlivnit výsledky.
  - Riziko, že výsledky mohou být ovlivněny změnami v historickém nebo sociálním kontextu během trvání studie.

## Metody vývojové psychologické diagnostiky

### 1. Pozorování
- **Přímé pozorování**: Systematické sledování chování v přirozeném prostředí (např. doma, ve škole).
- **Strukturované pozorování**: Sledování chování v kontrolovaných podmínkách (např. laboratoř).
- **Výhody**: Možnost zachytit přirozené chování, cenné pro analýzu interakcí a kontextuální vlivy.
- **Nevýhody**: Subjektivita pozorovatele, možné ovlivnění chování účastníků přítomností pozorovatele (tzv. Hawthornský efekt).

### 2. Experimentální metody
- **Kontrolované experimenty**: Manipulace s jednou nebo více proměnnými za účelem zjištění jejich efektů na vývojové změny.
- **Polopřírodní experimenty**: Kombinace experimentálních a přirozených podmínek, například sledování efektů přechodu do nové školy.
- **Výhody**: Možnost kontroly nad proměnnými, což zvyšuje vnitřní validitu.
- **Nevýhody**: Omezená ekologická validita, protože podmínky mohou být umělé.

### 3. Testy a škály
- **Standardizované testy**: Například inteligenční testy, vývojové testy (Bayley Scales of Infant Development), které měří specifické schopnosti a dovednosti.
- **Dotazníky a inventáře**: Používány k hodnocení osobnostních rysů, emočního vývoje, sociálních dovedností.
- **Výhody**: Standardizace umožňuje srovnání výsledků mezi jednotlivci a skupinami.
- **Nevýhody**: Možné zkreslení odpovědí, omezení možností zachycení komplexních procesů.

### 4. Klinické metody
- **Individuální rozhovory**: Podrobné rozhovory s jednotlivci zaměřené na pochopení jejich zkušeností, myšlenek a pocitů.
- **Case study (případové studie)**: Hluboké zkoumání jednotlivých případů, často v klinickém kontextu.
- **Výhody**: Hluboký vhled do individuálních případů, flexibilita.
- **Nevýhody**: Omezená generalizovatelnost výsledků, časová náročnost.

### 5. Longitudinální a retrospektivní studie
- **Longitudinální studie**: Sledování stejných jedinců po delší časové období.
- **Retrospektivní studie**: Analyzování minulých událostí nebo vývojových trajektorií pomocí zpětného pohledu, například analýza dětských záznamů u dospělých pacientů.
- **Výhody**: Možnost sledovat vývojové trajektorie a dlouhodobé efekty.
- **Nevýhody**: Možnost zkreslení pamětí účastníků, náročnost na sběr dat.
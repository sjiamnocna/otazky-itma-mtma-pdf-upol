# 11. Zvláštnosti dětské psychiky – sugestibilita, labilita, egocentrismus, expresivní a adaptivní chování, negativismus, eidetismus, konkretismus, personifikace a jejich projevy.

> Dětská psychika se vyznačuje několika specifickými rysy, které se mění s věkem a vývojem. Tyto rysy ovlivňují, jak děti vnímají svět, reagují na podněty a interagují s ostatními.

## Sugestibilita
- **Definice**: Zvýšená vnímavost a ovlivnitelnost vůči podnětům a názorům ostatních.
- **Projevy**: Děti jsou náchylnější věřit a přijímat informace bez kritického posouzení. To může vést k snadnému přejímání chování a postojů od autorit nebo vrstevníků.

## Labilita
- **Definice**: Nestálost a proměnlivost emocí a nálad.
- **Projevy**: Děti mohou rychle měnit své nálady, reagovat impulzivně a nevyzpytatelně na situace. Tato emocionální nestabilita je běžná a s věkem se stabilizuje.

## Egocentrismus
- **Definice**: Skoncentrování na vlastní perspektivu, neschopnost vidět svět očima druhých.
- **Projevy**: Děti často předpokládají, že ostatní vidí, cítí a myslí stejně jako ony. Příkladem je "teorie mysli" u malých dětí, které nerozumí, že ostatní lidé mohou mít odlišné myšlenky a pocity.

## Expresivní a adaptivní chování
- **Expresivní chování**: 
  - Projevy emocí a nálad prostřednictvím tělesných a verbálních výrazů (např. smích, pláč, gestikulace).
- **Adaptivní chování**:
  - Schopnost přizpůsobit se změnám v prostředí a situacím, často prostřednictvím učení a zkoušení nových strategií.
- **Projevy**: Děti ukazují své emoce bez zábran a rychle se učí adaptivnímu chování prostřednictvím zkušeností a pozorování.

## Negativismus
- **Definice**: Skon k odmítání a odporu vůči návrhům a pokynům od ostatních, často jako projev autonomního vývoje.
- **Projevy**: Děti mohou být odmítavé vůči požadavkům dospělých, i když s nimi souhlasí. Tento jev je často spojen s obdobím vzdoru, kdy se dítě učí prosazovat svou nezávislost.

## Eidetismus
- **Definice**: Schopnost živě si představovat a pamatovat si vizuální obrazy s vysokou přesností.
- **Projevy**: Děti s eidetismem mohou detailně popisovat obrazy, které viděly, nebo si je znovu představit jako by byly stále před nimi. Tento fenomén je výraznější u dětí než u dospělých.

## Konkretismus
- **Definice**: Tendence chápat věci doslovně a konkrétně, bez schopnosti abstrakce.
- **Projevy**: Děti mají sklon brát vše doslovně a mohou mít potíže s pochopením metafor, sarkasmu nebo abstraktních pojmů. Například mohou věřit, že "mít hlavu v oblacích" znamená doslova.

## Personifikace
- **Definice**: Přisuzování lidských vlastností a emocí neživým objektům nebo zvířatům.
- **Projevy**: Děti často mluví s hračkami, připisují jim pocity a motivy, nebo věří, že objekty mají vědomí. Příkladem je představa, že plyšový medvídek je smutný, když je sám.

Tyto zvláštnosti dětské psychiky jsou přirozenou součástí vývoje a většina z nich se s věkem a kognitivním zráním zmírňuje. Děti postupně rozvíjejí schopnost abstraktního myšlení, empatie a lepší emoční regulace.
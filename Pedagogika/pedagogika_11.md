# 11. Charakterizujte základní metody sběru empirických dat a na konkrétních příkladech bjasněte jejich použití.

## Charakterizujte základní metody sběru empirických dat a na konkrétních příkladech objasněte jejich použití.

### Základní metody sběru empirických dat
1. **Pozorování**
   - **Definice**: Smyslové vnímání okolního světa, záměrné a soustavné vnímání a zaznamenávání jevů a procesů.
   - **Typy**:
     - **Strukturované**: Přesně definované, co bude pozorováno.
     - **Nestrukturované**: Flexibilní, bez předem daných struktur.
   - **Použití**: 
     - Sledování nákupního chování zákazníků v obchodě.
     - Pozorování interakce učitelů a žáků ve třídě.

2. **Dotazování (dotazník)**
   - **Definice**: Sběr dat prostřednictvím otázek položených respondentům.
   - **Typy**:
     - **Osobní dotazování (rozhovor)**: Face-to-face interakce.
     - **Písemné dotazování (anketa)**: Respondenti odpovídají písemně.
     - **Internetové dotazování**: Online dotazníky.
     - **Telefonické dotazování**: Otázky pokládané po telefonu.
   - **Použití**:
     - Dotazník sebehodnocení žáka.
     - Dotazník pro uchazeče o práci.

3. **Experiment**
   - **Definice**: Metoda, při které se manipuluje s jednou nebo více proměnnými a pozorují se změny v jiných proměnných.
   - **Typy**:
     - **Laboratorní experiment**: Probíhá v kontrolovaném prostředí.
     - **Terénní experiment**: Probíhá v přirozeném prostředí.
   - **Použití**:
     - Milgramův experiment: Ověření, jak daleko jsou lidé schopni zajít ve své poslušnosti k autoritě.

4. **Studium písemných pramenů (analýza dokumentů)**
   - **Definice**: Analyzování textů a dokumentů, které obsahují relevantní informace pro výzkum.
   - **Použití**:
     - Analýza školních záznamů a vysvědčení.
     - Studium odborných článků a knih.

### Příklady použití metod sběru dat
1. **Pozorování**
   - **Příklad**: Pozorování učebních metod ve třídě, kde se sleduje interakce mezi učiteli a žáky, aby se zjistilo, jak různé vyučovací metody ovlivňují zapojení žáků.
   - **Výhody**: Umožňuje sběr dat v přirozeném prostředí, kde je chování spontánní.
   - **Nevýhody**: Může být časově náročné a subjektivní vliv pozorovatele může zkreslit výsledky.

2. **Dotazník**
   - **Příklad**: Dotazník pro rodiče žáků, kde se zjišťují jejich názory na kvalitu vzdělávání a školní prostředí.
   - **Výhody**: Může být distribuován velkému počtu respondentů, relativně snadná analýza kvantitativních dat.
   - **Nevýhody**: Riziko nízké návratnosti a neúplných odpovědí, možnost neupřímných odpovědí.

3. **Experiment**
   - **Příklad**: Výzkum vlivu různých metod výuky na výsledky žáků, kde jedna skupina žáků je vyučována tradiční metodou a druhá skupina novou interaktivní metodou.
   - **Výhody**: Umožňuje kontrolovat proměnné a zjistit příčinné vztahy.
   - **Nevýhody**: Může být obtížné realizovat v přirozeném prostředí, etické otázky při manipulaci s proměnnými.

4. **Analýza dokumentů**
   - **Příklad**: Studium školních politik a jejich dopad na inkluzi žáků se speciálními vzdělávacími potřebami.
   - **Výhody**: Přístup k rozsáhlým a detailním informacím, možnost historické analýzy.
   - **Nevýhody**: Může být obtížné ověřit přesnost a aktuálnost informací v dokumentech
# 10. Vývojová periodizace, příklady vývojových periodizací (Příhodova biopsychologická, Eriksonova psychosociální, Piagetova kognitivní, Freudova psychoanalytická).

> Vývojová periodizace je rozdělení lidského života na různé etapy nebo období, během nichž dochází k charakteristickým změnám ve fyzickém, psychickém a sociálním vývoji. Každé období má své specifické vývojové úkoly a výzvy.

## Příklady vývojových periodizací

### 1. Příhodova biopsychologická periodizace
- Tato periodizace se zaměřuje na propojení biologického a psychologického vývoje. 
- **Fáze:**
  1. **Prenatální období**: Od početí do narození.
  2. **Novorozenecké období**: Prvních několik týdnů po narození.
  3. **Kojenecké období**: Do 1 roku věku, rychlý fyzický růst a základní motorický vývoj.
  4. **Batolecí období**: 1–3 roky, rozvoj chůze, řeči a základních sociálních dovedností.
  5. **Předškolní období**: 3–6 let, rozvoj hrubé a jemné motoriky, rozšíření sociálních interakcí.
  6. **Mladší školní věk**: 6–12 let, rozvoj kognitivních schopností a školní zdatnosti.
  7. **Puberta a adolescence**: 12–18 let, rychlý fyzický a psychický vývoj, formování identity.
  8. **Dospělost**: Od 18 let výše, stabilizace osobnosti, profesní a rodinný život.
  9. **Stáří**: Od 60 let výše, úpadek fyzických a kognitivních schopností, adaptace na změny.

### 2. Eriksonova psychosociální periodizace
- Erik Erikson navrhl teorii osmi vývojových stádií, které pokrývají celý lidský život a každé z nich zahrnuje psychosociální konflikt, který je třeba vyřešit.
- **Fáze:**
  1. **Důvěra vs. nedůvěra (0–1 rok)**: Základní smysl důvěry v svět.
  2. **Autonomie vs. stud a pochybnosti (1–3 roky)**: Rozvoj autonomie a sebeúcty.
  3. **Iniciativa vs. vina (3–6 let)**: Rozvoj iniciativy a pocitu cílevědomosti.
  4. **Snaživost vs. méněcennost (6–12 let)**: Vývoj pracovitosti a kompetence.
  5. **Identita vs. zmatení rolí (12–18 let)**: Formování osobní identity.
  6. **Intimita vs. izolace (mladá dospělost)**: Vytváření blízkých vztahů.
  7. **Generativita vs. stagnace (střední dospělost)**: Péče o další generace.
  8. **Integrita vs. zoufalství (pozdní dospělost)**: Reflexe života a smíření s ním.

### 3. Piagetova kognitivní periodizace
- Jean Piaget rozlišil čtyři základní fáze kognitivního vývoje, které se týkají způsobu, jakým děti chápou a interpretují svět kolem sebe.
- **Fáze:**
  1. **Senzomotorické stádium (0–2 roky)**: Vnímání světa prostřednictvím smyslů a pohybů.
  2. **Předoperační stádium (2–7 let)**: Vývoj symbolického myšlení, egocentrismus.
  3. **Stádium konkrétních operací (7–11 let)**: Logické myšlení o konkrétních objektech.
  4. **Stádium formálních operací (od 11 let)**: Abstraktní a hypotetické myšlení.

### 4. Freudova psychoanalytická periodizace
- Sigmund Freud vymezil pět stadií psychosexuálního vývoje, která jsou určena zaměřením libidózní energie na různé erogenní zóny.
- **Fáze:**
  1. **Orální stádium (0–1,5 roku)**: Příjemné zkušenosti spojené s ústy (kojení).
  2. **Anální stádium (1,5–3 roky)**: Kontrola vyměšování, potěšení z ovládání tělesných funkcí.
  3. **Falické stádium (3–6 let)**: Zaměření na genitální oblast, vznik Oidipovského komplexu.
  4. **Latentní stádium (6–puberta)**: Utlumování sexuálních pudů, rozvoj sociálních dovedností.
  5. **Genitální stádium (puberta a dále)**: Znovuobjevení sexuality, rozvoj zralé sexuální identity.

Každá z těchto teorií nabízí odlišný pohled na vývoj člověka a zaměřuje se na různé aspekty vývoje, jako jsou biologické, psychologické a sociální faktory. Tyto periodizace slouží jako rámce pro pochopení komplexního procesu vývoje člověka.
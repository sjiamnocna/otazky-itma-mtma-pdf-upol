# 1. Předmět zkoumání v psychologii, základní pojmy (prožívání, chování, psychické procesy a stavy, introspekce, osobní konstrukty a mentální reprezentace). Systém psychologických věd.

## Předmět zkoumání v psychologii

- Psychologie je věda, která se zabývá studiem duševních procesů, chování a prožívání člověka.
- Doslova věda o duši (řeč. psyché = duše, logos = věda)
- Zkoumá, jak lidé vnímají svět kolem sebe, jak myslí, cítí a chovají se v různých situacích.

## Základní pojmy

1. **Psychika**:
   - Souhrn duševních dějů během celého lidského života.
   - Mysl – synonymum pro psychiku, propojení nervových a mentálních dějů.

2. **Introspekce**:
   - Schopnost pozorovat vlastní psychické prožitky probíhající ve vědomí.

3. **Reflexe**:
   - Schopnost lidského vědomí obrátit se k sobě a pozorovat svou činnost.

4. **Vědomí**:
   - Naše psychická zkušenost, která je evidentní.
   - Vlastnosti: intencionalita (plánování a záměrné jednání), subjektivita mentálních stavů, mentální kauzace (vliv subjektivních myšlenek na okolí).

5. **Nevědomí**:
   - Soubor psychických obsahů a procesů probíhajících mimo vědomí, které ovlivňují naše chování a prožívání.

6. **Osobní konstrukty**:
   - Vytváření vlastních teorií o tom, jací jsou ostatní.
   - Bipolární podoba: hodný-zlý, chytrý-hloupý.
   - Každý vidí věci jinak na základě vlastních zkušeností.

7. **Mentální reprezentace**:
   - Psychický obsah - představy a myšlenky na základě zkušeností.
   - Reprezentace reality pomocí vlastních poznávacích prostředků.
   - Výsledek zpracovávání informací (vnější svět, sebereflexe).

## Projevy

1. **Prožívání**:
   - Vnitřní svět člověka, uskutečňuje se prostřednictvím duševních jevů.
   - Nepřetržitý tok psychických zážitků.
   - Dělíme na psychické procesy (kognitivní, emotivní, motivační) a psychické stavy (nálada, aktuální psychické rozpoložení).
   - Základní složky: rozum, city, vůle.
   - Předmět prožívání: okolí, nitro, tělo.

2. **Chování**:
   - Vnější projevy prožívání.
   - Tělesná aktivita, kterou lze pozorovat, zaznamenávat a měřit.
   - Dělení: volní (úmyslné) a mimovolní (neúmyslné).
   - Základní vztahové dimenze: intimita (důvěrnost) a moc (vliv).

3. **Psychické procesy a stavy**:
   - **Procesy**: kognitivní (poznávací), emotivní (citové), motivační (volní).
   - **Stavy**: nálady a aktuální psychická rozpoložení.

4. **Introspekce**:
   - Schopnost pozorovat vlastní psychické prožitky probíhající ve vědomí.

5. **Osobní konstrukty**:
   - Vytváření vlastních teorií o tom, jací jsou ostatní.
   - Bipolární podoba: hodný-zlý, chytrý-hloupý.
   - Každý vidí věci jinak na základě vlastních zkušeností.

6. **Mentální reprezentace**:
   - Psychický obsah - představy a myšlenky na základě zkušeností.
   - Reprezentace reality pomocí vlastních poznávacích prostředků.
   - Výsledek zpracovávání informací (vnější svět, sebereflexe).

## Systém psychologických věd

1. **Základní psychologie**:
   - **Biologická psychologie**: vztahy mezi tělesnými procesy a psychickým děním.
   - **Obecná psychologie**: základní psychické procesy, lidské poznávání, motivaci a emoce.
   - **Vývojová psychologie**: vývoj jedince v průběhu života, tělesné, emocionální, kognitivní a sociální změny.
   - **Psychologie osobnosti**: jedinečné psychické vlastnosti ovlivňující myšlení, cítění a chování.
   - **Sociální psychologie**: vliv společenských faktorů na lidskou psychiku, interakce mezi lidmi.

2. **Aplikovaná psychologie**:
   - **Klinická psychologie**: diagnostika, léčení a přizpůsobení osob s mentálními poruchami.
   - **Poradenská psychologie**: pomoc jednotlivcům při překonávání osobních a mezilidských problémů.
   - **Pedagogická psychologie**: zkoumá procesy učení a vyučování, vytváření studijních osnov a metod.
   - **Psychologie práce**: podmínky práce a jejich vliv na pracovní výkon.
   - **Forenzní psychologie**: aplikace psychologie v soudnictví a kriminalistice.
   - **Psychologie sportu**: zlepšení výkonů sportovců a zvládání trémy.

3. **Speciální psychologie**:
   - **Psycholingvistika**: způsoby, jakými si lidé osvojují a užívají jazyk.
   - **Psychometrie**: měření duševních jevů a tvorba psychologických testů.
   - **Metodologie**: principy vědeckého bádání a experimentů.
   - **Psychodiagnostika**: rozpoznávání a posuzování psychických vlastností a stavů.


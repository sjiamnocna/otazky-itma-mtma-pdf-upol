# 24. Charakterizujte materiální didaktické prostředky a proveďte jejich možné klasifikace. Učební pomůcky, didaktickou techniku. Informatické myšlení, digitální kompetence pedagogů a žáků. Interaktivní a programovatelná technika, virtuální a rozšířená realita, převrácená třída, otevřené vzdělávání, digitální gramotnost, informatické myšlení. Evropský rámec digitálních kompetencí pedagogů.

## Materiální didaktické prostředky
Materiální didaktické prostředky jsou prostředky materiální povahy, které spolu s prostředky nemateriální povahy (např. organizační formy a metody práce, didaktické zásady) slouží k dosažení výukových cílů. Zprostředkovávají obsah probírané látky, usnadňují práci učiteli a nepřímo působí i na učební činnost žáka.

## Klasifikace materiálních didaktických prostředků
**Dle Otto Obst:**
- Učební pomůcky a školní potřeby
- Přístroje didaktické a diagnostické techniky
- Doplňující technické zařízení
- Televizní technika
- Fotografická technika
- Reprografická technika
- Vyučovací automaty a zpětnovazební zařízení (výpočetní technika, databázové systémy)

**Dle typu učebních pomůcek:**
- Žákovské – pro pozorování a vyhledávání učebních dat, pro materiální operace
- Demonstrační

**Další dělení učebních pomůcek:**
- Vizuální, auditivní, taktilní, olfaktorické

## Učební pomůcky a didaktická technika
- **Skutečné předměty** (přírodniny, výtvory, výrobky, preparáty)
- **Modely** – dynamické, statické
- **Přístroje, technická zařízení, stroje**
- **Zobrazení**:
  - Přímá zobrazení (obrazy, mapy, fotografie, poster, nástěnka, náčrty, kresby)
  - Statická projekce (diaprojekce, zpětná projekce) a dynamická projekce (TV, video, film, PC)
- **Zvukové záznamy** (hudební nástroje, gramofonové desky, magnetofonové pásky, CD-ROM)
- **Dotykové obrazové pomůcky** (reliéfové obrazy, Braillovo písmo)
- **Textové pomůcky**:
  - Učebnice
  - Pracovní materiál (pracovní sešity, studijní návody, tabulky, atlasy, sbírky úloh)
  - Doplňková a pomocná literatura (časopisy, encyklopedie)
  - Nosiče informací pro PC
- **Speciální pomůcky** (experimentální soupravy, pomůcky pro tělesnou výchovu)

## Digitální kompetence pedagogů a žáků
**Na úrovni žáka:**
- Ovládání běžně používaných digitálních zařízení, aplikací a služeb
- Vyhledávání, kritické posuzování, spravování a sdílení dat
- Vytváření a upravování digitálního obsahu
- Automatizace rutinních činností
- Kritické hodnocení přínosů a rizik digitálních technologií
- Etické jednání při spolupráci, komunikaci a sdílení informací v digitálním prostředí

**Na úrovni pedagoga:**
- Používání různých informačních technologií
- Vedení studentů ke správnému využívání technologií pro jejich osobní rozvoj
- Reflektivní praxe a soustavný profesní rozvoj

## Interaktivní a programovatelná technika
**Příklady:**
- Interaktivní tabule, dataprojektor, notebook, televize
- Nové trendy: Interaktivní dotykový panel, Interaktivní dotyková televize, Vizualizér, Digitální mikroskop, Tablet, Robotické technologie, Virtuální a rozšířená realita, 3D tisk, Laserové gravírování, Internet věcí, Herní konzole

**Virtuální realita (VR):**
- Vstup do interaktivního třírozměrného světa prostřednictvím speciálních brýlí
- Použití ve zdravotnictví, architektuře, armádě, vzdělávání

**Rozšířená realita (AR):**
- Integrace virtuálních prvků do obrazu reálného světa
- Využití v propagaci výrobků, efektivní výuka a domácí příprava

## Převrácená třída
- Žáci se seznámí s látkou doma online a ve škole pracují individuálně podle potřeb
- Efektivnější využití času ve třídě
- Příkladem jsou videa od Khan Academy

## Otevřené vzdělávání
- Odstraňování překážek ve vzdělávání
- Volný přístup k obsahu a datům
- Posun k novým výukovým metodám a postupům
- Otevřenost zahrnuje rovnost a spolupráci

## Evropský rámec digitálních kompetencí pedagogů (DigCompEdu)
- **Profesní zapojení učitele**: pracovní komunikace, odborná spolupráce, reflektivní praxe, soustavný profesní rozvoj
- **Digitální zdroje**: výběr, tvorba a úprava digitálních zdrojů, organizace, ochrana, sdílení
- **Výuka**: vyučování, vedení žáka, spolupráce žáků, samostatné učení
- **Digitální hodnocení**: strategie hodnocení, analýza výukových výsledků, zpětná vazba a plánování
- **Podpora žáků**: přístupnost a inkluze, diferenciace a individualizace, aktivizace žáků
- **Podpora digitálních kompetencí žáků**: informační a mediální gramotnost, digitální komunikace a spolupráce, tvorba digitálního obsahu, odpovědné používání digitálních technologií, řešení problémů prostřednictvím digitálních technologií
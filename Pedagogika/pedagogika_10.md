# 10. Charakterizujte základní typy měření v pedagogickém výzkumu a popište typy roměnných. Vysvětlete vlastnosti dobrého měření, výzkumné problémy (výzkumné tázky), věcné a statistické hypotézy ve výzkumu.

## Základní typy měření v pedagogickém výzkumu
1. **Nominální měření**
   - **Definice**: Přiřazování čísel kategoriím bez jakékoliv hierarchické struktury.
   - **Příklad**: Pohlaví (muž = 1, žena = 2).

2. **Ordinální měření**
   - **Definice**: Přiřazování čísel s ohledem na pořadí, ale bez stanovení přesných rozdílů mezi kategoriemi.
   - **Příklad**: Stupnice hodnocení (např. 1 = výborný, 2 = dobrý, 3 = dostatečný).

3. **Intervalové měření**
   - **Definice**: Měření s rovnoměrnými intervaly mezi hodnotami, ale bez absolutní nuly.
   - **Příklad**: Teplota v Celsius.

4. **Poměrové měření**
   - **Definice**: Měření s rovnoměrnými intervaly a absolutní nulou.
   - **Příklad**: Hmotnost (kg), výška (cm).

## Typy proměnných
1. **Nezávislá proměnná**
   - **Definice**: Proměnná, kterou výzkumník manipuluje nebo ji považuje za příčinu změn.
   - **Příklad**: Metoda výuky.

2. **Závislá proměnná**
   - **Definice**: Proměnná, která je ovlivněna nezávislou proměnnou.
   - **Příklad**: Výsledky testů žáků.

3. **Kovariátní proměnná**
   - **Definice**: Proměnná, která může ovlivnit vztah mezi nezávislou a závislou proměnnou.
   - **Příklad**: Socioekonomický status žáků.

4. **Kontrolní proměnná**
   - **Definice**: Proměnná, kterou výzkumník udržuje konstantní, aby zabránil jejímu vlivu na výsledek.
   - **Příklad**: Věk žáků.

## Vlastnosti dobrého měření
1. **Validita**
   - **Definice**: Míra, do jaké měření skutečně měří to, co má měřit.
   - **Typy**:
     - **Obsahová validita**: Posuzuje, do jaké míry měření zahrnuje všechny aspekty daného jevu.
     - **Souběžná validita**: Posuzuje, do jaké míry měření souhlasí s jinými měřeními téhož jevu.
     - **Predikční validita**: Posuzuje, do jaké míry měření předpovídá budoucí vývoj objektu.
     - **Konstruktová validita**: Posuzuje, do jaké míry měření odpovídá teoretickým očekáváním.

2. **Reliabilita**
   - **Definice**: Míra, do jaké měření poskytuje konzistentní výsledky při opakovaném použití za stejných podmínek.
   - **Metody hodnocení**:
     - **Test-retest**: Opakované měření stejným nástrojem.
     - **Metoda půlení**: Rozdělení výsledků na dvě poloviny a korelace mezi nimi.
     - **Cronbachův koeficient alfa**: Analýza vnitřní konzistence testu.

3. **Praktičnost**
   - **Definice**: Míra, do jaké je měření snadno proveditelné, ekonomicky a časově nenáročné.
   - **Charakteristiky**: Jednoduchost, hospodárnost, časová nenáročnost, snadná proveditelnost.

## Výzkumné problémy a otázky
1. **Deskriptivní problémy**
   - **Definice**: Popisují aktuální stav nebo situaci.
   - **Příklad**: Jaké druhy pochval používají učitelé?

2. **Relační problémy**
   - **Definice**: Zkoumají vztahy mezi dvěma nebo více proměnnými.
   - **Příklad**: Jaký je vztah mezi druhem pochval a výkony žáků?

3. **Kauzální problémy**
   - **Definice**: Zkoumají příčinné vztahy mezi proměnnými.
   - **Příklad**: Jaká je účinnost oddálené pochvaly na výkon žáků ve srovnání s bezprostřední pochvalou?.

## Věcné a statistické hypotézy ve výzkumu
1. **Věcné hypotézy**
   - **Definice**: Tvrdí vztah mezi proměnnými, který vychází z výzkumného problému.
   - **Příklad**: Učitelé, kteří používají humor ve třídě, dosahují lepších učebních výsledků než učitelé, kteří humor nepoužívají.

2. **Statistické hypotézy**
   - **Definice**: Testovatelné výroky o vztazích mezi proměnnými, často ve formě nulové a alternativní hypotézy.
   - **Příklad**:
     - **Nulová hypotéza (H0)**: Mezi proměnnými není vztah (např. Frekvence kouření je u mužů i žen stejně velká).
     - **Alternativní hypotéza (HA)**: Mezi proměnnými je vztah (např. Frekvence kouření je u mužů a žen rozdílná).


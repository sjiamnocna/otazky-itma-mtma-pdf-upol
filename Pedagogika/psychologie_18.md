# 18. Vývojové období dospělosti (periodizace, tělesné a psychické změny, krize životního středu).

> Dospělost je rozsáhlé období lidského života, které zahrnuje mnoho změn a výzev. Obvykle se dělí na tři hlavní fáze: **raná dospělost, střední dospělost a pozdní dospělost**. Každá z těchto fází je charakterizována specifickými **tělesnými, psychickými a sociálními změnami**.

## Periodizace dospělosti

### 1. Raná dospělost (přibližně 20–40 let)
- Období zaměřené na budování kariéry, zakládání rodiny a rozvoj osobních vztahů.
- Důraz na hledání a nalezení identity a vlastní životní cesty.

### 2. Střední dospělost (přibližně 40–65 let)
- Období, kdy lidé často reflektují na své životní úspěchy a neúspěchy.
- Zaměření na stabilizaci kariéry a vztahů, péči o děti a případně o stárnoucí rodiče.

### 3. Pozdní dospělost (nad 65 let)
- Období zaměřené na odchod do důchodu, adaptaci na stárnutí a bilancování života.
- Příprava na závěr života a zvládání změn v oblasti zdraví a sociálních vztahů.

## Tělesné a psychické změny

### Tělesné změny
- **Raná dospělost**:
  - Vrchol fyzické kondice, maximální síla a vytrvalost.
  - Postupný pokles metabolického tempa, který může vést k nárůstu hmotnosti.
  
- **Střední dospělost**:
  - Zpomalení metabolismu a ztráta svalové hmoty.
  - Změny v hormonální rovnováze, u žen menopauza, u mužů snížení hladiny testosteronu.
  - Zhoršení zraku a sluchu, zvýšené riziko chronických onemocnění (např. kardiovaskulární problémy, diabetes).

- **Pozdní dospělost**:
  - Pokračující fyzický úpadek, ztráta kostní hmoty, snížení svalové síly.
  - Zvýšená zranitelnost vůči nemocem a úrazům, snížená imunita.
  - Zhoršení smyslových funkcí, jako je zrak a sluch.

### Psychické změny
- **Raná dospělost**:
  - Vývoj identity, hledání sebe sama a místa ve světě.
  - Rozvoj kognitivních schopností, schopnost řešit komplexní problémy.
  - Posílení emoční stability a schopnosti vytvářet trvalé vztahy.

- **Střední dospělost**:
  - Reflexe životních úspěchů a neúspěchů, možnost vzniku krize středního věku.
  - Zvýšený zájem o osobní růst a rozvoj, hledání nových cílů a smyslu života.
  - Změny v kognitivních schopnostech, jako je snížení rychlosti zpracování informací, ale zlepšení zkušenostní moudrosti.

- **Pozdní dospělost**:
  - Reflexe a bilance života, adaptace na změny v životním stylu a zdraví.
  - Možný výskyt kognitivních poruch, jako je demence nebo Alzheimerova choroba.
  - Zvýšení důležitosti vztahů s rodinou a přáteli, hledání klidu a smíření.

## Krize životního středu
- **Definice**: Krize středního věku je často popisována jako období emočního nebo psychického nepohodlí, které může nastat v polovině života (obvykle kolem 40 až 50 let).
- **Příčiny**:
  - Reflexe na dosavadní životní úspěchy a neúspěchy, zpochybňování dosavadních rozhodnutí.
  - Uvědomění si omezené doby, kterou jedinec má, což může vést k pocitům úzkosti nebo deprese.
  - Fyzické změny spojené se stárnutím, změny ve vztazích, odchod dětí z domova (syndrom prázdného hnízda).
- **Projevy**:
  - Hledání nových výzev nebo cílů, někdy impulzivní změny v kariéře nebo osobním životě.
  - Změny ve vzhledu nebo životním stylu, snaha "zůstat mladý".
  - Zvýšený zájem o introspekci a hledání smyslu života.
- **Pozitivní stránka**:
  - Může vést k pozitivním změnám a růstu, jako je nalezení nových zájmů, zlepšení zdraví nebo prohloubení vztahů.

> Krize životního středu může být považována za přirozenou fázi osobního růstu, která poskytuje příležitost k reflexi a přehodnocení dosavadního života. Je to příležitost k osobnímu rozvoji a nalezení nového směru, který vede ke zralé dospělosti.
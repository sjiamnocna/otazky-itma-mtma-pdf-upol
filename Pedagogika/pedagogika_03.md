# 3. Vysvětlete rozdíly definovaní kategorie cíl výchovy v různých pedagogických koncepcích. Interpretujte znaky „dobrých“ cílů. Uveďte funkce, třídění, klasifikace a strukturu výchovných cílů. Charakterizujte názory na současné cíle výchovy.

## Různé pedagogické koncepce a cíle výchovy
- Pedagogická činnost učitele a žáků musí mít cíl (J. A. Komenský)
- Cíl výchovy můžeme definovat jako **ideální představu předpokládaných výsledků, jichž má být ve výchově dosaženo**
- Cíl výchovy by měl být takový, aby jedinci umožnil orientaci v životních situací, spořádaný život ve společnosti, rozvoj vědomostí a dovedností a vytvořil základy pro další růst
- Cíle výchovy nejsou statické, mění se s vývojem společnosti, aktualizují se
- Výchovného cíle se dosahuje soustavou výchovných prostředků v dlouhodobém procesu, cíl je potřeba stále srovnávat
s realitou

## Vysvětlete rozdíly definice cíl výchovy v různých pedagogických koncepcích
- Dva směry přístupu k problematice cílů:
    - Indeterministický
        - Odmítá společenskou podmíněnost výchovných cílů
        - Původ cílů nachází v nadpřirozených silách, je nezávislý na vývoji společnosti
        - Např. nábožensky založený
    - Deterministický
        - Vychází ze společenské podmíněnosti výchovných cílů
        - Musí být v souladu se dvěma základními stránkami výchovného procesu
            - Subjektivní = psychická úroveň vychovávaných
            - Objektivní = stav společensko-ekonomických vztahů i vývoje společnost

### Tradiční pedagogika
- V tradiční pedagogice jsou cíle výchovy často zaměřeny na předávání znalostí a dovedností, považovaných za nezbytné pro normální fungování ve společnosti.
- Hlavní důraz je kladen na:
    - **Akademické znalosti**: Memorování faktů a osvojení si základních akademických dovedností.
    - **Disciplína a poslušnost**: Rozvoj respektu k autoritě a dodržování pravidel.

### Progresivní pedagogika
- Zaměřuje se na rozvoj individuálních schopností a kreativity. Cíle se zaměřují na:
    - **Kritické myšlení**: Podpora analytických dovedností a nezávislého myšlení.
    - **Sociální dovednosti**: Spolupráce, empatie a komunikace.

### Humanistická pedagogika
- Zaměřuje se na osobní růst a seberealizaci jedince. Cíle zahrnují:
    - **Sebepoznání**: Podpora rozvoje osobní identity a sebepřijetí.
    - **Osobní odpovědnost**: Podpora etického a morálního vývoje.

### Konstruktivistická pedagogika
- Zaměřuje se na aktivní zapojení žáků do procesu učení. Cíle zahrnují:
    - **Aktivní učení**: Zapojení studentů do řešení problémů a projektů.
    - **Reflexivní myšlení**: Podpora sebereflexe a metakognice.

## Dobře formulované cíle výchovy by měly být:
- **Komplexní**
    - Musejí být vyjádřeny všechny tři roviny
        - kognitivní
        - psychomotorická
        - afektivní
- **Konzistentní**
    - Cíle musí být soudržné - provázané
    - Jdeme od nejjednodušších cílů ke složitějším
- **Kontrolovatelné**
    - Cíl má vlastnost umožňující kontrolu jeho splnění
- **Jednoznačné**
    - Cíl musí být stanoven tak, aby nebylo možné jiné interpretace.
    - Má jediný a přesný význam, který neumožňuje jiný výklad nebo řešení
- **Přiměřené**
    - Cíle mají být náročné, ale současně i splnitelné pro většinu žáků
    - Při sestavování cílů nesmí učitel zapomenout na rozdílné mentální, afektivní a psychomotorické úrovně jednotlivých žáků
    
- Cíl by měl **aktivním slovese** vyjádřit po žácích požadovaný výkon
    - Podmínky výkonu - metody dosažení výkonu, podmínky, pomůcky
    - Norma výkonu - počet příkladů, rozsah

## Funkce cílů výchovy
Cíle výchovy slouží několika funkcím:
- **Orientace**: Poskytují směr a zaměření výchovného procesu.
- **Motivace**: Podněcují zájem a úsilí studentů.
- **Kontrola**: Umožňují sledovat a hodnotit pokrok a úspěšnost výchovného procesu.
- **Integrace**: Spojují různé aspekty výchovy do soudržného celku.

## Třídění a klasifikace výchovných cílů

### Podle obsahu
- **Kognitivní cíle**: Zahrnují znalosti a intelektuální dovednosti.
- **Afektivní cíle**: Zaměřené na emocionální a sociální rozvoj.
- **Psychomotorické cíle**: Zahrnují fyzické dovednosti a koordinaci.

### Podle úrovně obecnosti
- **Obecné cíle**: Široce definované cíle, například rozvoj kritického myšlení.
- **Specifické cíle**: Konkrétní cíle, například osvojení si konkrétní dovednosti.

## Struktura výchovných cílů

Výchovné cíle lze strukturovat do několika úrovní:
- **Obecné cíle**: Široké a dlouhodobé cíle.
- **Specifické cíle**: Konkrétní a krátkodobé cíle.
- **Operativní cíle**: Praktické a okamžité cíle.

## Současné cíle výchovy

### Globalizace a multikulturní výchova
- **Interkulturní kompetence**: Schopnost porozumět a respektovat různé kultury.
- **Globální občanství**: Výchova k zodpovědnému a aktivnímu občanství ve světovém měřítku.

### Digitalizace a technologický pokrok
- **Digitální gramotnost**: Schopnost efektivně a bezpečně využívat moderní technologie.
- **Kritická mediální gramotnost**: Schopnost analyzovat a kriticky hodnotit informace z médií.

### Udržitelný rozvoj
- **Environmentální výchova**: Výchova k odpovědnému přístupu k životnímu prostředí.
- **Sociální a ekonomická udržitelnost**: Podpora rozvoje sociálně a ekonomicky udržitelného myšlení.
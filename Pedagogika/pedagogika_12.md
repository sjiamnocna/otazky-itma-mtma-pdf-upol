# 12. Popište nejběžnější postupy zpracování dat v pedagogických výzkumech (četnosti, rafické metody zobrazování dat, charakteristiky polohy, metody průzkumové analýzy at, normální rozdělení).

### Popište nejběžnější postupy zpracování dat v pedagogických výzkumech

#### Četnosti
1. **Definice**
   - Četnost vyjadřuje počet výskytů jednotlivých hodnot v datovém souboru.
2. **Typy četností**
   - **Absolutní četnost (ni)**: Počet výskytů konkrétní hodnoty.
   - **Relativní četnost (fi)**: Podíl absolutní četnosti a celkového počtu hodnot (fi = ni/n).
   - **Kumulativní četnost**: Součet četností hodnot až po danou hodnotu.
3. **Použití**
   - Sestavení tabulek četností pro přehlednost dat, například při měření výsledků testů žáků.

#### Grafické metody zobrazování dat
1. **Histogram**
   - **Popis**: Sloupcový diagram, kde na ose x jsou jednotlivé hodnoty (nebo intervaly) a na ose y četnosti.
   - **Použití**: Zobrazení distribuce výsledků testů žáků.
2. **Polygon četnosti**
   - **Popis**: Spojnicový diagram zobrazující četnosti hodnot podobně jako histogram.
   - **Použití**: Analýza trendů v datech.
3. **Graf kumulativních četností (Galtonova ogiva)**
   - **Popis**: Grafické znázornění závislosti mezi hodnotami a jejich kumulativními četnostmi.
   - **Použití**: Studium akumulace četností.
4. **Výsečový diagram**
   - **Popis**: Graf znázorňující strukturu složení výběrového souboru v procentuálních podílech.
   - **Použití**: Zobrazení rozdělení kategorií v souboru dat.

#### Charakteristiky polohy
1. **Aritmetický průměr**
   - **Definice**: Součet všech hodnot dělený počtem hodnot.
   - **Výhody**: Využitelný pro metrická data, snadno vypočitatelný.
   - **Nevýhody**: Citlivý na extrémní hodnoty.
2. **Medián**
   - **Definice**: Prostřední hodnota v seřazeném souboru dat.
   - **Výhody**: Není ovlivněn extrémními hodnotami, vhodný pro ordinální data.
3. **Modus**
   - **Definice**: Nejčastěji se vyskytující hodnota v souboru dat.
   - **Výhody**: Jednoduché určení, vhodné pro nominální data.
4. **Geometrický průměr**
   - **Definice**: n-tá odmocnina z součinu všech hodnot.
   - **Použití**: Pro výpočty průměrného růstu nebo výnosů.

#### Metody průzkumové analýzy dat
1. **Definice**
   - Průzkumová analýza dat (exploratory data analysis) zahrnuje postupy pro efektivní využívání statistických metod při minimálních nárocích na metodologickou přípravu.
2. **Typy**
   - **S-L grafy (stem-and-leaf display)**: Specifický typ histogramu, kde jsou data rozdělena na stonky (desítky) a listy (jednotky).
   - **Krabicové (kvartilové) grafy**: Znázorňují medián a kvartily souboru dat.
3. **Použití**
   - Analyzování výsledků testů, identifikace rozložení dat a detekce odlehlých hodnot.

#### Normální rozdělení
1. **Definice**
   - Pravděpodobnostní rozdělení, kde většina hodnot je soustředěna kolem průměru a extrémní hodnoty se vyskytují méně často, vytvářející zvonovitou křivku (Gaussova křivka).
2. **Vlastnosti**
   - **Intervaly kolem průměru**:
     - ±1σ: Přibližně 68,27 % všech hodnot.
     - ±2σ: Přibližně 95,4 % všech hodnot.
     - ±3σ: Přibližně 99,73 % všech hodnot.
   - **Distribuční funkce**: Pravděpodobnost, že hodnota je menší nebo rovna určité hodnotě.
3. **Použití**
   - Analýza výsledků testů a jiných měření, kde se předpokládá normální rozdělení hodnot.
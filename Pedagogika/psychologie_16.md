# 16. Psychologická charakteristika období dospívání – prepuberta, puberta (tělesné změny, kognitivní, emocionální a sociální vývoj).

> Období dospívání, rozdělené na prepubertu a pubertu, je fází intenzivních fyzických, kognitivních, emocionálních a sociálních změn, které vedou k přechodu z dětství do dospělosti.

## Prepuberta
Prepuberta je přechodné období mezi dětstvím a pubertou, **obvykle mezi 9 a 12 lety**. Během tohoto období začínají první tělesné změny spojené s dospíváním, ale dospívající ještě nejsou plně ve fázi puberty.

### Tělesné změny
- **Nástup tělesného růstu**: Zrychlení růstu, známé jako růstový spurt, zejména u dívek.
- **Začátek vývoje sekundárních pohlavních znaků**: U dívek začíná růst prsů, u chlapců zvětšování varlat.

### Kognitivní vývoj
- **Rozvoj abstraktního myšlení**: Začínají se objevovat schopnosti pro abstraktní a logické myšlení, i když jsou stále omezené.
- **Zvýšení kapacity paměti**: Zlepšuje se pracovní paměť a schopnost učení se složitějším informacím.

### Emoční vývoj
- **Zvýšená citlivost**: Děti se stávají více citlivými na své emocionální stavy a emocionální podněty z okolí.
- **Začátek hledání identity**: Začíná formování základních otázek ohledně sebe sama a své role ve společnosti.

### Sociální vývoj
- **Změny v sociálních vztazích**: Zvyšuje se význam vztahů s vrstevníky, první pokusy o sociální srovnávání.
- **Vliv médií a vrstevníků**: Rostoucí vliv médií a vrstevníků na utváření názorů a hodnot.

## Puberta
Puberta je období, kdy dospívající procházejí rychlými fyzickými změnami vedoucími k dosažení pohlavní zralosti. Obvykle začíná mezi 11 a 13 lety u dívek a 12 a 14 lety u chlapců.

### Tělesné změny
- **Růstový spurt**: Intenzivní zrychlení růstu, které vede k dosažení téměř konečné výšky.
- **Vývoj sekundárních pohlavních znaků**: U dívek rozvoj prsů, rozšíření boků, začátek menstruace; u chlapců nárůst svalové hmoty, změny hlasu, růst vousů a ochlupení.
- **Změny tělesného složení**: Zvýšené ukládání tuku u dívek a nárůst svalové hmoty u chlapců.

### Kognitivní vývoj
- **Rozvoj formálních operací (Piaget)**: Schopnost abstraktního myšlení, hypotetického uvažování a logické analýzy.
- **Metakognice**: Zlepšení schopnosti reflexe o vlastním myšlení a učení, plánování a strategické myšlení.
- **Etické a morální rozvoj**: Hlubší porozumění morálním a etickým otázkám, rozvoj osobních hodnot a přesvědčení.

### Emoční vývoj
- **Intenzivní emocionální zážitky**: Výkyvy nálad, prohloubení emocionálních prožitků, zvýšená citlivost na kritiku a odmítnutí.
- **Hledání identity**: Důležitá otázka "Kdo jsem?" zahrnuje experimentování s různými rolemi a identitami.
- **Vývoj autonomie**: Zvýšená potřeba nezávislosti od rodičů a hledání vlastních cest a rozhodnutí.

### Sociální vývoj
- **Vrstevnické skupiny**: Stále větší důraz na sociální interakce s vrstevníky, hledání skupiny, do které jedinec patří.
- **Vztahy s rodiči**: Časté konflikty a vyjednávání o autonomii a kontrole.
- **Vztahy s opačným pohlavím**: Začátek romantických vztahů a zkoumání sexuální identity.

> Puberta je klíčové období pro rozvoj jedince, kdy jsou položeny základy pro dospělou identitu, sociální role a osobní hodnoty. Rodiče, učitelé a další dospělí hrají důležitou roli při podpoře a navigaci dospívajících prostřednictvím těchto změn.
# 9. Charakterizujte základy kvantitativně a kvalitativně orientovaného výzkumu  pedagogice (metody lidského poznávání, proveďte jejich vzájemné porovnání, veďte klady, zápory, charakterizujte rozdíly výzkumu ex-post-facto a experimentu).

## Charakterizujte základy kvantitativně a kvalitativně orientovaného výzkumu v pedagogice

### Kvantitativně orientovaný výzkum
1. **Filozofický základ**
   - Pozitivismus: Předpokládá existenci objektivní reality nezávislé na lidských citech, postojích nebo přesvědčeních.
2. **Cíle**
   - Potvrzení nebo vyvrácení hypotéz a teorií.
3. **Charakteristiky**
   - Objektivita
   - Využití deduktivních myšlenkových postupů
   - Výsledky jsou často zobecnitelné na širší populaci.
4. **Metody sběru dat**
   - Dotazníky
   - Testy
   - Standardizované rozhovory
   - Experimenty
5. **Analýza dat**
   - Statistická analýza
   - Kvantitativní metody měření

### Kvalitativně orientovaný výzkum
1. **Filozofický základ**
   - Fenomenologie: Zdůrazňuje subjektivní aspekty lidského jednání a připouští existenci více realit.
2. **Cíle**
   - Hluboké porozumění konkrétnímu kontextu a chování lidí.
3. **Charakteristiky**
   - Subjektivita
   - Využití induktivních myšlenkových postupů
   - Výsledky jsou často podrobné a interpretativní.
4. **Metody sběru dat**
   - Nestrukturované rozhovory
   - Pozorování
   - Analýza dokumentů
   - Případové studie
5. **Analýza dat**
   - Kvalitativní metody analýzy
   - Tematická analýza
   - Interpretativní přístupy

### Porovnání kvantitativního a kvalitativního výzkumu
1. **Filozofický základ**
   - Kvantitativní: Pozitivismus
   - Kvalitativní: Fenomenologie
2. **Charakter**
   - Kvantitativní: Objektivní
   - Kvalitativní: Subjektivní
3. **Vztah k teorii**
   - Kvantitativní: Potvrzujeme/vyvracíme teorie
   - Kvalitativní: Tvorba teorie, tvorba hypotéz
4. **Myšlenkový postup**
   - Kvantitativní: Dedukce
    - Z obecných premis ke konkrétním závěrům
   - Kvalitativní: Indukce
    - Od specifických pozorování k závěrům
5. **Plánování výzkumu**
   - Kvantitativní: Písemný projekt na začátku
   - Kvalitativní: Pružný plán v průběhu práce
6. **Cíl**
   - Kvantitativní: Ověření teorie, hypotéz
   - Kvalitativní: Porozumění chování lidí v přirozeném prostředí

### Výzkum ex-post-facto vs. experiment
1. **Ex-post-facto**
   - **Definice**: Výzkum, u kterého se nemění nezávislá proměnná. Data o závislé proměnné jsou shromážděna a retrospektivně se hledají možné příčiny.
   - **Výhody**: Umožňuje studium příčin a následků, které nelze eticky nebo prakticky manipulovat.
   - **Nevýhody**: Obtížná kontrola nezávislých proměnných, což může snižovat věrohodnost výsledků.
2. **Experiment**
   - **Definice**: Výzkum, kde je alespoň jedna nezávislá proměnná manipulována.
   - **Výhody**: Umožňuje lepší kontrolu a tedy věrohodnější výsledky než ex-post-facto výzkum.
   - **Nevýhody**: Není vždy použitelný ve všech situacích, může způsobit nepřirozené chování zkoumaných osob.
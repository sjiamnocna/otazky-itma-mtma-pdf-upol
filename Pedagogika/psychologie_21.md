# 21. Sociální a interpersonální percepce, interakce a komunikace. Činitelé formování dojmu o jiné osobě. Obecné chyby v interpersonální percepci. Základní principy verbální a neverbální komunikace.

## Sociální a interpersonální percepce
- **Sociální a interpersonální percepce** je **proces**, kterým si jedinci **vytvářejí dojmy a názory o ostatních** lidech. Zahrnuje vnímání vzhledu, chování, řeči a dalších faktorů, které ovlivňují naše posuzování a reakce na druhé.

## Činitelé formování dojmu o jiné osobě
1. **Fyzický vzhled**:
   - První dojem často závisí na fyzickém vzhledu, včetně oblečení, upravenosti a atraktivity. Atraktivní jedinci mohou být často vnímáni pozitivněji (efekt "halo").

2. **Verbální komunikace**:
   - Jazyk, způsob vyjadřování, slovník a tón hlasu mohou ovlivnit, jak je jedinec vnímán. Například jistá a klidná řeč může naznačovat kompetenci a sebevědomí.

3. **Neverbální komunikace**:
   - Výrazy obličeje, gestikulace, držení těla, oční kontakt a proxemika (osobní prostor) hrají klíčovou roli při vytváření dojmu. Neverbální signály mohou často poskytnout více informací než slova.

4. **Sociální kategorie**:
   - Stereotypy spojené s pohlavím, rasou, věkem, profesí a dalšími sociálními kategoriemi mohou ovlivnit naše vnímání druhých.

5. **První dojem**:
   - První dojem je velmi silný a často ovlivňuje následné vnímání a hodnocení osoby. První dojmy jsou často založeny na rychlém zpracování dostupných informací a mohou být obtížně změnitelné.

## Obecné chyby v interpersonální percepci
1. **Efekt halo**:
   - Tendence vnímat osobu celkově pozitivně nebo negativně na základě jedné pozitivní nebo negativní vlastnosti.

2. **Efekt prvního dojmu**:
   - První informace, které o někom získáme, mají tendenci mít neúměrně velký vliv na náš celkový dojem o této osobě.

3. **Stereotypizace**:
   - Přisuzování vlastností nebo chování jedinci na základě jeho příslušnosti k určité skupině, aniž bychom brali v úvahu individuální rozdíly.

4. **Projekce**:
   - Tendence přenášet své vlastní vlastnosti nebo pocity na druhé lidi.

5. **Efekt kontrastu**:
   - Hodnocení osoby může být ovlivněno srovnáváním s předchozí osobou nebo situací.

## Základní principy verbální a neverbální komunikace

### Verbální komunikace
1. **Jasnost a srozumitelnost**:
   - Používání jasného a přesného jazyka, aby bylo zajištěno, že zpráva bude správně pochopena.

2. **Strukturovanost a organizace**:
   - Logická struktura a organizace informací pomáhají příjemci lépe pochopit sdělení.

3. **Aktivní naslouchání**:
   - Zahrnuje pozornost, zpětnou vazbu a potvrzení, že posluchač rozumí tomu, co je řečeno.

4. **Přizpůsobení stylu**:
   - Přizpůsobení stylu komunikace vzhledem k publiku, kontextu a situaci.

### Neverbální komunikace
1. **Výrazy obličeje**:
   - Obličejové výrazy jsou důležitým zdrojem informací o emocích a úmyslech. Úsměvy, zamračení a další výrazy mohou výrazně ovlivnit vnímání sdělení.

2. **Oční kontakt**:
   - Oční kontakt může vyjadřovat zájem, důvěru nebo úzkost. Je důležitý pro vytváření spojení a zajištění, že druhá osoba naslouchá.

3. **Gestikulace**:
   - Gesta mohou pomoci zdůraznit nebo doplnit verbální komunikaci. Mohou také ukazovat na emoce nebo úroveň zapojení.

4. **Držení těla a proxemika**:
   - Držení těla může signalizovat otevřenost, dominanci nebo obranný postoj. Osobní prostor je také klíčovým faktorem; překročení osobního prostoru může vyvolat nepohodlí.

5. **Paralingvistika**:
   - Zahrnuje prvky jako tón hlasu, rychlost řeči, hlasitost a intonaci. Tyto prvky mohou přidávat význam a nuance k verbálnímu sdělení.

> Správné pochopení a efektivní využití jak verbální, tak neverbální komunikace jsou klíčové pro úspěšné sociální interakce a budování pozitivních interpersonálních vztahů.
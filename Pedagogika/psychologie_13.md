# 13. Neurotické obtíže v dětském věku, příčiny vzniku, druhy zátěže typické pro jednotlivá vývojová období, konkrétní typy neurotických obtíží.

> Neurotické obtíže u dětí mohou být způsobeny řadou faktorů, které zahrnují biologické, psychologické a sociální vlivy.

1. **Biologické faktory**:
   - Genetická predispozice k úzkosti a nervozitě.
   - Biochemické nerovnováhy v mozku, které ovlivňují náladu a emoční regulaci.

2. **Psychologické faktory**:
   - Traumata nebo stresující události (např. ztráta rodiče, rozvod, nemoc).
   - Rodinné dynamiky, včetně nadměrného tlaku, nedostatečné podpory nebo konfliktů.
   - Negativní výchovné přístupy, jako je přehnaná kritika nebo nedostatek pozornosti.

3. **Sociální faktory**:
   - Sociální izolace nebo šikana.
   - Změny v sociálním prostředí (např. přestěhování, změna školy).
   - Vliv vrstevníků a společenských norem.

## Druhy zátěže typické pro jednotlivá vývojová období

1. **Prenatální období**:
   - Stres matky může ovlivnit vývoj plodu, což může mít dlouhodobé důsledky.

2. **Novorozenecké a kojenecké období**:
   - Separace od matky, problémy s kojením, nedostatek bezpečného prostředí.

3. **Batolecí období (1–3 roky)**:
   - Frustrace z nedostatku schopností a dovedností, konflikty s rodiči při stanovení hranic, separační úzkost.

4. **Předškolní období (3–6 let)**:
   - První sociální interakce mimo rodinu, přechod do školky nebo předškolní vzdělávací zařízení, strach z neznámého.

5. **Mladší školní věk (6–12 let)**:
   - Akademický tlak, sociální srovnávání, konflikty s vrstevníky, šikana.

6. **Puberta a adolescence (12–18 let)**:
   - Identitní krize, tlak na výkon a úspěch, vztahy s vrstevníky, otázky sexuality.

## Konkrétní typy neurotických obtíží

1. **Úzkostné poruchy**:
   - **Generalizovaná úzkostná porucha (GAD)**: Nadměrné a nekontrolovatelné obavy z různých věcí.
   - **Sociální úzkostná porucha**: Strach z veřejného vystupování a interakcí s ostatními.
   - **Separační úzkostná porucha**: Intenzivní strach z odloučení od rodičů nebo pečovatelů.

2. **Fobické poruchy**:
   - Specifické fobie, jako je strach z tmy, bouřky nebo zvířat.

3. **Obsedantně-kompulzivní porucha (OCD)**:
   - Opakující se, nevítané myšlenky (obsese) a rituály (kompulze), které dítě provádí, aby zmírnilo úzkost.

4. **Somatoformní poruchy**:
   - Tělesné symptomy, jako jsou bolesti břicha, bolesti hlavy, které nejsou způsobeny fyzickým onemocněním, ale mají psychický původ.

5. **Poruchy spánku**:
   - Noční můry, noční děsy, nespavost spojená s úzkostí nebo stresem.

6. **Enuréza a enkopréza**:
   - Neúmyslné močení nebo vyprazdňování ve spánku či během dne, často spojené s psychickým stresem.

> Neurotické obtíže u dětí mohou výrazně ovlivnit jejich každodenní život, vývoj a celkovou pohodu. Je důležité, aby rodiče, učitelé a pečovatelé byli citliví na tyto problémy a hledali vhodnou odbornou pomoc, pokud je to nutné.
# 26. Charakterizujte specifické poruchy učení a chování – vymezte základní pojmy, tiologii, jejich diagnostika a následnou pedagogikou intervenci.

#### Specifické poruchy učení (SPU)
- **Definice**: Skupina poruch a dysfunkcí, které jsou příčinou výukových obtíží dítěte ve škole. Patří sem dyslexie, dysortografie, dysgrafie a dyskalkulie.
- **Prevalence**: V populaci trpí některou z těchto poruch cca 4-5 % jedinců, vyšší procento výskytu je u chlapců. Projevy SPU se nejčastěji objeví po nástupu do 1. ročníku ZŠ.

##### Příčiny vzniku SPU
- **Dědičnost**
- **Prenatální faktory**: Nepříznivý nitroděložní vývoj, negativní vliv nevyvážené hormonální hladiny matky či dítěte, závažné onemocnění matky, vliv rizikových léků, intoxikace škodlivými látkami, dlouhodobý psychický stres matky.
- **Perinatální faktory**: Komplikace při porodu.
- **Postnatální faktory**: Nepříznivý vývoj dítěte do jednoho roku věku, např. těžká infekční onemocnění, úrazy hlavy, nedostatek podnětů v rodinném prostředí.

##### Diagnostika SPU
- **Pedagogicko-psychologické poradny (PPP)**
- **Speciální pedagogové**
- **Psychologové**: Uskutečňují testování a hodnocení, aby identifikovali specifické poruchy učení a navrhli vhodné intervence.

##### Pedagogická intervence
- **Individuální přístup**: Využívání speciálních učebnic a pomůcek, individuální vzdělávací plány.
- **Speciální pedagogická péče**: Zařazení speciálních předmětů do výuky, úpravy v metodách práce, organizaci a průběhu vzdělávání.
- **Podpora**: Přizpůsobení obsahu vzdělávání, využití podpůrných opatření.

#### Specifické poruchy chování (SPCH)
- **Definice**: Projevy chování, kterými se žák odlišuje od ostatních, porušuje morální nebo právní normy, vybočuje z normy přiměřeného chování dané věkové a sociokulturní skupiny.
- **Typy**: 
  - **ADHD (Attention Deficit Hyperactivity Disorder)**: Projevy zahrnují hyperaktivitu, impulzivitu, nepozornost, nedodržování pravidel, snížené sebeovládání.
  - **ADD (Attention Deficit Disorder)**: Podobné ADHD, ale bez hyperaktivity.
  - **ODD (Oppositional Defiant Disorder)**: Projevy zahrnují nesnášenlivost, hádavost, snížené sebeovládání, vidí chyby jen v druhých.

##### Etiologie SPCH
- **Sociální faktory**: Kriminální čin v rodině, užívání návykových látek rodiči, konflikty, nedostatečná péče, domácí násilí.
- **Psychologické faktory**: Hyperaktivita, kognitivní deficity, jazyková bariéra, posttraumatická stresová porucha.
- **Biologické faktory**: Deficity v autonomním nervovém systému, snížená hladina kortizolu, záchvatovitá onemocnění.
- **Genetické faktory**.

##### Diagnostika SPCH
- **Školské poradenské zařízení**: Diagnostika a identifikace poruch chování.
- **Spolupráce s rodiči**: Důležité pro získání úplného obrazu o dítěti a jeho chování v různých prostředích.

##### Pedagogická intervence
- **Individuální přístup**: Využití speciálních pedagogických metod a forem práce, změna uspořádání vybavení třídy.
- **Podpora a vedení**: Vytvoření pozitivního a strukturovaného prostředí ve třídě.
- **Komunikace s rodiči**: Pravidelná komunikace a spolupráce s rodiči pro dosažení konzistentního přístupu k dítěti.
- **Speciální opatření**: V některých případech může být potřeba ústavní péče nebo terapie.

#### Role učitele
- **Sledování a vyhodnocování**: Pravidelně sledovat chování žáka a identifikovat případné problémy.
- **Podpora**: Vytváření podpůrného prostředí pro žáky s SPU a SPCH, využívání vhodných metod a technik pro usnadnění jejich učení a chování.
- **Vzdělávání**: Neustálé sebevzdělávání a zlepšování kvalifikace v oblasti speciální pedagogiky a psychologie.

Tento systematický přístup k diagnostice a intervenci specifických poruch učení a chování pomáhá vytvořit inkluzivní a podpůrné prostředí pro všechny žáky ve školách.
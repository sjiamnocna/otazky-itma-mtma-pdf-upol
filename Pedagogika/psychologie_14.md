# 14. Psychologická charakteristika mladšího školního věku (vývoj tělesný, kognitivní, emoční a sociální vývoj, učení a práce).

## Vývoj tělesný
- **Růst a fyzický vývoj**: V tomto období (zhruba 6–12 let) děti procházejí stabilním, ale pomalejším růstem ve výšce a hmotnosti ve srovnání s předchozími obdobími. Dochází k postupnému zpevňování kostry a vývoji svalové hmoty.
- **Hrubá a jemná motorika**: Děti v tomto věku zlepšují své motorické dovednosti. Rozvoj jemné motoriky umožňuje precizní úkoly, jako je psaní nebo kreslení, zatímco zlepšení hrubé motoriky podporuje účast v různých sportech a fyzických aktivitách.
- **Fyzická aktivita**: Děti jsou v tomto věku velmi aktivní, mají dostatek energie a zapojují se do různých her a sportů. Je důležité podporovat pravidelnou fyzickou aktivitu pro správný tělesný vývoj a prevenci zdravotních problémů.

## Kognitivní vývoj
- **Piagetovo stadium konkrétních operací**:
  - Děti začínají rozvíjet logické myšlení a schopnost pracovat s konkrétními objekty a situacemi.
  - **Konzervace**: Schopnost chápat, že určitá vlastnost objektů (např. množství) zůstává stejná i přes změnu jejich formy nebo vzhledu.
  - **Klasifikace a sériace**: Děti se učí třídit objekty do kategorií a řadit je podle určitých kritérií (např. velikost, tvar).
  - **Reversibilita**: Schopnost pochopit, že změny mohou být obráceny, což je důležité pro rozvoj matematických a logických dovedností.
- **Jazykový rozvoj**: V tomto věku dochází k výraznému rozšíření slovní zásoby a zlepšení gramatických struktur. Děti jsou schopné používat složitější větné konstrukce a rozvíjet své komunikační dovednosti.

## Emoční vývoj
- **Sebepojetí a sebeúcta**: Děti si v tomto věku začínají formovat jasnější představu o sobě samých, svých schopnostech a hodnotách. Sebeúcta je ovlivňována úspěchy ve škole, sportu a sociálních vztazích.
- **Emoční regulace**: Děti zlepšují schopnost rozpoznávat a řídit své emoce, což jim pomáhá zvládat frustraci a konflikty. Učí se také empatii, což je schopnost porozumět a reagovat na emoce druhých.
- **Stabilizace emocí**: Emoční reakce jsou v tomto věku méně impulsivní a více kontrolované ve srovnání s předškolním obdobím.

## Sociální vývoj
- **Vztahy s vrstevníky**: Sociální interakce s vrstevníky se stávají důležitějšími. Děti vytvářejí přátelství na základě společných zájmů a aktivit. Tyto vztahy jsou důležité pro sociální učení a rozvoj sociálních dovedností.
- **Skupinová dynamika**: Děti se učí spolupracovat v týmech a dodržovat skupinová pravidla. Zapojení do skupinových aktivit podporuje rozvoj týmového ducha a pocitu sounáležitosti.
- **Vliv vrstevníků**: Vrstevníci začínají mít stále větší vliv na chování a rozhodování dětí. To může mít pozitivní i negativní dopady, včetně vlivu na sebehodnocení a morální vývoj.
- **Rodinné vztahy**: I když se děti stávají více nezávislými, rodina zůstává klíčovým zdrojem podpory a bezpečí.

## Učení a práce
- **Akademický rozvoj**: Děti se učí základní akademické dovednosti, jako je čtení, psaní a matematika. Tento věk je klíčový pro rozvoj základů, na kterých budou stavět další vzdělávací úspěchy.
- **Motivace k učení**: Děti rozvíjejí vnitřní motivaci k učení a zvědavost. Učitelé a rodiče mohou tuto motivaci podporovat prostřednictvím pozitivní zpětné vazby a podpory zájmů dítěte.
- **Rozvoj pracovních návyků**: Děti se učí organizovat své úkoly, plánovat čas a pracovat samostatně. Tyto dovednosti jsou důležité pro akademický úspěch i budoucí kariérní úspěch.
- **Hodnocení úspěchu a neúspěchu**: Děti se učí vyrovnávat se s úspěchy a neúspěchy, což přispívá k rozvoji odolnosti a sebevědomí.

> Toto období je klíčové pro rozvoj základních dovedností a vlastností, které budou děti provázet po celý život. Důraz na podporu zdravého fyzického, kognitivního, emočního a sociálního vývoje je nezbytný pro úspěšné zvládnutí tohoto vývojového stádia.
# 25. Prezentujte význam stěžejních právních předpisů pro práci pedagogického pracovníka v regionálním školství.

## Stěžejní právní předpisy

1. **Zákon č. 561/2004 Sb., o předškolním, základním, středním, vyšším odborném a jiném vzdělávání (školský zákon)**
   - Tento zákon je základním právním předpisem upravujícím organizaci a fungování školského systému v České republice. 
   - Obsahuje ustanovení týkající se povinné školní docházky, organizace školského systému, podmínek pro zřizování a provozování škol, vzdělávacích programů a hodnocení výsledků vzdělávání.

2. **Zákon č. 563/2004 Sb., o pedagogických pracovnících a o změně některých zákonů**
   - Definuje předpoklady pro výkon pedagogické činnosti, získávání odborné kvalifikace, podmínky pro výkon funkce ředitele škol a školských zařízení.
   - Stanovuje systém dalšího vzdělávání pedagogických pracovníků a jejich kariérní růst.

3. **Vyhlášky Ministerstva školství**
   - **Vyhláška č. 10/2005 Sb., o vyšším odborném vzdělávání**
   - **Vyhláška č. 12/2005 Sb., o podmínkách uznání rovnocennosti a nostrifikace vysvědčení vydaných zahraničními školami**
   - **Vyhláška č. 13/2005 Sb., o středním vzdělávání a vzdělávání v konzervatoři**
   - **Vyhláška č. 14/2005 Sb., o předškolním vzdělávání**
   - **Vyhláška č. 15/2005 Sb., kterou se stanoví náležitosti dlouhodobých záměrů, výročních zpráv a vlastního hodnocení školy**
   - **Vyhláška č. 16/2005 Sb., o organizaci školního roku**
   - **Vyhláška č. 263/2007 Sb., pracovní řád pro zaměstnance škol a školských zařízení zřízených MŠMT, krajem, obcí nebo dobrovolným svazkem obcí**.

4. **Nařízení vlády**
   - **Nařízení vlády č. 75/2005 Sb., o stanovení rozsahu přímé vyučovací, přímé výchovné, přímé speciálně pedagogické a přímé pedagogicko-psychologické činnosti pedagogických pracovníků**
   - Stanovuje rozsah hodin přímé pedagogické činnosti a počet tříd rozhodný pro stanovení přímé pedagogické činnosti.

## Význam těchto předpisů

- **Organizace školství**: Stanovují pravidla pro organizaci školského systému, včetně povinné školní docházky, struktury vzdělávacích programů a hodnocení výsledků vzdělávání.
- **Kvalifikace pedagogů**: Definují požadavky na odbornou kvalifikaci pedagogických pracovníků, čímž zajišťují kvalitu výuky.
- **Práva a povinnosti**: Upravují práva a povinnosti žáků, pedagogických pracovníků a dalších subjektů ve školství.
- **Bezpečnost a ochrana zdraví**: Obsahují ustanovení týkající se bezpečnosti a ochrany zdraví ve školním prostředí, což je klíčové pro vytvoření bezpečného a zdravého prostředí pro vzdělávání.

## Dopady na práci pedagogického pracovníka

- Pedagogičtí pracovníci musí být obeznámeni s právními předpisy, které upravují jejich profesní činnost, aby mohli efektivně vykonávat své povinnosti.
- Musí zajistit, aby výuka a další činnosti ve škole byly v souladu s platnými právními normami.
- Jsou zodpovědní za dodržování bezpečnostních předpisů a ochranu zdraví žáků během výuky a dalších školních aktivit.
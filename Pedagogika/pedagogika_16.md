# 16. Definujte didaktiku jako vědeckou disciplínu a začleňte ji do systému pedagogických ěd. Vysvětlete vztah pedagogiky a obecné didaktiky. Objasněte pojem „didaktický rojúhelník“ a uveďte příklady či důsledky preference jednotlivých aspektů. Vymezte ktuální problémy obecné didaktiky.

## Definice didaktiky
Didaktika je vědecká disciplína, která se zabývá problematikou vzdělávacích obsahů a procesů, tedy vyučováním a učením. Stručně řečeno, jde o teorii vzdělávání a vyučování. Slovo „didaktika“ pochází z řeckého „didaskein“, což znamená učit, vyučovat, poučovat, jasně vykládat, dokazovat.

## Začlenění do systému pedagogických věd
Didaktika je součástí širšího systému pedagogických věd, které zahrnují:
1. **Obecnou pedagogiku**: Základy pedagogiky, společenské funkce výchovy, cíle výchovy, metodologické problémy pedagogiky.
2. **Historie pedagogiky**: Vývoj pedagogických idejí a analýza děl pedagogických myslitelů.
3. **Pedagogická psychologie**: Psychologické aspekty výchovně-vzdělávacího procesu.
4. **Speciální pedagogika**: Výchova jedinců se speciálními potřebami.
5. **Sociologie výchovy a vzdělávání**: Společenské aspekty vzdělávání a výchovy.

## Vztah pedagogiky a obecné didaktiky
Pedagogika jako širší vědní obor zahrnuje teorie a praxi výchovy a vzdělávání. Obecná didaktika je specifickou součástí pedagogiky, která se zaměřuje na teoretické základy a metodologii vyučovacího procesu. Obecná didaktika poskytuje teoretický základ pro speciální didaktiky, které se soustředí na konkrétní předměty nebo vzdělávací stupně (např. didaktika matematiky, didaktika dějepisu).

## Didaktický trojúhelník
Didaktický trojúhelník je koncept, který znázorňuje interakci mezi třemi základními komponenty vyučovacího procesu:
1. **Učitel**: Osoba, která předává znalosti a řídí proces učení.
2. **Žák**: Osoba, která přijímá znalosti a aktivně se podílí na procesu učení.
3. **Obsah učiva**: Materiál a témata, která jsou předmětem výuky.

## Příklady a důsledky preference jednotlivých aspektů
- **Preference učitele**: Důraz na učitele může vést k autoritativnímu stylu výuky, kde je kladen důraz na přenos znalostí od učitele k žákům. Důsledek může být pasivní role žáků a méně prostor pro jejich samostatnou práci a kreativitu.
- **Preference žáka**: Zaměření na žáka podporuje aktivní učení a individualizovaný přístup. Důsledek je větší zapojení žáků, ale může být náročnější na organizaci a přípravu výuky.
- **Preference obsahu učiva**: Důraz na obsah může vést k systematickému a strukturovanému přístupu k výuce, ale může také omezit flexibilitu a schopnost reagovat na individuální potřeby žáků.

## Aktuální problémy obecné didaktiky
1. **Kurikulární reforma**: Neustálé změny a aktualizace kurikula pro reflektování aktuálních vědeckých a společenských potřeb.
2. **Inovace ve výuce**: Integrace nových technologií a metod do výuky, které vyžadují nové přístupy a dovednosti učitelů.
3. **Individualizace a diferenciace**: Přizpůsobení výuky různým potřebám a schopnostem žáků.
4. **Hodnocení a zpětná vazba**: Vývoj efektivních způsobů hodnocení, které podporují učení a motivaci žáků.
5. **Interdisciplinární přístup**: Potřeba spolupráce mezi různými vědními disciplínami pro komplexní pochopení a řešení vzdělávacích problémů.
# 30. Charakterizujte základní pojmy přístupy, formy a stupně prevence rizikového chování. veďte zásady primární prevence. Charakterizujte systém prevence ve školním rostředí - preventivní strategie školy, činnost jednotlivých aktérů, role školního etodika prevence. Uveďte příklady sekundární prevence a intervence a terciální revence

## Základní pojmy
- **Sociální deviace**: Odchylka od očekávaného standardního a intencionálně zdravého chování. Negativní, rizikové, ohrožující chování.
- **Delikvence**: Asociální chování/jednání, páchání přestupků, širší než kriminalita, ohrožující charakter.
- **Rizikové chování**: Jakékoli cílené jednání nebo aktivita jednotlivce, jehož následkem může být zranění, smrt, trvalé postižení nebo jiné snížení kvality života, narušení vztahů, psychiky, ekonomické a hmotné škody.

## Typy rizikového chování
- **Záškoláctví**
- **Šikana a extrémní projevy agrese**
- **Extrémně rizikové sporty a rizikové chování v dopravě**
- **Rasismus a xenofobie**
- **Negativní působení sekt**
- **Sexuální rizikové chování**
- **Závislostní chování**.

## Prevence
- **Definice**: Soubor opatření zaměřujících na předcházení nežádoucím společenským jevům a všem rizikovým formám chování – jejich vzniku, rozvíjení se a recidivě.

## Zásady primární prevence
- **Primární prevence**:
  - **Zaměřena na širší populaci (dětí, žáků)**, která nemá rizikovou zkušenost. Dotýká se všeobecných podmínek předcházení a měla by mít charakter imunizace.
  - **Dělení podle obsahu**:
    - **Nespecifická**: Podporuje žádoucí formu obecně (mravní výchova, výchova ke zdraví, životní styl, utváření postojů, trávení volného času).
    - **Specifická**: Zaměřena proti konkrétnímu riziku (drogy, delikvence, šikana, rizikové chování).
  - **Dělení dle cílové skupiny**:
    - **Všeobecná**: Zaměřena na širší spektrum dětí, aniž by u nich byly zjištěny nějaké konkrétní problémy.
    - **Selektivní**: Zaměřena na skupinu dětí a mládeže, u které bychom chtěli zabránit konkrétnímu rizikovému chování.
    - **Indikovaná**: Zaměřena na děti a mladistvé, u kterých se již rizikové chování vyskytlo.

## Systém prevence ve školním prostředí

### Preventivní strategie školy
- **Rámcové vzdělávací programy**: Tvorba školního vzdělávacího programu.
- **Vzdělávací oblast**: Člověk a jeho svět (1. stupeň), Člověk a zdraví (výchova ke zdraví, TV) – 2. stupeň.
- **Průřezová témata**: Osobností a sociální témata, mediální výchova.
- **Hlavní aktivity školy**: Zajištění školních metodiků prevence, zavádění etické a mravní výchovy, výchova ke zdraví, spolupráce s rodiči.

### Činnost jednotlivých aktérů
- **Ředitel školy**: Zodpovědný za celkovou strategii prevence.
- **Školní metodik prevence**: Koordinuje preventivní aktivity, vede programy a konzultace směřující ke zdravému životnímu stylu dětí, zajišťuje zdravé vztahy ve třídě a mezi spolužáky.
- **Třídní učitelé**: Realizují preventivní programy ve třídě.
- **Participující odborné instituce**: Poskytují odbornou pomoc a podporu.
- **Pedagogický sbor**: Podílí se na prevenci rizikového chování.

### Příklady preventivních programů
- **Peer programy**: Zaměřeny na vrstevníky, kteří se dobrovolně věnují primární prevenci.
- **Divadlo Fórum**: Prevence prožitkem, interaktivní hra týkající se problematiky rizikového chování.
- **Beseda s ex-userem**: Sezení s bývalým uživatelem návykových látek.
- **Interaktivní programy, žákovy projekty, promítání filmů** (např. My děti ze stanice Zoo, kniha Memento).

## Sekundární prevence
- **Zaměřena na ohrožené jedince a skupiny s různou mírou rizikové zkušenosti** (např. pravidelné experimentování s návykovými látkami, delikventní chování, agrese, šikanování).
- **Úlohou je zabránit „sociopatologické kariéře“ ohroženého**.
- **Aktivizace rodiny a dostupnost odborného poradenského systému ve škole** (školní poradenské pracoviště).

## Terciární prevence
- **Určena jedincům, kteří propadli závislosti nebo poruchou chování** a má zabránit její další recidivě.
- **Dva základní kroky**:
  - **Terapie**: Medicinská, psychologická, etopedická.
  - **Hledání nového smyslu života a místa ve společnosti**: Formování nových sociálních vztahů, nabídka pozitivních činností, návrat do společnosti (resocializace).
# 7. Charakterizujte specifika pedagogické komunikace (verbální, neverbální, činem). Uveďte příklady paralingvistických projevů a komunikace ve výchově. Charakterizujte typické bariéry v pedagogické komunikaci. Předpoklady úspěšné komunikace v edukačním prostředí. Obhajte asertivitu jako vhodný prostředek komunikace

## Specifika pedagogické komunikace
- **Verbální komunikace**:
  - Použití jazyka k předávání informací.
  - Klíčové prvky: jasnost, srozumitelnost, přiměřenost věku a schopnostem žáků.
  - Příklady: vysvětlení učiva, pokyny, otázky a odpovědi.

- **Neverbální komunikace**:
  - Komunikace pomocí gest, mimiky, postojů a pohybů těla.
  - Důležitost neverbálních signálů: doplnění a podpora verbální komunikace.
  - Příklady: oční kontakt, úsměv, gesta rukou, postoj těla.

- **Komunikace činem**:
  - Komunikace prostřednictvím konkrétních akcí a činností.
  - Pedagogické činy jako výchovné nástroje: příklady chování a modelování žádoucích vzorců.
  - Příklady: demonstrace experimentu, společná aktivita, konkrétní pomoc žákovi.

## Paralingvistické projevy a komunikace ve výchově
- **Paralingvistické projevy**:
  - Aspekty řeči, které nejsou spojeny s obsahem slov.
  - Příklady: intonace, hlasitost, tempo řeči, pauzy, výška hlasu.
- **Komunikace ve výchově**:
  - Používání paralingvistických projevů ke zvýšení účinnosti verbální komunikace.
  - Význam při vyjadřování emocí, důrazu a záměru.

## Typické bariéry v pedagogické komunikaci
- **Fyzické bariéry**:
  - Hluk, nevhodné uspořádání třídy, technické problémy.
- **Psychologické bariéry**:
  - Předsudky, strach, úzkost, nízké sebevědomí.
- **Jazykové bariéry**:
  - Nejasnosti, odborné termíny, jazykové rozdíly.
- **Sociální a kulturní bariéry**:
  - Různé kulturní normy, socioekonomické rozdíly.

## Předpoklady úspěšné komunikace v edukačním prostředí
- **Empatie a porozumění**:
  - Schopnost vžít se do situace žáků, porozumět jejich potřebám a problémům.
- **Jasnost a srozumitelnost**:
  - Používání jednoduchého a přehledného jazyka, přizpůsobení obsahu věku a schopnostem žáků.
- **Aktivní naslouchání**:
  - Věnování plné pozornosti žákům, zpětná vazba, potvrzení porozumění.
- **Důvěra a respekt**:
  - Budování pozitivních vztahů založených na vzájemné úctě a důvěře.

## Asertivita jako vhodný prostředek komunikace
- **Definice asertivity**:
  - Schopnost vyjádřit své názory, potřeby a pocity otevřeně a upřímně, aniž bychom porušovali práva druhých.
- **Výhody asertivní komunikace**:
  - Zlepšuje sebevědomí a sebeúctu.
  - Podporuje otevřenou a čestnou komunikaci.
  - Snižuje úzkost a stres spojený s komunikací.
- **Asertivita v pedagogické praxi**:
  - Vytváří pozitivní a podporující učební prostředí.
  - Umožňuje efektivní řešení konfliktů a problémových situací.
  - Poskytuje model zdravé komunikace pro žáky.
- **Příklady asertivního chování**:
  - Vyjádření nesouhlasu bez agrese.
  - Říci "ne" bez pocitu viny.
  - Požádání o pomoc nebo podporu jasně a přímě.

- **Techniky asertivní komunikace**:
  - **Já výroky**: Zaměření na vlastní pocity a potřeby místo obviňování druhých (např. "Cítím se přehlížen, když mě neposloucháš").
  - **Opakování**: Trvání na svém názoru nebo požadavku klidně a pevně.
  - **Vyjednávání**: Hledání kompromisu a dohody, která vyhovuje oběma stranám.

Asertivita je klíčovým prvkem úspěšné pedagogické komunikace, protože podporuje otevřenost, respekt a vzájemné porozumění.
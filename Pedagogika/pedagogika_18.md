# 18. Vyjmenujte pořadí šesti hierarchicky uspořádaných kategorií kognitivních cílů Bloomovy taxonomie a vysvětlete jednotlivé kategorie. Ke každé kategorii formulujte ednoznačně odpovídající výukový cíl (cíle spolu nemusí souviset).

1. Zapamatování (Remembering)
- **Definice**: Schopnost si vybavit, rozeznat a znovu reprodukovat informace, fakta a základní pojmy.
- **Výukový cíl**: Žák vyjmenuje hlavní města evropských zemí.

2. Porozumění (Understanding)
- **Definice**: Schopnost pochopit význam informací a vyjádřit je vlastními slovy, interpretovat a shrnout.
- **Výukový cíl**: Žák vysvětlí hlavní myšlenky článku o klimatických změnách.

3. Aplikace (Applying)
- **Definice**: Schopnost použít naučené informace v nových situacích a praktických kontextech.
- **Výukový cíl**: Žák aplikuje matematické vzorce k vyřešení slovních úloh.

4. Analýza (Analyzing)
- **Definice**: Schopnost rozložit informace na části a pochopit jejich strukturu, rozpoznat vztahy mezi částmi.
- **Výukový cíl**: Žák analyzuje strukturu básně a identifikuje použité literární prostředky.

5. Hodnocení (Evaluating)
- **Definice**: Schopnost posuzovat a kriticky hodnotit informace, argumenty nebo návrhy na základě kritérií a standardů.
- **Výukový cíl**: Žák posoudí věrohodnost zdrojů použitého článku a obhájí svůj názor.

6. Syntéza (Creating)
- **Definice**: Schopnost vytvořit nové, originální práce nebo myšlenky kombinováním různých prvků do nového celku.
- **Výukový cíl**: Žák navrhne a vytvoří vlastní vědecký experiment k ověření hypotézy.

---

## Formulace výukových cílů pro každou kategorii

1. **Zapamatování**
   - Žák vyjmenuje hlavní města evropských zemí.

2. **Porozumění**
   - Žák vysvětlí hlavní myšlenky článku o klimatických změnách.

3. **Aplikace**
   - Žák aplikuje matematické vzorce k vyřešení slovních úloh.

4. **Analýza**
   - Žák analyzuje strukturu básně a identifikuje použité literární prostředky.

5. **Hodnocení**
   - Žák posoudí věrohodnost zdrojů použitého článku a obhájí svůj názor.

6. **Syntéza**
   - Žák navrhne a vytvoří vlastní vědecký experiment k ověření hypotézy.
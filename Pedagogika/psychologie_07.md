# 7. Oblast citů a jejich význam v životě. Dělení a vlastnosti citů, fyziologie citů, pojem emoční inteligence (Goleman). Emoce a motivace.

## Význam citů
- **Citové prožívání** je důležitou součástí lidského života, ovlivňuje rozhodování, motivaci, mezilidské vztahy a celkovou pohodu.
- Cíle, hodnoty a morální přesvědčení jsou často formovány a posilovány emocionálními zážitky.
- **Emoce** poskytují rychlou informaci o okolním prostředí a vnitřních stavech, což je klíčové pro přežití a adaptaci.

## Dělení citů
1. **Základní emoce**: 
   - Univerzální, vrozené a často rychle reagující na podněty.
   - **Příklady**: Radost, smutek, strach, hněv, překvapení, znechucení.

2. **Komplexní emoce**:
   - Vznikají kombinací základních emocí a jsou často kulturně a individuálně specifické.
   - **Příklady**: Žárlivost, pýcha, vina, láska.

3. **Afektivní stavy**:
   - Dlouhotrvající emoční stavy, které ovlivňují celkové prožívání.
   - **Příklady**: Nálady, deprese, euforie.

## Vlastnosti citů
- **Intenzita**: Síla, s jakou je cit prožíván.
- **Doba trvání**: Jak dlouho emoce trvá.
- **Polarita**: Pozitivní (příjemné) a negativní (nepříjemné) emoce.
- **Komplexnost**: Jednoduché (základní) vs. složené (komplexní) emoce.

## Fyziologie citů
- **Fyziologické změny**: Emoce ovlivňují autonomní nervový systém (srdeční frekvence, krevní tlak, dýchání) a endokrinní systém (hormony).
- **Vnější projevy**: Tělesné reakce jako výrazy obličeje, gestikulace, změny hlasu.
- **Mozková činnost**: Emoce jsou regulovány limbickým systémem, zvláště amygdalou, která hraje klíčovou roli v emočním zpracování.

## Pojem emoční inteligence (Goleman)
- **Emoční inteligence (EI)**: Schopnost rozpoznávat, chápat a regulovat vlastní emoce a emoce ostatních.
- **Složky EI podle Daniela Golemana**:
  1. **Sebepoznání**: Uvědomování si vlastních emocí a jejich vlivu.
  2. **Sebeovládání**: Schopnost regulovat své emoce a impulsy.
  3. **Motivace**: Emoční hnací síla vedoucí k dosažení cílů.
  4. **Empatie**: Schopnost vnímat a rozumět emocím ostatních.
  5. **Sociální dovednosti**: Schopnost řídit vztahy a navazovat pozitivní interakce.

## Emoce a motivace
- **Motivace**: Vnitřní nebo vnější faktory, které iniciují, udržují a směřují chování.
- **Role emocí v motivaci**: Emoce často fungují jako motivační síla, například strach může motivovat k úniku, zatímco radost může motivovat k pokračování v činnosti.
- **Pozitivní a negativní motivace**: Pozitivní motivace (odměny, pozitivní emoce) směřují k dosažení příjemných cílů, zatímco negativní motivace (tresty, negativní emoce) vedou k vyhýbání se nepříjemným situacím.
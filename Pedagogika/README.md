# KPG/SZPR@ - Pedagogická propedeutika (2023/24)

- [Seznam otázek z pedagogiky](#otázky-z-pedagogiky-202324)
- [Seznam otázek z psychologie](#psychologie)

## Otázky z pedagogiky (2023/24)

1. [Charakterizujte pedagogiku jako vědní obor. Popište a interpretujte historický vývoj edagogiky jako vědy. Vysvětlete vztah pedagogiky k ostatním vědám. Charakterizujte předmět, znaky a funkce pedagogiky. Vztah pedagogické teorie a pedagogické praxe. Uveďte systém pedagogických disciplín. Popište a zhodnoťte aktuální problémy a otázky pedagogiky.](./pedagogika_1.md)
 
2. [Popište a objasněte faktory rozvoje lidského jedince – biologické faktory, prostředí, výchova. Definujte výchovu, její pojetí, atributy, funkce, význam, podmínky. Vysvětlete rozdíl mezi intencionálním a funkcionálním výchovným působení. Charakterizujte sebevýchovu.](./pedagogika_2.md)
 
3. [Vysvětlete rozdíly definovaní kategorie cíl výchovy v různých pedagogických koncepcích. Interpretujte znaky „dobrých“ cílů. Uveďte funkce, třídění, klasifikace a strukturu výchovných cílů. Charakterizujte názory na současné cíle výchovy.](./pedagogika_3.md)
 
4. [Vysvětlete kategorii výchovný proces – etapy, fáze a principy výchovného procesu. Uveďte výchovné zásady a efektivitu výchovného procesu. Vysvětlete kategorii podmínky a prostředky výchovy.](./pedagogika_4.md)
 
5. [Objasněte kategorii osobnost učitele. Uveďte požadavky kladené na osobnost učitele. Uveďte aktuální problémy pedagogické profese. Prezentujte možné typologie učitele a vývoj profesní dráhy učitelů. Charakterizujte kázeň a autoritu učitele ve výchově. Popište možnosti reflexe a sebereflexe v kontextu problematiky syndromu vyhoření u pedagogických pracovníků.](./pedagogika_5.md)
 
6. [Objasněte kategorii osobnost vychovávaného jedince a popište a argumentujte modely determinace lidské psychiky (výchovný pesimismus, pedagogický utopismus, pedagogický optimismus, pedagogický realismus). Charakterizujte interakci vychovatele a vychovávaného. Uveďte a argumentujte specifika výchovná práce s jedinci se specifickými potřebami, obhajte koncept integrace a inkluze.](./pedagogika_6.md)
 
7. [Charakterizujte specifika pedagogická komunikace (verbální, neverbální, činem). Uveďte příklady paralingvistických projevů a komunikace ve výchově. Charakterizujte typické bariéry v pedagogické komunikaci. Předpoklady úspěšné komunikace v edukačním prostředí. Obhajte asertivitu jako vhodný prostředek komunikace](./pedagogika_7.md)
 
8. [Charakterizujte prostředí školy. Uveďte možné inspirace alternativními pedagogickými systémy pro zefektivnění výchovného procesu.](./pedagogika_8.md)
 
9. [Charakterizujte základy kvantitativně a kvalitativně orientovaného výzkumu v pedagogice (metody lidského poznávání, proveďte jejich vzájemné porovnání, uveďte klady, zápory, charakterizujte rozdíly výzkumu ex-post-facto a experimentu).](./pedagogika_9.md)
 
10. [Charakterizujte základní typy měření v pedagogickém výzkumu a popište typy proměnných. Vysvětlete vlastnosti dobrého měření, výzkumné problémy (výzkumné otázky), věcné a statistické hypotézy ve výzkumu.](./pedagogika_10.md)
 
11. [Charakterizujte základní metody sběru empirických dat a na konkrétních příkladech objasněte jejich použití.](./pedagogika_11.md)
 
12. [Popište nejběžnější postupy zpracování dat v pedagogických výzkumech (četnosti, grafické metody zobrazování dat, charakteristiky polohy, metody průzkumové analýzy dat, normální rozdělení).](./pedagogika_12.md)
 
13. [Prezentujte výukové metody a jejich klasifikaci (dle vybraného autora). Prezentujte organizační formy výuky a jejich klasifikaci. Demonstrujte možnosti využití vhodných metod a forem výuky ve výchovně vzdělávací činnosti.](./pedagogika_13.md)
 
14. [Charakterizujte pedagogickou diagnostiku a argumentujte její význam v současné škole. Popište základní metody pedagogické diagnostiky. Charakterizujte způsoby a význam hodnocení ve školní praxi.](./pedagogika_14.md)
 
15. [Charakterizujte a zdůvodněte základní trendy proměn výchovy v historickém kontextu dobových paradigmat (pojetí člověka a výchovy, cíle výchovy). Vysvětlete, jak se dobové koncepty výchovy odrážejí v současných koncepcích výchovy a vzdělávání.](./pedagogika_15.md)
 
16. [Definujte didaktiku jako vědeckou disciplínu a začleňte ji do systému pedagogických věd. Vysvětlete vztah pedagogiky a obecné didaktiky. Objasněte pojem „didaktický trojúhelník“ a uveďte příklady či důsledky preference jednotlivých aspektů. Vymezte aktuální problémy obecné didaktiky.](./pedagogika_16.md)
 
17. [Definujte pojem „výukový cíl“. Vyjmenujte požadavky na formulaci výukových cílů, aby jich mohlo být ve výuce co nejefektivněji dosaženo. Vysvětlete pojem aktivní (aktivizační) slovesa a v kontextu vaší oborové specializace uveďte čtyři konkrétní příklady jejich použití při formulaci výukového cíle. S využitím zvolené taxonomie vyhodnoťte náročnost těchto cílů.](./pedagogika_17.md)
 
18. [Vyjmenujte pořadí šesti hierarchicky uspořádaných kategorií kognitivních cílů Bloomovy taxonomie a vysvětlete jednotlivé kategorie. Ke každé kategorii formulujte jednoznačně odpovídající výukový cíl (cíle spolu nemusí souviset).](./pedagogika_18.md)
 
19. [Objasněte principy, na kterých jsou postaveny taxonomie cílů afektivních, některou z taxonomií představte. Ke konkrétnímu tematickému celku/výukové jednotce naformulujte libovolný výukový cíl, v němž převládá postojová dimenze, a uveďte, která klíčová kompetence je tímto především rozvíjena.](./pedagogika_19.md)
 
20. [Objasněte principy, na kterých je postavena taxonomie cílů psychomotorických, některou z taxonomií představte. Na konkrétním příkladu prezentujte jednotlivé úrovně této taxonomie. V souladu s vaší oborovou zaměřeností formulujte libovolný výukový cíl, v němž převládá psychomotorická dimenze a kterého lze dosáhnout v rámci výuky ve škole.](./pedagogika_20.md)
 
21. [Prezentujte ucelený systém didaktických zásad. Vysvětlete postup učitelovy práce s chybou ve vyučování. Prezentujte chybu jako pozitivní jev pro učení žáka a navrhněte konkrétní doporučení pro učitele při práci s chybou ve výuce vašeho aprobačního předmětu.](./pedagogika_21.md)
 
22. [Ozřejměte, jaké jsou zásady práce učitele s materiálními didaktickými prostředky. Posuďte, jaké učební pomůcky lze ve vašem aprobačním předmětu používat při realizaci zásady názornosti, a uveďte je. Objasněte, jaká je jejich role při výuce i jaký je poměr učitelova slova a prezentace pomůcky.](./pedagogika_22.md)
 
23. [Popište systém kurikulárních dokumentů aktuálně platných pro školy regionálního školství. U dokumentů na státní úrovni vymezte, čím a jak podmiňují podobu kurikulárních dokumentů na nižší úrovni.](./pedagogika_23.md)
 
24. [Charakterizujte materiální didaktické prostředky a proveďte jejich možné klasifikace. Učební pomůcky, didaktická technika. Informatické myšlení, digitální kompetence pedagogů a žáků. Interaktivní a programovatelná technika, virtuální a rozšířená realita, převrácená třída, otevřené vzdělávání, digitální gramotnost, informatické myšlení. Evropský rámec digitálních kompetencí pedagogů.](./pedagogika_24.md)
 
25. [Prezentujte význam stěžejních právních předpisů pro práci pedagogického pracovníka v regionálním školství.](./pedagogika_25.md)
 
26. [Charakterizujte specifické poruchy učení a chování – vymezte základní pojmy, etiologii, jejich diagnostika a následnou pedagogikou intervenci.](./pedagogika_26.md)
 
27. [Popište systém vzdělávání v ČR. Uveďte specifika předškolního, základního, středního, vyššího odborného, uměleckého, jazykového a zájmového (příp. jiného) vzdělávání. Uveďte dílčí podrobnosti organizace školního roku.](./pedagogika_27.md)
 
28. [Definujte pojem školská legislativa. Demonstrujte provázanost školských právních předpisů v rámci logicky uzavřeného celku s právními předpisy jednotlivých odvětví práva soukromého a veřejného s následnou aplikací do praktické roviny.](./pedagogika_28.md)
 
29. [Vyjmenujte práva a povinnosti účastníků vzdělávacího procesu v návaznosti na vnitřní předpisy škol a školských zařízení. Zdůvodněte význam dodržování předpisů z oblasti bezpečnosti a ochrany zdraví při vzdělávání a s ním přímo souvisejících činnostech. Popište možnosti využití interdisciplinární spolupráce při řešení výchovných problémů na školách a možnosti využití výchovných opatření ze strany škol a školských zařízení.](./pedagogika_29.md)
 
30. [Charakterizujte základní pojmy přístupy, formy a stupně prevence rizikového chování. Uveďte zásady primární prevence. Charakterizujte systém prevence ve školním prostředí - preventivní strategie školy, činnost jednotlivých aktérů, role školního metodika prevence. Uveďte příklady sekundární prevence a intervence a terciální prevence.](./pedagogika_30.md)

## Psychologie

1. [Předmět zkoumání v psychologii, základní pojmy (prožívání, chování, psychické procesy a stavy, introspekce, osobní konstrukty a mentální reprezentace). Systém psychologických věd.](./psychologie_01.md)

2. [Přehled základních psychologických směrů – psychoanalýza, behaviorismus, humanistická psychologie, kognitivní psychologie, gestalt psychologie, transpersonální psychologie a představitelé jednotlivých směrů.](./psychologie_2.md)

3. [Psychologická charakteristika osobnosti (univerzální znaky), determinace lidské psychiky (biologická, sociálně-kulturní a duchovní-Frankl, psychologický determinismus, nativistické a environmentální teorie). Přístupy ke studiu osobnosti (biologický, experimentální, psychometrický, sociální).](./psychologie_3.md)

4. [Poznávací procesy a jejich charakteristika - čití a vnímání, představy a fantazie, paměť.](./psychologie_4.md)

5. [Poznávací procesy a jejich charakteristika – pozornost a vědomí, myšlení a inteligence.](./psychologie_5.md)

6. [Temperament, přehled základních temperamentových typologií a možnosti jejich praktického využití (např. Kretschmer, Spranger, Eysenck, Pavlov, Jung). Emotivita a temperament.](./psychologie_6.md)

7. [Oblast citů a jejich význam v životě. Dělení a vlastnosti citů, fyziologie citů, pojem emoční inteligence (Goleman). Emoce a motivace.](./psychologie_7.md)

8. [Vývojová psychologie a její předmět, determinace psychického vývoje člověka, pojem vývojové změny, klasifikace vývojových změn. Zrání a učení.](./psychologie_8.md)

9. [Metody vývojové psychologie – výzkumné přístupy (průřezový a longitudinální přístup), metody vývojové psychologické diagnostiky.](./psychologie_9.md)

10. [Vývojová periodizace, příklady vývojových periodizací (Příhodova biopsychologická, Eriksonova psychosociální, Piagetova kognitivní, Freudova psychoanalytická).](./psychologie_10.md)

11. [Zvláštnosti dětské psychiky – sugestibilita, labilita, egocentrismus, expresivní a adaptivní chování, negativismus, eidetismus, konkretismus, personifikace a jejich projevy.](./psychologie_11.md)

12. [Vymezení normality a abnormality, problematika stresu, obranné mechanismy, frustrace, konflikt, stres, vnímání stresu; dimenze stresu: emocionální, intelektuální, sociální, enviromentální, spirituální, fyzická.](./psychologie_12.md)

13. [Neurotické obtíže v dětském věku, příčiny vzniku, druhy zátěže typické pro jednotlivá vývojová období, konkrétní typy neurotických obtíží.](./psychologie_13.md)

14. [Psychologická charakteristika mladšího školního věku (vývoj tělesný, kognitivní, emoční a sociální vývoj, učení a práce).](./psychologie_14.md)

15. [Poruchy chování, klasifikace problémového chování, příčiny vzniku, agresivita a agrese, neagresivní poruchy chování, problematika syndromu ADHD, důsledky ADHD v dospělosti.](./psychologie_15.md)

16. [Psychologická charakteristika období dospívání – prepuberta, puberta (tělesné změny, kognitivní, emocionální a sociální vývoj).](./psychologie_16.md)

17. [Psychologická charakteristika období dospívání – adolescence (identita, kognitivní, emocionální a sociální vývoj).](./psychologie_17.md)

18. [Vývojové období dospělosti (periodizace, tělesné a psychické změny, krize životního středu).](./psychologie_18.md)

19. [Vývojové období stáří (periodizace, znaky stárnutí, změny fyzické a psychické, adaptace na stáří, nejčastější patologické symptomy ve stáří).](./psychologie_19.md)

20. [Socializace, formy socializace a její činitelé. Sociální učení. Sociální postoje. Struktura, zjišťování, utváření a možnosti změny. Stereotypy a předsudky.](./psychologie_20.md)

21. [Sociální a interpersonální percepce, interakce a komunikace. Činitelé formování dojmu o jiné osobě. Obecné chyby v interpersonální percepci. Základní principy verbální a neverbální komunikace.](./psychologie_21.md)

22. [Jedinec v sociální skupině. Znaky a klasifikace sociálních skupin. Struktura a dynamika malé sociální skupiny.](./psychologie_22.md)
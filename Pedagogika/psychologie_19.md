# 19. Vývojové období stáří (periodizace, znaky stárnutí, změny fyzické a psychické, adaptace na stáří, nejčastější patologické symptomy ve stáří).

> Stáří je závěrečnou fází lidského života, která je charakterizována řadou fyzických, psychických a sociálních změn. Toto období vyžaduje adaptaci na nové životní podmínky a výzvy spojené se stárnutím.

## Periodizace stáří

1. **Mladší stáří (časné stáří)**: Přibližně od 65 do 74 let. Toto období je často spojeno s odchodem do důchodu a začátkem aktivního stárnutí.
2. **Střední stáří (pravé stáří)**: Přibližně od 75 do 84 let. V tomto období mohou začít být patrné výraznější fyzické a zdravotní problémy.
3. **Pozdní stáří (vysoké stáří)**: 85 let a více. Toto období je často spojeno s potřebou zvýšené péče a podpory.

## Znaky stárnutí

### Fyzické změny
- **Zpomalení metabolických procesů**: Snižuje se metabolismus, což může vést k nárůstu tělesné hmotnosti.
- **Snížení svalové hmoty a síly**: Postupná ztráta svalové hmoty a síly, známá jako sarkopenie.
- **Zhoršení smyslových funkcí**: Zhoršení zraku, sluchu, chuti a čichu.
- **Změny v kardiovaskulárním systému**: Snížená pružnost cév, zvýšené riziko hypertenze a srdečních chorob.
- **Zhoršení pohybového aparátu**: Degenerativní změny kloubů (artritida), osteoporóza.
- **Změny v imunitním systému**: Snížení účinnosti imunitního systému, vyšší náchylnost k infekcím a nemocem.

### Psychické změny
- **Zpomalení kognitivních procesů**: Snížení rychlosti zpracování informací, problémy s krátkodobou pamětí, avšak dlouhodobá paměť může zůstat zachována.
- **Zvýšení emocionální stability**: Někteří starší lidé mohou vykazovat vyšší úroveň emoční stability a schopnosti regulovat své emoce.
- **Sebereflexe a bilancování života**: Zvýšená míra reflexe na dosavadní život a hledání smyslu a účelu.
- **Možné změny v náladě**: Zvýšené riziko deprese, úzkosti nebo osamělosti.

## Adaptace na stáří

- **Akceptace fyzických omezení**: Přijetí a přizpůsobení se změnám v tělesné funkčnosti.
- **Udržování sociálních vztahů**: Udržování a navazování nových sociálních kontaktů, zapojení do komunitních aktivit.
- **Hledání nových zájmů a činností**: Aktivní hledání nových zájmů a koníčků, které mohou přinést radost a smysl života.
- **Péče o zdraví**: Pravidelná fyzická aktivita, zdravá strava, dodržování lékařských doporučení.
- **Přijetí koncepce životního konce**: Smíření se s konečností života, plánování závěrečné fáze života, včetně právních a finančních záležitostí.

## Nejčastější patologické symptomy ve stáří

1. **Demence**: Syndrom, který zahrnuje pokles kognitivních funkcí, jako je paměť, myšlení, orientace, porozumění a úsudek. Alzheimerova choroba je nejběžnějším typem demence.
2. **Deprese**: Může být způsobena fyzickými problémy, ztrátou blízkých, sociální izolací nebo pocitem beznaděje.
3. **Delirium**: Náhlý stav zmatenosti a snížené pozornosti, často způsobený akutním onemocněním nebo léky.
4. **Inkontinence**: Ztráta schopnosti kontrolovat močení nebo stolici, která může být fyzicky i psychicky náročná.
5. **Chronické bolesti**: Například bolesti zad, kloubů, hlavy, které mohou významně ovlivnit kvalitu života.
6. **Srdeční a cévní choroby**: Včetně hypertenze, ischemické choroby srdeční, mrtvice.
7. **Osteoporóza**: Snížení hustoty kostí, což zvyšuje riziko zlomenin.

> Stáří přináší mnoho výzev, ale také možnosti osobního růstu a dosažení nové úrovně porozumění života. Podpora rodiny, přátel a zdravotníků je klíčová pro zvládnutí této etapy s důstojností a kvalitou života.